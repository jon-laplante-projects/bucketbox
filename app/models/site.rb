require 'fileutils'
class Site < ActiveRecord::Base
  belongs_to :user
  belongs_to :template
  has_many :pages, dependent: :destroy
  has_many :domains, dependent: :destroy
  enum s3_status: [ :published, :ready ]
  before_create :no_template

  has_attached_file :upload, :storage => :s3,
                    :s3_credentials => Proc.new { |b| b.instance.bucket_name }
                    #:s3_credentials => {:bucket => "bucketbox-sites" }
  
  validates_attachment_content_type :upload, :content_type => ['application/zip']

  def generate_token(column, length = 64)
    begin
      self[column] = SecureRandom.urlsafe_base64 length
      self[column] = self[column].gsub(/[^0-9a-z]/i, '').gsub(" ", "_").downcase
    end while Site.exists?(column => self[column])
  end

  def self.[] directory
    find_by_directory directory
  end

  def local_path
    @local_path ||= "#{Rails.root}/public/#{directory}"
  end

  def zip
    %w(zips s3_downloads).each {|dir| FileUtils.mkdir_p("#{Rails.root}/tmp/#{dir}") }
    output_path = "#{Rails.root}/tmp/zips/#{directory}.zip"
    s3_downloads_path = nil
    input_path = if mode == 'published'
      s3_downloads_path = "#{Rails.root}/tmp/s3_downloads/#{directory}"
      AWS.config({
                     region: 'us-east-1',
                     credentials: AWS.config(access_key_id: "#{user.amazon_key.access_key}",
                                             secret_access_key: "#{user.amazon_key.secret_access_key}"),
                 })
      s3 = AWS::S3.new
      s3_client = AWS::S3::Client.new
      bucket = s3.buckets[directory]
      bucket.objects.each do |bucket_object|
        begin # This is failing on file download.
          key = bucket_object.key
          FileUtils.mkdir_p File.dirname("#{s3_downloads_path}/#{key}")
          File.open("#{s3_downloads_path}/#{key}", 'wb') do |file|
            s3_client.get_object({ bucket_name: directory, key: key }, target: file)
          end
        rescue
          # Nothing to do here...
        end # End Begin...rescue...end block.
      end
      s3_downloads_path
    else
      local_path
    end
    ZipGenerator.new(input_path, output_path).write
    FileUtils.rm_rf(s3_downloads_path) if s3_downloads_path
    output_path
  end

  # private
  def amazon_bucket_name
    main_domain.present? ? main_domain.url : directory
  end

  def main_domain
    domains.where(parent: true).first 
  end

  def subdomains
    domains.where(parent_url_id: main_domain.id) if main_domain
  end


  def bucket_name
    user = User.find(self.user_id)
    amazon_key = AmazonKey.find_by(user_id: self.user_id)

    { :bucket => "bucketbox-sites-#{user.token}", :access_key_id => "#{amazon_key.access_key}", :secret_access_key => "#{amazon_key.secret_access_key}" }
  end

  private

  def no_template
    self.template_id = self.template_id || Template.find_by(name: 'no_template').id
  end
end

class Image < ActiveRecord::Base
  belongs_to :user

  has_attached_file :file, :styles => { :medium => "500x500>", :thumb => "250x250>" },
                      :default_url => "/images/:style/missing.png",
                      :storage => :s3,
                      :s3_credentials => Proc.new { |b| b.instance.bucket_name },
                      # :s3_credentials => {:bucket => "bucketbox-images-#{current_user.token}" },
                      :s3_permissions => 'public-read'

  TYPES = %W( image/jpg ,  image/jpeg ,  image/png ,  image/gif )                 
                      
  validates_attachment_content_type :file, content_type: TYPES

  before_create :extract_metadata, :create_label

  before_save :extract_dimensions
  serialize :dimensions

  # Retrieves dimensions for image assets
  # @note Do this after resize operations to account for auto-orientation.
  def extract_dimensions
    tempfile = file.queued_for_write[:original]
    unless tempfile.nil?
      geometry = Paperclip::Geometry.from_file(tempfile)
      self.dimensions = [geometry.width.to_i, geometry.height.to_i]
    end
  end

  def image_attached?
    self.file.file?
  end

  def current_file_path
    file.queued_for_write[:original].path
  end  

  def extract_metadata
    return unless image_attached?
    data = MagickMetadata.new(current_file_path)
    self.metadata = { file_size: data.file_size, 
                    filename_suffix: data.filename_suffix,
                    resolution: data.resolution } 
  end

  def bucket_name
    user = User.find(self.user_id)
    amazon_key = AmazonKey.find_by(user_id: self.user_id)

    { :bucket => "bucketbox-images-#{user.token}", :access_key_id => "#{amazon_key.access_key}", :secret_access_key => "#{amazon_key.secret_access_key}" }
  end

  def create_label
    self.label = self.file_file_name
  end
end
class ReportBucket < ActiveRecord::Base
  require 'zip'
  belongs_to :user
  has_many :reports, dependent: :destroy

  after_create :create_aws_bucket

  def get_report
    aws_config
    client = Aws::S3::Client.new
    name = user.report_bucket.name
    list = client.list_objects(bucket: name).contents.select{ |x| x[:key][/\.csv.zip$/] }
    sorted_list = list.sort_by {|x| x['last_modified']}.reverse
    if sorted_list.present?
      report_key = sorted_list.first.key
      report_zip_name = report_key.split('/')[-1]
      full_name = "tmp/#{report_zip_name}"
      resp = client.get_object( response_target: full_name,
                            bucket: name,
                            key: report_key)
      unzip_file = "tmp/#{report_zip_name.gsub('.zip', '')}"

      begin
        Zip::File.open(full_name) do |zip_file|
          entry = zip_file.glob('*.csv').first
          entry.extract(unzip_file)
        end
      rescue => e
        puts "File exists already."
      end

      handled_report = SmarterCSV.process(unzip_file)

      report = Report.new
      report.name = report_zip_name.gsub('.zip', '')
      report.report_bucket_id = id
      report.current_month_spend = get_current_month_spend(handled_report)
      report.spend_by_service = get_spend_by_service(handled_report).to_json
      report.total_hosted_data = get_total_hosted_data

      report.save
      clean_tmp(full_name, unzip_file)
    else
      Rails.logger.info I18n.t('.no_reports')
    end
  end

  def check_report_bucket
    aws_config
    bucket = Aws::S3::Bucket.new(name: name)
    bucket.exists?
  end

  private

  def create_aws_bucket
    aws_config
    resource = Aws::S3::Resource.new
    bucket = resource.create_bucket({ bucket: name })
    unless bucket.exists?
      bucket = resource.create_bucket({ bucket: name })
    end
    policy = Aws::S3::BucketPolicy.new(bucket_name: name)
    update_attribute(:has_policy, true) if policy.put(policy: bucket_policy.to_json)
  end

  def clean_tmp(full_name,unzip_file)
    File.delete(full_name) if File.exist?(full_name)
    File.delete(unzip_file) if File.exist?(unzip_file)
  end

  def aws_config
    amazon_key = AmazonKey.where(user_id: user_id).last
    Aws.config.update({
      region: 'us-east-1',
      credentials: Aws::Credentials.new(amazon_key.access_key, amazon_key.secret_access_key)
    })
  end

  def get_spend_by_service(handled_report)
    list_of_services = get_list_of_services(handled_report)
    spend_by_service = {}
    if list_of_services.count > 1
      list_of_services.each do |service|
        select_for_service = handled_report.select {|hash| hash[:productname] == service }
        amount_for_service = select_for_service.inject(0) {|sum, hash| sum + hash[:cost] if hash[:cost]}
        # amount_for_service = select_for_service.inject(0) {|sum, hash| sum + hash[:'lineitem/blendedcost']}
        spend_by_service["#{service}"] = amount_for_service
      end
    else
      select_for_service = handled_report.select {|hash| hash[:productname] == select_for_service[0] }
      amount_for_service = select_for_service.inject(0) {|sum, hash| sum + hash[:cost] if hash[:cost]}
      # amount_for_service = select_for_service.inject(0) {|sum, hash| sum + hash[:'lineitem/blendedcost']}
      spend_by_service["#{select_for_service[0]}"] = amount_for_service
    end

    spend_by_service
  end

  def get_current_month_spend(handled_report)
    unless handled_report.blank?
      handled_report.inject(0) {|sum, hash| sum + hash[:cost] if sum && hash[:cost]}
      # handled_report.inject(0) {|sum, hash| sum + hash[:'lineitem/blendedcost']}
    end
  end

  def get_total_hosted_data
    aws_config
    client = Aws::S3::Client.new
    total_size = 0
    client.list_buckets.buckets.select{ |x| x[:name][/bucketbox\S*/] }.each do |bucket|
      total_size += client.list_objects(bucket: bucket.name).contents.map(&:size).sum
    end
    total_size
  end

  def get_list_of_services(handled_report)
    list_of_services_dirty  = handled_report.uniq { |x| x[:productname] }
    list_of_services = list_of_services_dirty.collect {|x| x[:productname]}
    list_of_services.reject! { |s| s.nil? }
    # list_of_services_dirty  = handled_report.uniq { |x| x[:'lineitem/productcode'] }
    # list_of_services = list_of_services_dirty.collect {|x| x[:'lineitem/productcode']}
  end

  def bucket_policy
    {
      Version: "2008-10-17",
      Statement: [
        {
          Effect: "Allow",
          Principal: {
            AWS: "arn:aws:iam::386209384616:root"
          },
          Action: [
            "s3:GetBucketAcl",
            "s3:GetBucketPolicy"
          ],
          Resource: "arn:aws:s3:::#{name}"
        },
        {
          Effect: "Allow",
          Principal: {
            AWS: "arn:aws:iam::386209384616:root"
          },
          Action: [
            "s3:PutObject"
          ],
          Resource: "arn:aws:s3:::#{name}/*"
        }
      ]
    }
  end

end

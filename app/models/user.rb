class User < ActiveRecord::Base
  after_create :create_token
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :buckets, dependent: :destroy
  has_many :documents, dependent: :destroy
  has_many :sites, dependent: :destroy
  has_many :images, dependent: :destroy
  has_many :videos, dependent: :destroy
  has_many :audios, dependent: :destroy
  has_one :amazon_key, dependent: :destroy
  has_one :report_bucket, dependent: :destroy

  def create_token
    generate_token("token", 12)
    self.save
  end

  def generate_token(column, length = 64)
	  begin
	    self[column] = SecureRandom.urlsafe_base64 length
      self[column] = self[column].gsub(/[^0-9a-z]/i, '').gsub(" ", "_").downcase
	  end while User.exists?(column => self[column])
  end

  def get_s3
    Aws.config.update({
                          region: 'us-east-1',
                          credentials: Aws::Credentials.new(amazon_key.access_key, amazon_key.secret_access_key)
                      })

    s3 = Aws::S3::Resource.new
  end

  def aws_reports_configured?
    report_bucket.present?
  end
end

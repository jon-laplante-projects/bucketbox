class Audio < ActiveRecord::Base
  belongs_to :user

  has_attached_file :file, :storage => :s3,
  :s3_credentials => Proc.new { |b| b.instance.bucket_name },
  # :s3_credentials => {:bucket => "bucketbox-audio-#{current_user.token}" },
  :s3_permissions => 'public-read'
  before_create :extract_metadata, :create_label
  # before_save :extract_metadata
  serialize :metadata
  TYPES = %W( video/x-ms-asf, audio/aiff ,  audio/x-aiff ,  audio/mpeg ,  audio/x-mpequrl ,  audio/midi ,  audio/x-mid ,  audio/x-midi ,  audio/mod ,  audio/x-mod ,  audio/x-mpeg ,  audio/mpeg3 ,  audio/x-mpeg-3 ,  audio/mp3 ,  audio/x-pn-realaudio ,  audio/voc ,  audio/x-voc ,  audio/wav ,  audio/x-wav ,  audio/xm,  audio/x-ms-wma)

  #validates_attachment_content_type :file, :content_type => ["*"], :if => :sound_attached?
  validates_attachment_content_type :file, content_type: TYPES

  def sound_attached?
      self.file.file?
  end

  def current_file_path
    file.queued_for_write[:original].path
  end

  def bucket_name
    user = User.find(self.user_id)
    amazon_key = AmazonKey.find_by(user_id: self.user_id)

    { :bucket => "bucketbox-audio-#{user.token}", :access_key_id => "#{amazon_key.access_key}", :secret_access_key => "#{amazon_key.secret_access_key}" }
  end

  def create_label
    self.label = self.file_file_name
  end

  private

  # Retrieves metadata for MP3s
  def extract_metadata
    return unless sound_attached?
    open_opts = { :encoding => 'utf-8' }
    Mp3Info.open(current_file_path, open_opts) do |mp3info|
      self.metadata = { title: mp3info.tag['title'],
                        artist: mp3info.tag['artist'],
                        album: mp3info.tag['album'],
                        year: mp3info.tag['year'],
                        comments: mp3info.tag['comments']
                      }
    end
  end

  def create_label
    self.label = self.file_file_name
  end
end

class Category < ActiveRecord::Base
  has_many :tutes
  validates :name, presence: true
end

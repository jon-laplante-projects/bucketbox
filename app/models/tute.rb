class Tute < ActiveRecord::Base
  belongs_to :category
  validates :name, :category, :description, :url_high, presence: true

  def video_high
    VideoInfo.new(url_high)
  end

  def vimeo_video_id
    video_high.video_id
  end

  def vimeo_thumbnail
    vimeo_video_json_url     = "http://vimeo.com/api/v2/video/%s.json" % vimeo_video_id
    thumbnail_image_location = JSON.parse(open(vimeo_video_json_url).read).first['thumbnail_large'] rescue nil
  end
end

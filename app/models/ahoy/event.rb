# == Schema Information
#
# Table name: ahoy_events
#
#  id         :uuid(16)         not null, primary key
#  visit_id   :uuid(16)
#  user_id    :integer
#  name       :string(255)
#  properties :text(65535)
#  time       :datetime
#  domain_id  :integer
#
# Indexes
#
#  index_ahoy_events_on_domain_id  (domain_id)
#  index_ahoy_events_on_time       (time)
#  index_ahoy_events_on_user_id    (user_id)
#  index_ahoy_events_on_visit_id   (visit_id)
#

module Ahoy
  class Event < ActiveRecord::Base
    self.table_name = "ahoy_events"

    belongs_to :visit
    belongs_to :user

    belongs_to :domain
    validates :domain, presence: true

    scope :with_domains, -> { includes(:domain) }
    scope :by_time, -> { order 'time DESC' }

    serialize :properties, JSON
  end
end

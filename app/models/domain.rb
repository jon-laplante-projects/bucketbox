# == Schema Information
#
# Table name: domains
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  description :string(255)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Domain < ActiveRecord::Base
  validates :name, presence: true, uniqueness: true
  validates :url, presence: true, uniqueness: true
  validates :site_id, presence: true, uniqueness: false

  belongs_to :site

  has_many :visits, dependent: :destroy
  has_many :ahoy_events, class_name: 'Ahoy::Event', dependent: :destroy
end

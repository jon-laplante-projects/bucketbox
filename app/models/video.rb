class Video < ActiveRecord::Base
  belongs_to :user

  # has_attached_file :file, :storage => :s3, :s3_credentials => { :bucket => "bucketbox-videos-#{current_user.token}" }, :styles => { :thumb => { :format => 'jpg', :time => 6, :rotate => false } }, :processors => [:transcoder], :s3_permissions => :public-read
  has_attached_file :file, :storage => :s3,
  :s3_credentials => Proc.new { |b| b.instance.bucket_name },
  # :s3_credentials => { :bucket => "bucketbox-videos-#{current_user.token}" },
  :styles => { #:preview => { :geometry => "268x268!", :format => 'mp4', :streaming => true },
  :thumb => { #:geometry => Proc.new do |file|
          #aspect_ratio = eval(file.file_meta)[:aspect]
          # "500x#{500/aspect_ratio.ceil}"
          #end,
        :format => 'jpg', :time => 6, :auto_rotate => false } },
  :processors => [:transcoder], :s3_permissions => 'public-read'
  before_create :extract_metadata, :create_label
  TYPES = %W( video/avi ,  video/msvideo ,  video/x-msvideo ,  video/mpeg ,  video/x-motion-jpeg ,  video/quicktime ,  video/mpeg ,  video/x-mpeg ,  video/x-mpeq2a ,  application/x-shockwave-flash ,  video/mp4 ,  video/x-flv )
  
  validates_attachment_content_type :file, content_type: TYPES

  after_post_process :rotate_image

  def bucket_name
    user = User.find(self.user_id)
    amazon_key = AmazonKey.find_by(user_id: self.user_id)

    { :bucket => "bucketbox-videos-#{user.token}", :access_key_id => "#{amazon_key.access_key}", :secret_access_key => "#{amazon_key.secret_access_key}" }
  end

  def video_attached?
    self.file.file?
  end

  def current_file_path
    file.queued_for_write[:original].path
  end

  def extract_metadata
    return unless video_attached?
    movie = FFMPEG::Movie.new(current_file_path)
    if movie.valid?
      self.metadata = { video_stream: movie.video_stream,
                      video_codec: movie.video_codec,
                      resolution: movie.resolution,
                      audio_codec: movie.audio_codec,
                      audio_stream: movie.audio_stream
                    }
    end
  end

  def rotate_image
    unless self.file_meta.blank?
      meta = eval(self.file_meta)
      if meta[:height] > meta[:width]
        thumbnail = MiniMagick::Image.new("#{self.file.url(:thumb)}")

        thumbnail.rotate "90"

        thumbnail.write "#{self.file.url(:thumb)}"
      end
    end
  end

  def create_label
    self.label = self.file_file_name
  end
end

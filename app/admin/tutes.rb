ActiveAdmin.register Tute do
  permit_params :name, :category_id, :description, :url_high

  index do
    selectable_column
    id_column
    column :name
    column :category
    column :description
    column :url_high
    column :created_at
    column :updated_at
    actions
  end

  form do |f|
    f.inputs "New tutorial" do
      f.input :name
      f.input :category, :as => :select, :collection => Category.all
      f.input :description
      f.input :url_high
    end
    f.actions
  end
end

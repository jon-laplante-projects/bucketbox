ActiveAdmin.register Template do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :name, :label, :description, :thumbnail, :directory, :template_design_id
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end

  index do
    selectable_column
    id_column
    column :name
    column :label
    column :description
    column :thumbnail
    column :directory
    column :created_at
    column :updated_at
    actions
  end

  form do |f|
    f.inputs "Template Details" do
      f.input :template_design_id
      f.input :name
      f.input :label
      f.input :description
      f.input :thumbnail
      f.input :directory
    end
    f.actions
  end

end

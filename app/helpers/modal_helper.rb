module ModalHelper

  def modal(_alias)
    result = <<-HTML
      <div class='modal fade' id="#{_alias}_modal" tabindex="-1" role='dialog' aria-labelledby="#{_alias.camelcase}ModalLabel" aria-hidden='true'>
        <div class='modal-dialog modal-lg'>
          <div class='modal-content'>
            #{ yield }
          </div>
        </div>
      </div>
    HTML
    result.html_safe
  end

  def close_modal
    result = <<-HTML
      <button aria-label='Close' class='close' data-dismiss='modal' type='button'>
        <span aria-hidden='true'>&times;</span>
      </button>
    HTML
    result.html_safe
  end

end
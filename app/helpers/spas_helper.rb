module SpasHelper

  def random_div_id
    rand(1..10000)
  end


  def site_index(site)
    return 'index.html' if File.exists?("#{site.local_path}/index.html")
    return 'index.htm' if File.exists?("#{site.local_path}/index.htm")
  end

  def find_html_files(site_id, curr_directory)
    puts "Find pages..."
    site = Site.find(site_id)

    Dir.chdir("#{curr_directory}") do
      html_files = Dir.glob("**/*.html")

      html_files.each do |file|
        puts "Writing to DB: #{file}"
        filename = file.split("/").pop

        title_html = Nokogiri::HTML( File.open(file) )
        title = title_html.css("title").text.to_s.truncate(100)

        page = site.pages.new

        page.site_id = site_id
        page.name = filename
        page.label = title
        page.url = file
        page.page = true
        page.save
      end
    end
  end
end

module ViewHelper

  def file_render(file)
    case file.class.name
      when 'Audio'
        media_class = "audio-link"
        file_name = audio_name(file)
        file_tile = audio_tile(file_name)
      when 'Image'
        media_class = "image-link"
        file_name = document_name(file)
        file_tile = image_tile(file, file_name)
      when 'Video'
        media_class = "video-link"
        file_name = document_name(file)
        file_tile = video_tile(file, file_name)
      when 'Document'
        media_class = "document-link"
        file_name = document_name(file)
        file_tile = document_tile(file_name)
    end

    "<div class=\"media_overlay #{media_class}\" data-id=\"#{file.id}\" style=\"max-width: 130px; max-height: 130px; text-align: center; font-size: 10pt;\">
      <a href=\"javascript:void(0);\" style=\"display: block;\">
        #{file_tile}
      </a>
    </div>".html_safe
    # "<div class=\"media_overlay #{media_class}\" data-id=\"#{file.id}\" style=\"width: 120px; text-align: center;\">
    #   <a href=\"javascript:void(0);\" style=\"display: block;\">
    #     #{file_tile}
    #   </a>
    # </div>".html_safe
  end

  def audio_tile(file_name)
    "#{image_tag "audio_placeholder.png", style: "max-width: 130px; max-height: 130px;"}<div class=\"desc\" style=\"font-size: 10pt;\">#{file_name}</div>"
    # "#{image_tag "audio_placeholder.png", style: "width: 160px; height: 120px;"}<div class=\"desc\">#{file_name}</div>"
  end

  def image_tile(file, file_name)
    "#{image_tag file.file.url(:thumb), style: "max-width: 130px; max-height: 130px;"}"
    #"#{image_tag file.file.url(:thumb), style: "border: none; max-width: 120px; max-height: 200px;"}<div style=\"padding: 2px 2px 2px 5px;\" class=\"desc\">#{file_name}</div>"
  end

  def video_tile(file, file_name)
    "#{image_tag file.file.url(:thumb), style: "max-width: 130px; max-height: 130px;"}"
    # "#{image_tag file.file.url(:thumb), style: "width: 200px;"}<div class=\"desc\">#{file_name}</div>"
  end

  def document_tile(file_name)
    "#{image_tag "files_icon.svg", width: "100", style: "border: none;"}<div style=\"padding: 2px 2px 2px 5px; max-height: 130px; max-width: 130px; font-size: 10pt;\" class=\"desc\">#{file_name}</div>"
    # "#{image_tag "files_icon.svg", width: "120", style: "border: none;"}<div style=\"padding: 2px 2px 2px 5px;\" class=\"desc\">#{file_name}</div>"
  end

  def audio_name(file)
    begin
      meta = eval(file.metadata)
      name = "#{meta["title"]}"
    rescue
      document_name(file)
    end
  end

  def document_name(file)
    extension = file.file_file_name.split(".").last
    if file.file_file_name.size >= 12
      file_name = file.file_file_name.truncate(12).concat(extension)
    else
      file_name = file.file_file_name
    end
  end

  def total_hosted_data
    if report_exists?
      number_to_human_size(current_user.report_bucket.reports.last.total_hosted_data)
    end
  end

  def current_month_spend
    if report_exists?
      number_to_currency(current_user.report_bucket.reports.last.current_month_spend)
    end
  end

  def spent_by_service
    if report_exists?
      #spent_by_service = JSON.parse(current_user.report_bucket.reports.last.spend_by_service)
      spent_by_service = JSON.parse(current_user.report_bucket.reports.last.spend_by_service).map { |key,value| [key, value.to_f.round(2)] }
      spent_by_service.each do |s|
        s[1] = 0.01 if s[1] < 0.01
      end

      pie_chart(spent_by_service, library: {pieHole: 0.5})
    end
  end

  def report_exists?
    current_user.report_bucket && current_user.report_bucket.reports.count > 0
  end

end

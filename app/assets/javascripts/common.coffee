window.download = (data, filename)->
  blob = new Blob([data])
  link = document.createElement('a')
  link.href = window.URL.createObjectURL(blob)
  link.download = filename
  link.click()
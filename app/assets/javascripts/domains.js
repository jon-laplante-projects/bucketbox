var site_domains = [];

$(function(){
    $(document).on("click", "#manage_domains_btn", function(){
        $('#manage_domains_modal').modal("show");

        // Attempt to load data.
        $.ajax({
            url: "/sites/" + BucketBox.Sites.current.id + "/domains.json",
            type: "get",
            success: function(data){
                if (typeof data.domains != "undefined") {
                    site_domains = data.domains;
                } else {
                    site_domains = [];
                }

                // Initialize.
                if (site_domains.length < 1) {
                    $("#domain_manager").html("<p>You haven't yet assigned any custom domains to this website.</p>");
                } else {
                    $("#domain_manager").html("");
                    $.each(site_domains, function(){
                        var tmpl = $( $("#domain_tmpl").html() );

                        $(tmpl).attr("data-id", this.id);

                        // Set the subdomains display and edit.
                        $.each(this.subdomains, function(){
                            var sub_tmpl = $( $("#subdomain_tmpl").html() );

                            $(sub_tmpl).attr("data-id", this.id);
                            $(sub_tmpl).find(".subdomain.text").text(this.url);

                            $(tmpl).find(".subdomains-list").append( sub_tmpl );
                        });

                        // Set the main domain display and edit.
                        // Doing this after adding subdomains so the action can be picked up there, too.
                        $(".main-domain.text", tmpl).text(this.url);
                        $(".main-domain.text", tmpl).val(this.url);

                        $("#domain_manager").append(tmpl);
                    });
                }

                $("#loading_body").slideUp("fast", function(){ $("#data_body").slideDown("fast") });
            },
            error: function(data){
                $("#loading_body").slideUp("fast", function(){ $("#error_body").slideDown("fast") });
            }
        });
    });

    // Behaviors.
    // Change the display to match the edit of the currently scoped domain.
    $(document).on("keyup", ".main-domain.edit.text", function(){
        var self = this;
        var text = $(self).val();
        var container = $(self).parent().parent().parent();
        var id = $(container).attr("data-id");

        $(container).find("span.main-domain.text").text(text);

        $.each(site_domains, function(){ if( this.id === parseInt(id) ){ this.url = text } });
    });

    // Show the edit of the currently scoped domain.
    $(document).on("click", "a.main-domain.edit.show", function(){
        var container = $(this).parent().parent();

        $(container).find(".main-domain.edit").show();
        $(container).find(".main-domain.display").hide();
    });

    // Close the currently displayed domain edit.
    $(document).on("click", "button.main-domain.cancel.btn", function(){
        var container = $(this).parent().parent().parent().parent();

        $(container).find(".main-domain.edit").hide();
        $(container).find(".main-domain.display").show();
    });

    // Show the "Add domain" dialog.
    $(document).on("click", "#add_domain", function(){
        $("#add_domain_group").show();
    });

    // Add the new domain.
    $(document).on("click", "button.main-domain.add.btn", function(){
        var last_id;
        var first_domain;
        if (site_domains.length > 0) {
            first_domain = false;
            last_id = site_domains[site_domains.length - 1].id;
        } else {
            first_domain = true;
            last_id = 0;
        }

        var tmpl = $( $("#domain_tmpl").html() );
        var text = $(this).parent().parent().find("input.main-domain.add.text").val();

        // Update the data.
        site_domains.push({ id: parseInt(last_id) + 1, name: text, subdomains: [] });

        // Update the UI.
        $(tmpl).attr("data-id", parseInt(last_id) + 1);

        // Set the main domain display and edit.
        // Doing this after adding subdomains so the action can be picked up there, too.
        $(".main-domain.text", tmpl).text(text);
        $(".main-domain.text", tmpl).val(text);

        if (first_domain === false) {
            $("#domain_manager").append(tmpl);
        } else {
            $("#domain_manager").html(tmpl);
        }

        $("button.main-domain.cancel.btn").trigger("click");
    });

    // Cancel adding a new main domain.
    $(document).on("click", "button.main-domain.cancel.btn", function(){
        $(this).parent().parent().find("input.main-domain.add.text").val("");
        $(this).parent().parent().parent().hide();
    });

    // Save the list of domains.
    $(document).on("click", "#btn_save_domains", function(){
        var payload = { domains: site_domains, site_id: BucketBox.Sites.current.id };

        $.ajax({
            url: "/sites/create_or_update_domain",
            type: "POST",
            datatype: "JSON",
            data: payload,
            async: true,
            success: function(data){

            },
            error: function(data){

            }
        });
    });

    // Show the "Add subdomain" dialog.
    $(document).on("click", "button.subdomain.add", function(){
        var container = $(this).parent();
        var parent_container = $(this).parent().parent();
        var id = $(container).attr("data-id");

        $(container).find("div.add-subdomain-container").show();
    });

    // Add new subdomain to the current scope.
    $(document).on("click", "button.subdomain.add.save", function(){
        var self = this;
        var container = $(self).parent().parent().parent();
        var parent_container = $(self).parent().parent().parent().parent().parent();
        var parent_id = $(parent_container).attr("data-id");
        var new_id;
        var new_name = $(self).parent().parent().find("input.subdomain.add.edit").val();

        // Update the data.
        $.each(site_domains, function(idx, domain){
            if( domain.id === parseInt(parent_id) ){
                // Let's generate a temporary ID.

                var sub_arr_len = domain.subdomains.length;
                var last_id;

                if (sub_arr_len > 0) {
                    last_id = domain.subdomains[sub_arr_len - 1].id;
                } else {
                    last_id = 0;
                }

                new_id = parseInt(last_id) + 1;

                // Push the new record into the array.
                domain.subdomains.push({ id: new_id, name: new_name });
            }
        });

        // Update the GUI.
        var sub_tmpl = $( $("#subdomain_tmpl").html() );

        $(sub_tmpl).attr("data-id", new_id);
        $(sub_tmpl).find(".subdomain.text").text(new_name);

        $(sub_tmpl).find(".main-domain.text").text( $(parent_container).find("span.main-domain.text").first().text() );

        $("#domain_manager").append(sub_tmpl);

        $(container).parent().find(".subdomains-list").append(sub_tmpl);

        // Hide the "add subdomain" and clear the input.
        $(container).find("button.add.subdomain.cancel").trigger("click");
    });

    // Cancel adding a new subdomain to the current scope.
    $(document).on("click", "button.add.subdomain.cancel", function(){
        var container = $(this).parent().parent().parent();

        // Clear the input.
        $(container).find("input.subdomain.add.edit").val("");

        $(container).hide();
    });

    // Delete the currently scoped domain.
    $(document).on("click", "a.main-domain.remove", function(){
        var self = this;
        var container = $(self).parent().parent();
        var id = $(container).attr("data-id");

        // Remove data.
        $.each(site_domains, function(idx, domain){
            if( domain.id === parseInt(id) )
            {
                site_domains.splice(idx, 1);
                return false;
            }
        });

        // Update UI.
        if (site_domains.length < 1) {
            $("#domain_manager").html("<p>You haven't yet assigned any custom domains to this website.</p>");
        } else {
            $(container).remove();
        }
    });

    // Delete the currently scoped subdomain.
    $(document).on("click", ".subdomain.remove", function(){
        var container = $(this).parent().parent();
        var parent_container = $(container).parent().parent().parent().parent();
        var id = $(container).attr("data-id");
        var parent_id = $(parent_container).attr("data-id");

        // Remove from GUI.
        $(container).remove();

        // Remove from hashtable.
        $.each(site_domains, function(idx, domain){
            if(domain.id === parseInt(parent_id)){
                $.each(domain.subdomains, function(key, sub){
                    if(sub.id === parseInt(id)){
                        site_domains[idx].subdomains.splice(key, 1);
                        return false;
                    }
                });
            }
        });
    });
});
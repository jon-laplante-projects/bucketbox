// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require bootstrap.min
//= require dropzone.min
//= require smoke.min
//= require moment
//= require jquery.zclip.min
//= require jQueryRotate
//= require froala_editor.min
//= require Chart
//= require excanvas
//= require zeroclipboard
//= require bootstrapValidator.min
// require formvalidation.min
//= require jquery-file-download
//= require plugins/block_styles.min
//= require plugins/colors.min
//= require plugins/media_manager.min
//= require plugins/tables.min
//= require plugins/video.min
//= require plugins/font_family.min
//= require plugins/font_size.min
//= require plugins/file_upload.min
//= require plugins/lists.min
//= require plugins/char_counter.min
//= require plugins/fullscreen.min
//= require plugins/urls.min
//= require plugins/inline_styles.min
//= require plugins/entities.min
//= require registration
//= require bootstrap-select
//= require ahoy
//= require video
//= require vjs.vimeo
//= require handlebars
//= require_tree ./templates
//= require clipboard
//= require domains
//= require router
//= require_tree .


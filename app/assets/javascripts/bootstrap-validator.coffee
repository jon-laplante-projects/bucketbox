$(document).ready ->
  $.fn.bootstrapValidator.i18n.uri = $.extend $.fn.bootstrapValidator.i18n.uri || {},
    'default': 'Please enter a valid URL'
  $('form.bs-valid').bootstrapValidator()
  $('form.bs-valid').find('input,textarea,select').filter('[required]').each (i, el)->
    $(el).closest('form').find('*[type="submit"]').prop('disabled', $(el).val() == '')

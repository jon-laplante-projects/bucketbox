Path.map("#/home").to(function(){
    $("a[href='#home']").tab("show");
});

Path.map("#/media").to(function(){
    $("a[href='#media']").tab("show");
});

Path.map("#/buckets").to(function(){
    $("a[href='#buckets']").tab("show");
});

Path.map("#/files").to(function(){
    $("a[href='#files']").tab("show");
});

Path.map("#/domains").to(function(){
    $("a[href='#domains']").tab("show");
});

Path.map("#/sites").to(function(){
    $("a[href='#sites']").tab("show");
});

Path.map("#/tutes").to(function(){
    $("a[href='#tutes']").tab("show");
});

Path.map("#/cloud").to(function(){
    $("a[href='#cloud']").tab("show");
});

$(function(){
    Path.listen();

    $('a[href="#home"]').click(function(){
        document.location.hash = "/home";
    });

    $('a[href="#media"]').click(function(){
        document.location.hash = "/media"
    });

    $('a[href="#buckets"]').click(function(){
        document.location.hash = "/buckets";
    });

    $('a[href="#files"]').click(function(){
        document.location.hash = "/files";
    });

    $('a[href="#domains"]').click(function(){
        document.location.hash = "/domains";
    });

    $('a[href="#sites"]').click(function(){
        document.location.hash = "/sites";
    });

    $('a[href="#tutes"]').click(function(){
        document.location.hash = "/tutes";
    });

    $('a[href="#cloud"]').click(function(){
        document.location.hash = "/cloud";
    });
});
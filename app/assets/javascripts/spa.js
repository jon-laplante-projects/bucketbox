var BucketBox = BucketBox || {};

BucketBox.Sites = {
  init: function(){
    $.ajax({
      url: "sites/list.json",
      type: "get",
      contentType: "application/ajax",
      dataType: "json",
      data: null,
      success: function(data){
        BucketBox.Sites.collection = data["sites"];

        // Update the site tables.
      },
      error: function(data){
        //$.smkAlert({text: "Could not initialize BucketBox data.<p/>Check your internet connection.", type: "danger", time: 3})
      }
    });
  },
  collection: Array(),
  // addSite: function(id, template_id, template_label, template_thumbnail, directory, created_at, label, pages, import_data, processing_event, processing_msg){
  //   var new_site_hash = { mode: "edit", domain: null };
  //   var template_hash = {};
  //   template_hash["id"] = template_id;
  //   template_hash["label"] = template_label;
  //   template_hash["thumbnail"] = template_thumbnail;

  //   new_site_hash["id"] = id,
  //   new_site_hash["template"] = template_hash;
  //   new_site_hash["label"] = label;
  //   new_site_hash["directory"] = directory;
  //   new_site_hash["created_at"] = moment(created_at).format("MMMM Do, YYYY");
  //   new_site_hash["pages"] = pages;
  //   new_site_hash["import_data"] = import_data;
  //   new_site_hash["processing_event"] = processing_event;
  //   new_site_hash["processing_msg"] = processing_msg;

  //   BucketBox.Sites.collection.push(new_site_hash);

  //   // make current...
  //   BucketBox.Sites.setCurrent(id, BucketBox.Sites.setDetails());

  //   update_site_UI("add", id);
  // },
  setCurrent: function(id, callback){
    BucketBox.Sites.init();
    var selected = $.grep(BucketBox.Sites.collection, function(coll){ return coll.id == id });

    BucketBox.Sites.current.id = selected[0].id;

    if (selected[0].template != null) {
      BucketBox.Sites.current.template.id = selected[0].template.id;
      BucketBox.Sites.current.template.label = selected[0].template.label;
      BucketBox.Sites.current.template.thumbnail = selected[0].template.thumbnail;
    } else {
      BucketBox.Sites.current.template.id = null;
      BucketBox.Sites.current.template.label = null;
      BucketBox.Sites.current.template.thumbnail = null;
    };

    BucketBox.Sites.current.label = selected[0].label;

    var ga_code;
    if ( selected[0].ga_code === null ) {
      ga_code = "No analytics code."
    } else {
      ga_code = selected[0].ga_code;
    };

    BucketBox.Sites.current.ga_code = ga_code;
    BucketBox.Sites.current.mode = selected[0].mode;
    BucketBox.Sites.current.directory = selected[0].directory;
    BucketBox.Sites.current.created_at = selected[0].created_at;
    BucketBox.Sites.current.domain = selected[0].domain;
    BucketBox.Sites.current.pages = selected[0].pages;
    BucketBox.Sites.current.processing_event = selected[0].processing_event;
    BucketBox.Sites.current.processing_msg = selected[0].processing_msg;

    // var page = {};
    // var payload = {};
    // payload["site_id"] = BucketBox.Sites.current.id;

    // $.ajax({
    //   url: "site/pages",
    //   type: "post",
    //   data: payload,
    //   success: function(data){
    //     $.each(data, function(key, val){
    //       page["label"] = val.label;
    //       page["url"] = val.url;
    //     });

    //     BucketBox.Sites.current.pages.push(page);
    //   },
    //   error: function(data){
    //     $.smkAlert({text: "Could not initialize BucketBox data.<p/>Check your internet connection.", type: "danger", time: 3})
    //   }
    // });

    if (typeof callback === "function") {
      callback();
    };
  },
  nullCurrent: function(){
    // Reset the current site.
    BucketBox.Sites.current.id = null;
    BucketBox.Sites.current.template.id = null;
    BucketBox.Sites.current.template.label = null;
    BucketBox.Sites.current.template.thumbnail = null;
    BucketBox.Sites.current.label = null;
    BucketBox.Sites.current.ga_code = null;
    BucketBox.Sites.current.mode = null;
    BucketBox.Sites.current.directory = null;
    BucketBox.Sites.current.created_at = null;
    BucketBox.Sites.current.domain = null;
    BucketBox.Sites.current.import_data = null;
    BucketBox.Sites.current.pages = [];
    BucketBox.Sites.current.processing_event = null;
    BucketBox.Sites.current.processing_msg = null;
  },
  deleteCurrent: function(){
    // Remove the deleted item from the collection.
    BucketBox.Sites.collection = $.grep(BucketBox.Sites.collection, function(val) {
      return val != BucketBox.Sites.current.id;
    });

    BucketBox.Sites.nullCurrent();

    update_site_UI("remove", null);
  },
  setDetails: function(){
    var pages = [];

    // Unless there aren't any, iterate pages and set up the list.
    if ( BucketBox.Sites.current.pages != null ) {
      $.each(BucketBox.Sites.current.pages, function(key, val){
        var url;
        if (val.url == "index.html" || val.url == "index.htm") {
          url = "";
        } else {
          url = val.url;
        };

        pages.push("<li><a href='javascript:void(0);' title='Duplicate This Page' data-page-id='" + val.id + "' data-label='" + val.label + "' class='clone-page'><span class='fa fa-files-o'></span></a>&nbsp;&nbsp;<a href='/" + BucketBox.Sites.current.directory + "/" + url + "' target='_BLANK' title='" + val.name + "'>" + val.label + "</a>&nbsp;&nbsp;<a href='javascript:void(0);' title='Delete This Page' data-page-id='" + val.id + "' class='delete-page'><span class='fa fa-times'></span></a></li>");
      });
    } else {
      pages.push("<li>Pages have not yet been processed for this site.</li>");
    };

    $(".site-details-panel").html( $("#site_details_tmpl").html() );

    $(".pnl-site-name").text( BucketBox.Sites.current.label );
    $("#site_name").val( BucketBox.Sites.current.label );

    $(".pnl-site-ganalytics").text( BucketBox.Sites.current.ga_code );
    $("#site_ganalytics").val( BucketBox.Sites.current.ga_code );

    $(".site-domain").html( '<a href="' + BucketBox.Sites.current.domain + '" target="_BLANK">' + BucketBox.Sites.current.domain + '</a>' );
    $(".site-visits").text( "1,234" );
    $(".site-created-at").text( BucketBox.Sites.current.created_at );
    $(".pnl-site-pages").html( pages );
    if (BucketBox.Sites.current.template.thumbnail != null) {
      $(".selected-template-img").attr("src", BucketBox.Sites.current.template.thumbnail );
      //$(".selected-template-img").attr("src", "assets/" + BucketBox.Sites.current.template.thumbnail );
    } else {
      $(".selected-template-img").attr("src", "assets/no_template.jpg");
      //$(".selected-template-img").attr("src", "assets/no_template.jpg");
    };


    //$(".selected-template-img").attr("src", "assets/images/ " + BucketBox.Sites.current.template.thumbnail );
    $(".selected-template-img").attr("alt", BucketBox.Sites.current.template.label);

    // If this site has a domain, make the "Unpublish" button active.
    // If this site doesn't have a domain, make the "Publish" button active.
    if ( BucketBox.Sites.current.domain == null ) {
      $('.pnl-btn.publish-site').attr("disabled", false);
      $('.pnl-btn.publish-site').show();
      $('.pnl-btn.unpublish-site').attr("disabled", "disabled");
        $('.pnl-btn.unpublish-site').hide();
    } else {
      $('.pnl-btn.unpublish-site').attr("disabled", false);
      $('.pnl-btn.unpublish-site').show();
      $('.pnl-btn.publish-site').attr("disabled", "disabled");
        $('.pnl-btn.publish-site').hide();
    };



    // Processing status.
    if ( BucketBox.Sites.current.processing_event == null || BucketBox.Sites.current.processing_event == "complete" ) {
      $(".site-process-status").alert("close");
    } else {
      $(".site-process-status").css("display", "block");
      $("#site_process_status_msg").html( BucketBox.Sites.current.processing_msg );
    };
  },
  setDomains: function(site_id) {
      BucketBox.Sites.domains.get_domains(site_id)
  },
  domains: {
      init: function(){

      },
      id: null,
      main: null,
      collection: [],
      current: {},
      domain: null,
      subdomains: [],
      subdomain_form: null,
      subdomain_form2: null,
      get_domains: function(site_id){
          $.ajax({
              url: "/sites/" + site_id + "/domains.json",
              type: "POST",
              contentType: "application/ajax",
              dataType: "json",
              data: null,
              success: function(data){
                if (typeof data.domains != 'undefined') {

                    BucketBox.Sites.domains.id = data.domains.id
                    BucketBox.Sites.domains.main = true
                    BucketBox.Sites.domains.domain = data.domains.url
                    BucketBox.Sites.domains.subdomains = data.subdomains
                    BucketBox.Sites.domains.subdomain_form2 =  $('#subdomains_info .subdomain.edit:first').clone();

                    set_updated_main_domain_text('p.main-domain.display', data.domains.url)
                    $('#main_domain_text').val(data.domains.url)
                    if (data.subdomains.length > 0 && data.subdomains != null) {
                        $('#subdomains_info').show()
                        for (i = 0; i < data.subdomains.length; i++) {
                            add_subdomain_form(data.subdomains[i]);
                        }
                        $('#subdomains_list .subdomain.edit').last().remove();
                    }


                    $('#subdomains_info').show()
                    $('.subdomain_without_id:first').find('span.main-domain').text(data.domains.url)
                    $('.subdomain_without_id:first').show()
                }
              },
              error: function(data){
                  var data = JSON.parse(data.responseText)
                  $.smkAlert({text: "Houston, we have a problem!<br/>" + data.message, type: "warning", time: 3});
              }
          });
      },
      remove_domains: function(domain_id){
          $.ajax({
              url: "/sites/" + domain_id + "/delete_domain",
              type: "GET",
              contentType: "application/ajax",
              dataType: "json",
              data: null,
              success: function(data){
                  $.smkAlert({text: "Domain was deleted with success", type: "success", time: 3});
              },
              error: function(data){
                  var data = JSON.parse(data.responseText)
                  $.smkAlert({text: "Houston, we have a problem!<br/>" + data.message, type: "warning", time: 3});
              }
          });
      },
      set_domains: function(site_id, is_main, domain_id, domain, div){
        var payload = {site_id: site_id, is_main: is_main, domain_id: domain_id, domain: domain};
          $.ajax({
            url: "sites/create_or_update_domain",
            type: "POST",
            data: payload,
            async: true,
            success: function(data){
                if (is_main) {
                    $('#' + div).hide()
                    BucketBox.Sites.domains.id = data.domain_id || null;
                    set_updated_main_domain_text('p.main-domain.display', domain)
                    if ($('.subdomain_without_id:visible').length == 0) {
                        $('#subdomains_info').show()
                        $('.subdomain_without_id:first').show()
                    }
                    update_all_subdomains_text('p.subdomain.display', domain)
                    if (BucketBox.Sites.domains.id == null) {
                        BucketBox.Sites.domains.id = data.domain_id
                        BucketBox.Sites.domains.domain = domain
                    }
                    $('p.main-domain.display').show()
                } else {
                    $('#' + div).find('div.input-group').hide()
                    add_subdomain(data.domain_id, domain)
                    set_updated_subdomain_text('#' + div + ' p.subdomain.display', BucketBox.Sites.domains.domain)
                    $('#' + div + ' #subdomain_name').attr('data-id', data.domain_id)
                    $('#' + div).attr('id', 'subdomain_' + data.domain_id)
                    $('#subdomain_' + data.domain_id).removeClass('subdomain_without_id')
                }
            },
            error: function(data){
                var data = JSON.parse(data.responseText)
                $.smkAlert({text: "Houston, we have a problem!<br/>" + data.message, type: "warning", time: 3});
            }
        });
      }
  },
  current: {
    id: null,
    template: {
      id: null,
      label: null,
      thumbnail: null
    },
    label: null,
    ga_code: null,
    mode: null,
    directory: null,
    created_at: null,
    domain: null,
    pages: [],
    processing_event: null,
    processing_msg: null
  },
  import_data: null
};

function bytesToSize(bytes) {
  var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
  if (bytes == 0) return 'n/a';
  var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
  if (i == 0) return bytes + ' ' + sizes[i];
  return (bytes / Math.pow(1024, i)).toFixed(1) + ' ' + sizes[i];
};

BucketBox.Media = {
    has_images_bucket: null,
    has_videos_bucket: null,
    has_audio_bucket: null,
    init: function () {
        // Determine whether we've got buckets.
        var buckets = {};

        $.getJSON("/bucket/status", function (data) {
            BucketBox.Media.has_images_bucket = data["has_images_bucket"];
            BucketBox.Media.has_videos_bucket = data["has_videos_bucket"];
            BucketBox.Media.has_audio_bucket = data["has_audio_bucket"];
        });

        // Populate the images collection.
        BucketBox.Media.Images.updateCollection();

        // Populate the videos collection.
        BucketBox.Media.Videos.updateCollection();

        // Populate the audio collection.
        BucketBox.Media.Audios.updateCollection();
    },
    Images: {
        collection: Array(),
        pagination_coll: Array(),
        current: {
            id: null,
            file_name: null,
            dimensions: null,
            filetype: null,
            bucket_id: null,
            bucket_name: null,
            created_at: null,
            original_size_url: null,
            medium_size_url: null,
            thumbnail_size_url: null
        },
        addImage: function () {

        },
        updateCollection: function () {
            // Populate the images collection.
            $.ajax({
                url: "/images/list.json",
                type: "post",
                contentType: "application/ajax",
                dataType: "json",
                data: null,
                success: function (data) {
                    BucketBox.Media.Images.collection = data["images"];

                    counter = 0;

                    while (data.length) {
                        var page_hash = {num: counter, images: data.splice(0, 6)};

                        BucketBox.Media.Images.pagination_coll.push(page_hash);

                        counter++;
                    }
                    ;
                },
                error: function (data) {
                    // $.smkAlert({text: "Could not initialize BucketBox data.<p/>Check your internet connection.", type: "danger", time: 3})
                }
            });
        },
        setCurrent: function (id, callback) {
            BucketBox.Media.init();
            var selected = $.grep(BucketBox.Media.Images.collection, function (coll) {
                return coll.id == id
            });

            BucketBox.Media.Images.current.id = selected[0].id;
            BucketBox.Media.Images.current.file_name = selected[0].name;
            BucketBox.Media.Images.current.dimensions = selected[0].dimensions;
            BucketBox.Media.Images.current.fileSize = selected[0].file_size;
            BucketBox.Media.Images.current.filetype = selected[0].file_type;
            BucketBox.Media.Images.current.bucket_id = selected[0].bucket_id;
            BucketBox.Media.Images.current.bucket_name = selected[0].bucket_name;
            BucketBox.Media.Images.current.created_at = selected[0].created_at;
            BucketBox.Media.Images.current.original_size_url = selected[0].original_url;
            BucketBox.Media.Images.current.medium_size_url = selected[0].preview_url;
            BucketBox.Media.Images.current.thumbnail_size_url = selected[0].thumbnail_url;

            if (typeof callback === "function") {
                callback();
            }
            ;
        },
        deleteCurrent: function () {
            // Remove the deleted item from the collection.
            BucketBox.Media.Images.collection = $.grep(BucketBox.Media.Images.collection, function (val) {
                return val != BucketBox.Sites.current.id;
            });

            // Reset the current image.
            BucketBox.Media.Images.current.id = null;
            BucketBox.Media.Images.current.file_name = null;
            BucketBox.Media.Images.current.dimensions = null;
            BucketBox.Media.Images.current.filetype = null;
            BucketBox.Media.Images.current.bucket_id = null;
            BucketBox.Media.Images.current.bucket_name = null;
            BucketBox.Media.Images.current.created_at = null;
            BucketBox.Media.Images.current.original_size_url = null;
            BucketBox.Media.Images.current.medium_size_url = null;
            BucketBox.Media.Images.current.thumbnail_size_url = null;

            update_image_UI();
        },
        setDetails: function () {
            $(".file-details-panel").html($("#image_details_tmpl").html());

            $("#image_file_name").text(BucketBox.Media.Images.current.file_name);
            $("#image_file_size").text(bytesToSize(BucketBox.Media.Images.current.fileSize));
            $("#image_filetype").text(BucketBox.Media.Images.current.filetype);

            $("#actual_copy_size_url_to_clipboard").attr('data-clipboard-text', BucketBox.Media.Images.current.original_size_url);
            $("#actual_download_size_url_to_clipboard").attr('href', BucketBox.Media.Images.current.original_size_url);

            $("#medium_copy_size_url_to_clipboard").attr('data-clipboard-text', BucketBox.Media.Images.current.medium_size_url);
            $("#medium_download_size_url_to_clipboard").attr('href',BucketBox.Media.Images.current.medium_size_url);

            $("#thumb_copy_size_url_to_clipboard").attr('data-clipboard-text', BucketBox.Media.Images.current.thumbnail_size_url);
            $("#thumb_download_size_url_to_clipboard").attr('href', BucketBox.Media.Images.current.thumbnail_size_url);

            if (BucketBox.Media.Images.current.bucket_id != null) {
                $(".add-or-remove-image-from-bucket-btn").html('<button class="btn btn-info toggle-buckets-btn" style="width: 100%; margin-bottom: 10px;" data-media-type="image"><i class="fa fa-minus-square-o"></i>&nbsp;Remove From Bucket</button>');
                $("#image_bucket_details").html( "<ul><li>" + BucketBox.Media.Images.current.bucket_name + "</li></ul>" );
            } else {
                $(".add-or-remove-image-from-bucket-btn").html('<button class="btn btn-success toggle-buckets-btn" style="width: 100%; margin-bottom: 10px;" data-media-type="image"><i class="fa fa-plus-square-o"></i>&nbsp;Add To Bucket</button>');
                $("#image_bucket_details").html("");
            }
        }
    },
    Videos: {
        collection: Array(),
        current: {
            id: null,
            file_name: null,
            label: null,
            bucket_id: null,
            bucket_name: null,
            dimensions: null,
            height: null,
            width: null,
            aspect: null,
            // tall: null,
            file_type: null,
            file_size: null,
            length: null,
            duration: null,
            created_at: null,
            original_size_url: null,
            // preview_size_url: null,
            thumbnail_size_url: null
        },
        setCurrent: function (id, callback) {
            BucketBox.Media.init();
            var selected = $.grep(BucketBox.Media.Videos.collection, function (coll) {
                return coll.id == id
            });

            BucketBox.Media.Videos.current.id = selected[0].id;
            BucketBox.Media.Videos.current.file_name = selected[0].file_name;
            BucketBox.Media.Videos.current.label = selected[0].label;
            BucketBox.Media.Videos.current.bucket_id = selected[0].bucket_id;
            BucketBox.Media.Videos.current.bucket_name = selected[0].bucket_name;
            BucketBox.Media.Videos.current.dimensions = selected[0].dimensions;
            BucketBox.Media.Videos.current.height = selected[0].height;
            BucketBox.Media.Videos.current.width = selected[0].width;
            BucketBox.Media.Videos.current.aspect = selected[0].aspect;
            // BucketBox.Media.Videos.current.tall = selected[0].tall;
            BucketBox.Media.Videos.current.file_type = selected[0].file_type;
            BucketBox.Media.Videos.current.file_size = selected[0].file_size;
            BucketBox.Media.Videos.current.length = selected[0].length;
            BucketBox.Media.Videos.current.duration = selected[0].duration;
            BucketBox.Media.Videos.current.created_at = selected[0].created_at;
            BucketBox.Media.Videos.current.original_size_url = selected[0].original_size_url;
            // BucketBox.Media.Videos.current.preview_size_url = selected[0].preview_size_url;
            BucketBox.Media.Videos.current.thumbnail_size_url = selected[0].thumbnail_size_url;

            if (typeof callback === "function") {
                callback();
            }
            ;
        },
        nullCurrent: function () {
            BucketBox.Media.Videos.current.id = null;
            BucketBox.Media.Videos.current.file_name = null;
            BucketBox.Media.Videos.current.label = null;
            BucketBox.Media.Videos.current.bucket_id = null;
            BucketBox.Media.Videos.current.bucket_name = null;
            BucketBox.Media.Videos.current.dimensions = null;
            BucketBox.Media.Videos.current.height = null;
            BucketBox.Media.Videos.current.width = null;
            BucketBox.Media.Videos.current.aspect = null;
            // BucketBox.Media.Videos.current.tall = null;
            BucketBox.Media.Videos.current.file_type = null;
            BucketBox.Media.Videos.current.file_size = null;
            BucketBox.Media.Videos.current.length = null;
            BucketBox.Media.Videos.current.duration = null;
            BucketBox.Media.Videos.current.created_at = null;
            BucketBox.Media.Videos.current.original_size_url = null;
            // BucketBox.Media.Videos.current.preview_size_url = null;
            BucketBox.Media.Videos.current.thumbnail_size_url = null;
        },
        setDetails: function () {
            $(".file-details-panel").html($("#video_details_tmpl").html());

            $("#video_file_name").text(BucketBox.Media.Videos.current.file_name);
            $("#video_file_size").text(BucketBox.Media.Videos.current.file_size);
            $("#video_file_type").text(BucketBox.Media.Videos.current.file_type);

            $("#actual_copy_size_url_to_clipboard").attr('data-clipboard-text', BucketBox.Media.Images.current.original_size_url);
            $("#actual_download_size_url_to_clipboard").attr('href', BucketBox.Media.Images.current.original_size_url);

            $("#thumb_copy_size_url_to_clipboard").attr('data-clipboard-text', BucketBox.Media.Videos.current.original_size_url);
            $("#thumb_download_size_url_to_clipboard").attr('href', BucketBox.Media.Videos.current.thumb_size_url);


            if (BucketBox.Media.Videos.current.bucket_id != null) {
                $(".add-or-remove-video-from-bucket-btn").html('<button class="btn btn-info toggle-buckets-btn" style="width: 100%; margin-bottom: 10px;" data-media-type="video"><i class="fa fa-minus-square-o"></i>&nbsp;Remove From Bucket</button>');
                $("#video_bucket_details").html( "<ul><li>" + BucketBox.Media.Videos.current.bucket_name + "</li></ul>" );
            } else {
                $(".add-or-remove-video-from-bucket-btn").html('<button class="btn btn-success toggle-buckets-btn" style="width: 100%; margin-bottom: 10px;" data-media-type="video"><i class="fa fa-plus-square-o"></i>&nbsp;Add To Bucket</button>');
                $("#video_bucket_details").html("");
            }
        },
        updateCollection: function () {
            // Populate the images collection.
            $.ajax({
                url: "/videos/list.json",
                type: "post",
                contentType: "application/ajax",
                dataType: "json",
                data: null,
                success: function (data) {
                    BucketBox.Media.Videos.collection = data["videos"];

                    counter = 0;

                    while (data.length) {
                        var page_hash = {num: counter, videos: data.splice(0, 6)};

                        BucketBox.Media.Videos.pagination_coll.push(page_hash);

                        counter++;
                    }
                    ;

                    return true;
                },
                error: function (data) {
                    // $.smkAlert({text: "Could not initialize BucketBox data.<p/>Check your internet connection.", type: "danger", time: 3})

                    return false;
                }
            });
        }
    },
    Audios: {
        collection: Array(),
        current: {
            id: null,
            file_name: null,
            bucket_id: null,
            bucket_name: null,
            file_content_type: null,
            file_size: null,
            file_uploaded: null,
            title: null,
            artist: null,
            album: null,
            year: null,
            comments: null,
            genre: null,
            genre_s: null,
            download_url: null
        },
        setCurrent: function (id, callback) {
            BucketBox.Media.init();
            var selected = $.grep(BucketBox.Media.Audios.collection, function (coll) {
                return coll.id == id
            });

            BucketBox.Media.Audios.current.id = selected[0].id;
            BucketBox.Media.Audios.current.file_name = selected[0].file_name;
            BucketBox.Media.Audios.current.bucket_id = selected[0].bucket_id;
            BucketBox.Media.Audios.current.bucket_name = selected[0].bucket_name;
            BucketBox.Media.Audios.current.file_content_type = selected[0].file_content_type;
            BucketBox.Media.Audios.current.file_size = selected[0].file_size;
            BucketBox.Media.Audios.current.file_uploaded = selected[0].file_uploaded;
            BucketBox.Media.Audios.current.title = selected[0].title;
            BucketBox.Media.Audios.current.artist = selected[0].artist;
            BucketBox.Media.Audios.current.album = selected[0].album;
            BucketBox.Media.Audios.current.year = selected[0].year;
            BucketBox.Media.Audios.current.comments = selected[0].comments;
            BucketBox.Media.Audios.current.genre = selected[0].genre;
            BucketBox.Media.Audios.current.genre_s = selected[0].genre_s;
            BucketBox.Media.Audios.current.download_url = selected[0].download_url;

            if (typeof callback === "function") {
               callback();
            };
        },
        deleteCurrent: function(){

        },
        nullCurrent: function () {
            BucketBox.Media.Audios.current.id = null;
            BucketBox.Media.Audios.current.file_name = null;
            BucketBox.Media.Audios.current.bucket_id = null;
            BucketBox.Media.Audios.current.bucket_name = null;
            BucketBox.Media.Audios.current.file_content_type = null;
            BucketBox.Media.Audios.current.file_size = null;
            BucketBox.Media.Audios.current.file_uploaded = null;
            BucketBox.Media.Audios.current.title = null;
            BucketBox.Media.Audios.current.artist = null;
            BucketBox.Media.Audios.current.album = null;
            BucketBox.Media.Audios.current.year = null;
            BucketBox.Media.Audios.current.comments = null;
            BucketBox.Media.Audios.current.genre = null;
            BucketBox.Media.Audios.current.genre_s = null
            BucketBox.Media.Audios.current.download_url = null;
        },
        setDetails: function () {
            $(".file-details-panel").html($("#audio_details_tmpl").html());

            $("#audio_file_name").text(BucketBox.Media.Audios.current.file_name);
            $("#audio_file_size").text(bytesToSize(BucketBox.Media.Audios.current.file_size));
            $("#audio_file_type").text(BucketBox.Media.Audios.current.file_content_type);

            $("#actual_copy_size_url_to_clipboard").attr('data-clipboard-text', BucketBox.Media.Audios.current.download_url);
            $("#actual_download_size_url_to_clipboard").attr('href', BucketBox.Media.Audios.current.download_url);


            if (BucketBox.Media.Audios.current.bucket_id != null) {
                $(".add-or-remove-audio-from-bucket-btn").html('<button class="btn btn-info toggle-buckets-btn" style="width: 100%; margin-bottom: 10px;" data-media-type="audio"><i class="fa fa-minus-square-o"></i>&nbsp;Remove From Bucket</button>');
                $("#audio_bucket_details").html( "<ul><li>" + BucketBox.Media.Audios.current.bucket_name + "</li></ul>" );
            } else {
                $(".add-or-remove-audio-from-bucket-btn").html('<button class="btn btn-success toggle-buckets-btn" style="width: 100%; margin-bottom: 10px;" data-media-type="audio"><i class="fa fa-plus-square-o"></i>&nbsp;Add To Bucket</button>');
                $("#audio_bucket_details").html("");
            }
        },
        updateCollection: function () {
            $.ajax({
                url: "/audios/list.json",
                type: "post",
                contentType: "application/ajax",
                dataType: "json",
                data: null,
                success: function (data) {
                    BucketBox.Media.Audios.collection = data["audios"];
                },
                error: function (data) {
                    // $.smkAlert({text: "Could not initialize BucketBox data.<p/>Check your internet connection.", type: "danger", time: 3})
                }
            });
        }
    }
};

  BucketBox.Buckets = {
    init: function(){
        $.ajax({
            url: "/buckets/list.json",
            type: "POST",
            async: true,
            success: function(data){
                BucketBox.Buckets.collection = data.buckets;
                BucketBox.Buckets.getBucketsSelectList();
            },
            error: function(data){
                $.smkAlert({text: "Could not load bucket data. Are you connected to the internet?.", type: "warning", time: 3});
            }
        });
    },
    new: function(label){
        var payload = { label: label };

        $.ajax({
            url: "/buckets/new",
            type: "POST",
            data: payload,
            async: true,
            success: function(data){
                if ( data["success"] = true ) {
                    $.smkAlert({text: "Bucket has been created." + data["message"], type: "info", time: 3});
                    BucketBox.Buckets.updateCollection();
                    update_buckets_UI();
                } else {
                    $.smkAlert({text: "No bucket name provided. Could not save." + data["message"], type: "warning", time: 3});
                }
            },
            error: function(data){
                $.smkAlert({text: "Could not save. Critical error.", type: "warning", time: 3});
            }
        });
    },
    collection: [],
    current: {
     id: null,
     label: null,
     items: {
         audios: [],
         images: [],
         videos: []
     }
    },
    setCurrent: function(id){
        var selected = $.grep(BucketBox.Buckets.collection, function (coll) { return coll.id == id });

        BucketBox.Buckets.current = selected[0];
        // BucketBox.Buckets.current.label = selected[0].label;

        update_buckets_UI();
    },
    nullCurrent: {
      id: null,
      label: null
    },
    updateCollection: function(){
        BucketBox.Buckets.init();
    },
    deleteCurrent: function(id){
        var payload = { id: id };

        $.ajax({
            url: "/buckets/delete",
            type: "POST",
            data: payload,
            async: true,
            success: function(data){
                if (data["success"] === true) {
                    $.smkAlert({text: "Bucket has been deleted.", type: "info", time: 3});
                } else {
                    $.smkAlert({text: "Error! Bucket could not be removed.", type: "warning", time: 3});
                }
            },
            error: function(data){
                $.smkAlert({text: "Error! Bucket could not be removed.", type: "warning", time: 3});
            }
        });

        update_buckets_UI();
    },
    getBucketsSelectList: function(){
        $(".buckets-sel-list-container").load("/buckets/select/list");
    }
  };

BucketBox.Files = {
    init: function () {
        $.getJSON("/documents/list.json", function(data){
            BucketBox.Files.collection = data.files;
        });
    },
    new: function () {

    },
    collection: [],
    current: {
      id: null,
      file_name: null,
      bucket_id: null,
      bucket_name: null,
      file_content_type: null,
      file_size: null,
      file_uploaded: null,
      download_url: null,
      metadata: null
    },
    setCurrent: function (id, callback) {
      BucketBox.Files.init();
      var selected = $.grep(BucketBox.Files.collection, function (coll) { return coll.id == id });
      console.log(selected[0]);
      BucketBox.Files.current.id = selected[0].id;
      BucketBox.Files.current.fileName = selected[0].file_name;
      BucketBox.Files.current.fileType = selected[0].file_content_type;
      BucketBox.Files.current.fileSize = selected[0].file_size;
      BucketBox.Files.current.file_uploaded = selected[0].file_uploaded;
      BucketBox.Files.current.bucket_id = selected[0].bucket_id;
      BucketBox.Files.current.bucket_name = selected[0].bucket_name;
      BucketBox.Files.current.url = selected[0].download_url;
      BucketBox.Files.current.metadata = selected[0].metadata;

      var context = { metadata: BucketBox.Files.current.metadata };
      file_details = Handlebars.partials['file/_file_details']( context );



      if (typeof callback === "function") {
          callback();
      };

    },
    setDetails: function(imageUrl) {
      $(".file-details-panel").html($("#file_details_tmpl").html());

      $("#file_file_name").text(BucketBox.Files.current.fileName);
      $("#file_file_type").text(BucketBox.Files.current.fileType);
      $("#file_file_size").text(bytesToSize(BucketBox.Files.current.fileSize));
      $("#actual_copy_size_url_to_clipboard").attr('data-clipboard-text', BucketBox.Files.current.url);
      $("#actual_download_size_url_to_clipboard").attr('href', BucketBox.Files.current.url);
      $("#file-details-section").html(file_details)

      if (BucketBox.Media.Images.current.bucket_id != null) {
          $(".add-or-remove-file-from-bucket-btn").html('<button class="btn btn-info toggle-buckets-btn" style="width: 100%; margin-bottom: 10px;"><i class="fa fa-minus-square-o"></i>&nbsp;Remove From Bucket</button>');
          $("#file_bucket_details").html( "<ul><li>" + BucketBox.Files.current.bucket_name + "</li></ul>" );
      } else {
          $(".add-or-remove-file-from-bucket-btn").html('<button class="btn btn-success toggle-buckets-btn" style="width: 100%; margin-bottom: 10px;"><i class="fa fa-plus-square-o"></i>&nbsp;Add To Bucket</button>');
          $("#file_bucket_details").html("");
      }
    },
    updateCollection: function () {
      BucketBox.Files.init();
      BucketBox.Files.applySearch();
    },

    applySearch: function(){
      var data = {
        q: {
          file_file_name_cont: $('#data_search_files').val()
        },
        style: $('button[id^="files_style_"][disabled]').attr("data-style")
      }

      if ($('#remove_bucket_filter').length > 0) {
        data.q.bucket_id_eq = $('#remove_bucket_filter').attr('data-bucket-id')
      }
      if ($('#remove_media_filter').length > 0) {
        data.media_tag = $('#remove_media_filter').attr('data-media-type')
      }
      $.ajax({
        url: "/documents/files_render",
        type: "get",
        data: data,
        success: function(data){
          $('#files_render_collection').html(data);
        },
        error: function(data){
          $.smkAlert({text: "Files wasn't loaded", type: "danger", time: 3});
        }
      })
    },
    deleteCurrent: function(){
      BucketBox.Files.collection = $.grep(BucketBox.Files.collection, function (val) {
            return val != BucketBox.Sites.current.id;
        });

        // Reset the current image.
        BucketBox.Files.current = null;
        update_file_UI();
    }
};

BucketBox.Files.init();

BucketBox.helpers = {
  title_truncate: function(filename){
    var file = filename.split(".");
    var extension = filename[filename.length - 1];
    var name;

    for (var i = 0; i < filename.length - 2; i++) {
      name += name[i];
    };


    var shortText = jQuery.trim(name).substring(0, 10)
      .split(" ").slice(0, -1).join(" ") + "...";

    return shortText + extension;
  }
};

// Init dropzone.js.
Dropzone.options.newImage = {
  init: function() {
    this.on("complete", function(file) { update_image_UI() });
  }
};

Dropzone.options.newVideo = {
  init: function() {
    this.on("complete", function(file) { update_video_UI() });
  }
};

Dropzone.options.newAudio = {
  init: function() {
    this.on("complete", function(file) { update_audio_UI() });
  }
};

Dropzone.options.newFile = {
  init: function() {
    this.on("complete", function(file) { update_file_UI() });
  }
};

Dropzone.options.upload_site_dropzone = {
  init: function() {
    this.on("addedFile", function(file) { $.smkAlert({text: "BucketBox is attempting to upload your site.", type: "info", time: 10}) });
    this.on("complete", function(file) { renderSites() });
  }
}

function add_subdomain_form(subdomain_info) {
    var elem = $('.subdomain_without_id:first').clone();
    elem.attr('id', 'subdomain_' + subdomain_info.id)
    elem.attr('style', '')
    elem.find('#subdomain_name').attr('data-id', subdomain_info.id)
    elem.find('#subdomain_name').val(subdomain_info.url)
    elem.find('.main-domain').text(subdomain_info.url + '.' + $('#main_domain_text').val())
    elem.removeClass('subdomain_without_id')
    elem.find('.remove-subdomain').attr('disabled', '')
    $('#subdomains_list').prepend(elem)
}

// Generate a unique string.
function unique_string(length, chars) {
  var mask = '';
  if (chars.indexOf('a') > -1) mask += 'abcdefghijklmnopqrstuvwxyz';
  if (chars.indexOf('A') > -1) mask += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  if (chars.indexOf('#') > -1) mask += '0123456789';
  if (chars.indexOf('!') > -1) mask += '~`!@#$%^&*()_+-={}[]:";\'<>?,./|\\';
  var result = '';
  for (var i = length; i > 0; --i) result += mask[Math.round(Math.random() * (mask.length - 1))];

  var dup = $.grep(BucketBox.Sites.collection, function(coll){ return coll.guid == result });

  if ( dup[0] != null ) {
    unique_string(length, chars);
  } else {
    return result;
  };
}

function add_subdomain(id, domain) {
    var updated = false
    for (var i = 0; i < BucketBox.Sites.domains.subdomains.length; i++) {
        if (BucketBox.Sites.domains.subdomains[i].id == id) {
            BucketBox.Sites.domains.subdomains[i]['url'] = domain
            updated = true
        }
    }
    if (!updated) {
        BucketBox.Sites.domains.subdomains.push({id: id, domain: domain})
    }
}

// Import XML file for data import.
function import_xml(){
  //var file = $("#wordpress_export_xml").val();
  var file = document.getElementById("wordpress_export_xml").files[0];
  if (file) {
      var reader = new FileReader();
      reader.readAsText(file, "UTF-8");
      reader.onload = function (evt) {
          BucketBox.Sites.import_xml = evt.target.result;
          alert("Done importing!");
      }
      reader.onerror = function (evt) {
          BucketBox.Sites.import_xml = "error reading file";
      }
  }
}

// Template pagination in new site wizard.
function paginate(paginator, curr_page) {
  $(paginator).removeClass("active");
  $(paginator + '[data-page=\'' + curr_page + '\']').addClass("active");
}

// Handle new site UI changes.
function update_site_UI(change_type, added_id){
  // Determine whether we have sites or not.
  if ( BucketBox.Sites.collection.length > 0 ) {
    // Clear the current table data.
    $(".sites-list").html( $("#sites_tbl_tmpl").html() );
    $.each(BucketBox.Sites.collection, function(key, val){
      //var new_row = $(".sites-list>table").append( $("#sites_tbl_row_tmpl").html() );

      // Add data to the newly added row.
      // var new_row = $(".site-list-tr").last();

      // If the current id is the selected row, make it active.
      var tr = '<tr class="site-list-tr" data-id="' + val.id + '">';
      if ( val["id"] == BucketBox.Sites.current.id ) {
        //$(new_row).addClass("active");
        tr = '<tr class="active site-list-tr" data-id="' + val.id + '">';
      };

      var domain = " - ";
      if ( val["domain"] != null ) {
        domain = val["domain"];
      };

      var processing = "";
      if ( val.processing_event == null || val.processing_event == "complete" || val.processing_event == "error" ) {
        processing = '';
        $(".site-process-status").alert("close");

      } else {
        processing = '';
        $(".site-process-status").css("display", "block");
      };

      var mode = val["mode"];
      switch (mode){
        case "edit":
          mode = "<i class='fa fa-times-circle'></i>";
          break;
        case "published":
          mode = "<i class='fa fa-rocket'></i>";
          break;
        default:
          mode = "<i class='fa fa-question-circle'></i>";
      }

        var table_row = tr + processing + "<td>" + val["label"] + "</td><td><a href='" + domain + "' target='_BLANK'>" + domain + "</a></td><td>" + mode + "</td><td>" + val["created_at"] + "</td></tr>";
      // var table_row = tr + processing + "<td>" + val["label"] + "</td><td><a href='" + domain + "' target='_BLANK'>" + domain + "</a></td><td>" + val["mode"] + "</td><td>" + "0" + "</td><td>" + val["created_at"] + "</td></tr>";
      $(".sites-list>table").append( table_row );
      // $(".site-list-tr:last").attr("data-id", val["id"]);
      // $(".site-name:last").text(val["label"]);

      // if ( val["domain"] == null ) {
      //   $(".site-domain:last").text(" - ");
      // } else {
      //   $(".site-domain:last").text(val["domain"]);
      // };

      // $(".site-mode:last").text(val["mode"]);
      // $(".site-visits:last").text("0");
      // $(".site-created-at:last").text(val["created_at"]);
    });

    if (change_type == "add") {
      BucketBox.Sites.setDetails();
    } else {
      if (BucketBox.Sites.current.id == null) {
        $(".site-details-panel").html("Click on a site from your list to view its details.");
      };
    };

  } else {
    $(".sites-list").html( $("#no_sites_tmpl").html() );

    if (BucketBox.Sites.current.id == null) {
      $(".site-details-panel").html("Click on a site from your list to view its details.");
    }
  };
}

// Update the file list UI.
function update_file_UI() {
  $.when($("#files_render_collection").load(location.href + " #files_render_collection")).then(function(){
    BucketBox.Files.updateCollection();
    if ( BucketBox.Files.collection.length > 0 ) {
      $.get("documents/page/1", function(data){
      //$.get("images/page/" + $(this).text(), function(data){
        $(".file-container").html( data );

        $(".file-nav .pagination li a").removeClass("active");
        $(this).addClass("active");
      });

      $.get("documents/pagination", function(data){
        $(".file-nav").html( data );
      });

      if ( BucketBox.Files.collection.length < 9 ) {
        $(".file-nav").hide();
      } else {
        $(".file-nav").show();
      }
    } else {
      // ************************************
      // ************************************
      // Need to handle the alternative here.
      // ************************************
      // ************************************
      //Click on a video from your list to view its details.
    };

    $(".file-details-panel").text("Click on an file from your list to view its details.");
  });
}

// Update the images list UI.
function update_image_UI() {
  // event.preventDefault();
  // event.stopPropagation();

  $.when( BucketBox.Media.Images.updateCollection() ).then(function(){
    if ( BucketBox.Media.Images.collection.length > 0 ) {
      BucketBox.Files.updateCollection();
      $.get("images/page/1", function(data){
      //$.get("images/page/" + $(this).text(), function(data){
        $(".image-container").html( data );

        $(".media-image-nav .pagination li a").removeClass("active");
        $(this).addClass("active");
      });

      $.get("images/pagination", function(data){
        $(".media-image-nav").html( data );
      });

      if ( BucketBox.Media.Images.collection.length < 9 ) {
        $(".media-image-nav").hide();
      } else {
        $(".media-image-nav").show();
      }
    } else {
      // ************************************
      // ************************************
      // Need to handle the alternative here.
      // ************************************
      // ************************************
      //Click on a video from your list to view its details.
    };

    $(".image-details-panel").text("Click on an image from your list to view its details.");
  });
}

function update_video_UI() {
  $.when( BucketBox.Media.Videos.updateCollection() ).then(function(){
    if ( BucketBox.Media.Videos.collection.length > 0 ) {
      // event.preventDefault();
      // event.stopPropagation();
      BucketBox.Files.updateCollection();
      $.get("videos/page/1", function(data){
        $(".video-container").html( data );

        $(".media-video-nav .pagination li a").removeClass("active");
        $(this).addClass("active");
      });

      $.get("videos/pagination", function(data){
        $(".media-video-nav").html( data );
      });
    } else {
      // ************************************
      // ************************************
      // Need to handle the alternative here.
      // ************************************
      // ************************************
      $(".video-container").html( '<p class="lead">You haven\'t uploaded any videos through BucketBox yet.</p><div style="width: 100%;" class="flex-center"><button class="btn btn-primary add-vid-btn" type="button"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;Upload Your First Video</button></div>' );
    }

    $(".video-details-panel").html("Click on a video from your list to view its details.");
  });
}

function set_updated_main_domain_text(elem_identifier, domain) {
    var elem = $(elem_identifier)
    var link = $(elem_identifier + ' a').clone().wrap('<div>').parent().html()
    var text = elem.html(link + '&nbsp;&nbsp;' + domain)
    return text
}

function set_updated_subdomain_text(elem_identifier, domain) {
    // debugger;
    var elem = $(elem_identifier)
    var final_text = elem.closest('.subdomain.edit').find('#subdomain_name').val() + '.' + domain
    elem.closest('.subdomain.edit').find('.main-domain').text(final_text)
}

function update_all_subdomains_text(elem_identifier, domain) {
    $('#manage_domains_modal').find(elem_identifier).each( function () {
        var final_text = $(this).closest('.subdomain.edit').find('#subdomain_name').val() + '.' + domain
        $(this).closest('.subdomain.edit').find('.main-domain').text(final_text)
    });
}

function update_audio_UI() {
  BucketBox.Media.Audios.updateCollection();

  if ( BucketBox.Media.Audios.collection.length > 0 ) {
    // event.preventDefault();
    // event.stopPropagation();
    BucketBox.Files.updateCollection();
    $.get("audios/page/1", function(data){
      $(".audio-container").html( data );

      $(".media-audio-nav .pagination li a").removeClass("active");
      $(this).addClass("active");
    });

    $.get("audios/pagination", function(data){
      $(".media-audio-nav").html( data );
    });
  } else {
    // ************************************
    // ************************************
    // Need to handle the alternative here.
    // ************************************
    // ************************************
  };

  $(".audio-details-panel").html("Click on an audio file from your list to view its details.");
}

function update_buckets_UI() {
    $.when( BucketBox.Buckets.updateCollection() ).then(function() {
        if (BucketBox.Buckets.collection.length > 0) {
            // event.preventDefault();
            // event.stopPropagation();

            var page = $(".buckets-nav .pagination li a.active").text() || 1;

            $.get("buckets/page/" + page, function (data) {
                $(".bucket-container").html(data);

                $(".buckets-nav .pagination li a").removeClass("active");
                $(this).addClass("active");
            });

            $.get("buckets/pagination", function (data) {
                $(".buckets-nav").html(data);
            });

            BucketBox.Buckets.getBucketsSelectList();
        } else {
            // ************************************
            // ************************************
            // Need to handle the alternative here.
            // ************************************
            // ************************************
        }

        if ( BucketBox.Buckets.collection.length < 7 ) {
            $(".buckets-nav").hide();
        } else {
            $(".buckets-nav").show();
        }

        $(".bucket-details-panel").html("Click on a bucket from your list to view its details.");
    });
}

function update_site_publication_stats(){
  // Get publication stats from the site context.
  var unpub = $('td:contains("edit")', "#sites_list").length;
  var pub = $('td:contains("published")', "#sites_list").length;

  // Replace the chart canvas element
  $('#site_pub_piechart').replaceWith('<canvas id="site_pub_piechart" width="225" height="400"></canvas>');

  // Draw the chart
  var ctx = $('#site_pub_piechart').get(0).getContext("2d");
  new Chart(ctx).Pie([
    { value: unpub,
      color: "#46BFBD",
      highlight: "#5AD3D1",
      label: "Unpublished" },
    { value: pub,
      color: "#FDB45C",
      highlight: "#FFC870",
      label: "Published" }], {animation: false});

  // Update the number of sites.
  var total_sites = parseInt(unpub) + parseInt(pub);
  if (total_sites > 0) {
    if (total_sites > 1) {
      $("#sites_count").text(total_sites + " sites.");
    } else{
      $("#sites_count").text(total_sites + " site.");
    };
  } else {
    $("#sites_count").text("No sites.");
  };

  // Update the number of sites published.
  if (pub > 0) {
    // ((@sites_deployed.to_f / @sites_count.to_f )*100)
    var deployed_percent = parseInt((parseInt(pub)/(parseInt(unpub) + parseInt(pub))) * 100);
    $("#sites_published").text(deployed_percent + "% published.");
  } else {
    $("#sites_published").text("No sites published.");
  };
}

function random_number() {
  return Math.floor((Math.random() * 10000) + 1)
}

function bucket_media_preview(media_type, id){
    if ( media_type != null && id != null ) {
        switch (media_type){
            case "image":
                var selected = $.grep(BucketBox.Media.Images.collection, function(coll){ return coll.id == id });
                var links = [];

                links.push('<li><a href="' + selected[0].original_url + '" target="_BLANK">Original</a></li>');
                links.push('<li><a href="' + selected[0].preview_url + '" target="_BLANK">Preview</a></li>');
                links.push('<li><a href="' + selected[0].thumbnail_url + '" target=_BLANK">Thumbnail</a></li>');

                selected[0].links = links;
                break;

            case "video":
                var selected = $.grep(BucketBox.Media.Videos.collection, function(coll){ return coll.id == id });
                var links = [];

                links.push('<li><a href="' + selected[0].original_size_url + '" target="_BLANK">Original Video</a></li>');
                links.push('<li><a href="' + selected[0].preview_size_url + '" target="_BLANK">Preview Video</a></li>');
                links.push('<li><a href="' + selected[0].thumbnail_size_url + '" target="_BLANK">Thumbnail JPG</a></li>');

                selected[0].thumbnail_url = selected[0].thumbnail_size_url;
                selected[0].name = selected[0].file_name;

                selected[0].links = links;
                break;

            case "audio":
                var selected = $.grep(BucketBox.Media.Audios.collection, function(coll){ return coll.id == id });
                var links = [];

                links.push('<li><a href="' + selected[0].download_url + '" target=_BLANK">Original Audio File</a></li>');

                selected[0].thumbnail_url = "/assets/audio_placeholder.png";
                selected[0].name = selected[0].file_name;

                selected[0].links = links;
                break;
        }

        return selected[0];
    } else {
    // Placeholder.
    }
}

function bucket_media_copy_to_clipboard(media_type, id){
    if ( media_type != null && id != null ) {
        switch (media_type) {
            case "image":
                var selected = $.grep(BucketBox.Media.Images.collection, function(coll){ return coll.id == id });
                var clip = new ZeroClipboard();
                clip.setText(selected[0].original_url);
                break;

            case "video":
                var selected = $.grep(BucketBox.Media.Videos.collection, function(coll){ return coll.id == id });
                var clip = new ZeroClipboard();
                clip.setText(selected[0].original_size_url);

                break;

            case "audio":
                var selected = $.grep(BucketBox.Media.Audios.collection, function(coll){ return coll.id == id });
                var clip = new ZeroClipboard( selected[0].download_url );

                break;
        }
    }
}

function bucket_media_remove_link(media_type, id){
    // Set the appropriate current object.
    switch( media_type ){
        case "image":
            BucketBox.Media.Images.setCurrent( id, null );
            break;

        case "video":
            BucketBox.Media.Videos.setCurrent( id, null );
            break;

        case "audio":
            BucketBox.Media.Audios.setCurrent( id, null );
            break;
    }

    // Set payload variables.
    var payload = { media_type: media_type, id: id };
// debugger;
    $.ajax({
        url: "buckets/remove",
        type: "POST",
        data: payload,
        async: true,
        success: function(data){
            $.smkAlert({text: "Media has been successfully removed from this bucket.", type: "success", time: 3});

            // Remove item from list.
            switch(media_type) {
                case "image":
                    $('[data-id="' + id + '"]', '.bucket-image-item').remove();
                    BucketBox.Media.Images.current.bucket_id = null;

                    // Update the collection.
                    BucketBox.Media.Images.updateCollection();

                    // Update the image details button.
                    $(".add-or-remove-image-from-bucket-btn").html('<button class="btn btn-success toggle-buckets-btn" style="width: 100%; margin-bottom: 10px;" data-media-type="image"><i class="fa fa-plus-square-o"></i>&nbsp;Add To Bucket</button>');
                    $("#image_bucket_details").html("");
                    break;

                case "video":
                    $('[data-id="' + id + '"]', '.bucket-video-item').remove();
                    BucketBox.Media.Videos.current.bucket_id = null;

                    // Update the collection.
                    BucketBox.Media.Videos.updateCollection();

                    // Update the image details button.
                    $(".add-or-remove-video-from-bucket-btn").html('<button class="btn btn-success toggle-buckets-btn" style="width: 100%; margin-bottom: 10px;" data-media-type="video"><i class="fa fa-plus-square-o"></i>&nbsp;Add To Bucket</button>');
                    $("#video_bucket_details").html("");
                    break;

                case "audio":
                    $('[data-id="' + id + '"]', '.bucket-audio-item').remove();
                    BucketBox.Media.Audios.current.bucket_id = null;

                    // Update the collection.
                    BucketBox.Media.Audios.updateCollection();

                    // Update the image details button.
                    $(".add-or-remove-audio-from-bucket-btn").html('<button class="btn btn-success toggle-buckets-btn" style="width: 100%; margin-bottom: 10px;" data-media-type="audio"><i class="fa fa-plus-square-o"></i>&nbsp;Add To Bucket</button>');
                    $("#audio_bucket_details").html("");
                    break;
            }

            BucketBox.Buckets.updateCollection();

            update_buckets_UI();

            // If there are no items in the bucket, close the modal.
            if ( $(".bucket-image-item").text() === "" && $(".bucket-video-item").text() === "" && $(".bucket-audio-item").text() ) {
            // if ( BucketBox.Buckets.current.audios.length + BucketBox.Buckets.current.images.length + BucketBox.Buckets.current.videos.length < 1 ) {
                $("#open_bucket_modal").modal("hide");
            }
        },
        error: function(data){
            $.smkAlert({text: "Could not remove media from this bucket.", type: "danger", time: 3});
        }
    });
}

$(function(){
  // Initialize data.
  BucketBox.Sites.init();
  BucketBox.Media.init();
  BucketBox.Buckets.init();

  $('[data-toggle="tooltip"]').tooltip();

  // Set options on the new site wizard.
  var new_site_options = {
    contentHeight: 550,
    contentWidth: 820,
    keyboard: true,
    submitUrl: "site/new"
  };

  // Fire when image pagination link is clicked.
  $("body").on("click", ".media-image-nav .pagination li a", function(event){
    var that = this;

    event.preventDefault();
    event.stopPropagation();

    $.get("images/page/" + $(this).text(), function(data){
      $(".image-container").html( data );

      $(".media-image-nav .pagination li a").removeClass("active");
      $(that).addClass("active");
    });
  });

  // Fire when video pagination link is clicked.
  $("body").on("click", ".media-video-nav .pagination li a", function(event){
    var that = this;

    event.preventDefault();
    event.stopPropagation();

    $.get("videos/page/" + $(this).text(), function(data){
      $(".video-container").html( data );

      $(".media-video-nav .pagination li a").removeClass("active");

      $(that).addClass("active");
    });
  });

  // Fire when audio pagination link is clicked.
  $("body").on("click", ".media-audio-nav .pagination li a", function(event){
    var that = this;

    event.preventDefault();
    event.stopPropagation();

    $.get("audios/page/" + $(this).text(), function(data){
      $(".audio-container").html( data );

      $(".media-audio-nav .pagination li a").removeClass("active");

      $(that).addClass("active");
    });
  });

    $("body").on("click", ".buckets-nav .pagination li a", function(event){
        var that = this;

        event.preventDefault();
        event.stopPropagation();

        $.get("buckets/page/" + $(this).text(), function(data){
            $(".bucket-container").html( data );

            $(".buckets-nav .pagination li a").removeClass("active");
            $(that).addClass("active");
        });
    });

  $(".domain-link").click(function(){
    $("#tab_links a[href='#domains']").tab("show");
  });

  // Add site wizard - template pagination button click handler.
  $(".template-paginator-btn").click(function(){
    $(".template-paginator-btn").removeClass("active");
    $(this).addClass("active");
  });

  function validateAwsCredentials(access_key, secret_key) {
    if (access_key.length == 20 && secret_key.length == 40) {
      return true;
    }

    if (access_key.length == 40 && secret_key.length == 20) {
      $.smkAlert({text: "Amazon S3 keys are not valid. You seem to mix up S3 Access Key and Secret Access Key.", type: "warning", time: 4});
      return false;
    }

    if (access_key.length != 20 && secret_key.length == 40) {
      $.smkAlert({text: "S3 Access Key is not valid.", type: "warning", time: 4});
      return false;
    }

    if (access_key.length == 20 && secret_key.length != 40) {
      $.smkAlert({text: "Secret Access Key is not valid.", type: "warning", time: 4});
      return false;
    }

    $.smkAlert({text: "Amazon S3 keys is not valid.", type: "warning", time: 4});
    return false;
  }

  // AWS credentials.
  $("#save_aws_credentials_btn").click(function(){
    // Don't forget to add validation.
    if ( $("#s3_access_key").val() != null && $("#secret_access_key").val() != null ) {
      var payload = {};
      payload["access_key"] = $("#s3_access_key").val();
      payload["secret_key"] = $("#secret_access_key").val();

      if (!validateAwsCredentials(payload["access_key"], payload["secret_key"])) {
        return;
      }

      payload = JSON.stringify(payload);

      $.ajax({
        url: "aws/new",
        type: "POST",
        dataType: "json",
        contentType: "application/json",
        data: payload,
        success: function(data){
          if ( data["result"]["success"] == true ) {
            $.smkAlert({text: "Verified.<br/>" + data["result"]["message"], type: "info", time: 3});

              // Enable the disabled tags.
              $("[href='#home']").attr("data-toggle", "tab");
              $("[href='#media']").attr("data-toggle", "tab");
              $("[href='#domains']").attr("data-toggle", "tab");
              $("[href='#sites']").attr("data-toggle", "tab");
          } else {
            $.smkAlert({text: "Houston, we have a problem!<br/>" + data["result"]["message"], type: "warning", time: 3});
          };

        },
        error: function(data){
          $.smkAlert({text: "Houston, we have a problem!<br/>" + data["result"]["message"], type: "warning", time: 3});
        },
        timeout: function(){
          $.smkAlert({text: "You don't appear to be connected to the internet.", type: "warning", time: 4});
        }
      });
    } else {
      $.smkAlert({text: "There appears to be a problem in either your S3 Access Key or your Secret Access Key.", type: "danger", time: 4});
    };
  });

  // Open tutes tab.
  $("body").on("click", "#dashboard .tutes-btn", function(){
    $("#tab_links a[href='#tutes']").tab("show");
  });

  // Add a new bucket.
  $("body").on("click", "#dashboard .add-bucket-btn", function(){
    $("#tab_links a[href='#buckets']").tab("show");
    $("#buckets .add-bucket-btn").click();
  });

  // Add a new site.
  $("body").on("click", ".add-site-btn", function(){
    $("#tab_links a[href='#sites']").tab("show");
    $("#site_creation_method_modal").modal("show");
  });

  // Launch the new site wizard.
  $("#new_site_btn").click(function(){
    $("#site_creation_method_modal").modal("hide");

    $("#site_wizard_modal").modal("show");
  });

  // Launch the upload site modal.
  $("#upload_site_btn").click(function(){
    $("#site_creation_method_modal").modal("hide");

    $("#upload_site_modal").modal("show");
  });

  // Launch the "import site" modal.
  $("#import_site_btn").click(function(){
    $("#site_creation_method_modal").modal("hide");

    $("#import_site_modal").modal("show");
  });

  // Launch the "import page" modal.
  $("#import_page_btn").click(function(){
    $("#site_creation_method_modal").modal("hide");

    $("#import_page_modal").modal("show");
  });

  // User has accepted responsibility for importing a site.
  $("#wont_steal_websites").change(function(){
    if ( $("#wont_steal_websites").prop("checked") == true ) {
      $("#import_site_btn").prop("disabled", false);
    } else {
      $("#import_site_btn").prop("disabled", true);
    };
  });

  // User has accepted responsibility for importing a page.
  $("#wont_steal_webpages").change(function(){
    if ( $("#wont_steal_webpages").prop("checked") == true ) {
      $("#import_page_btn").prop("disabled", false);
    } else {
      $("#import_page_btn").prop("disabled", true);
    };
  });


  var importing_status_updater = function(site_id){
    setTimeout(function(){
      $.ajax({
        url: "sites/"+ site_id +"/processing_status",
        method: 'get',
        success: function(data){
          if (data['processing_event'] == 'complete' ) {
            $.smkAlert({text: "Your page has been successfully cloned.", type: "success", time: 3});
          } else if (data['processing_event'] == 'error' ) {
            $.smkAlert({text: "Unable to clone your page.", type: "danger", time: 3});
          } else {
            return importing_status_updater(site_id);
          }

          // if status was changed, update sites list
          BucketBox.Sites.init();
          update_site_publication_stats();
        }
      });
    }, 2000);
  };

  // Import an entire website into BucketBox.
  $(".import-site").click(function(){
    $("#import_site_modal").modal("hide");

    $.smkAlert({text: "Cloning your site. This may take several minutes.", type: "info", time: 5});

      if ( $("#import_site_url").val() != null ) {
      // Validate that this is a URL.
      var payload = { url: $("#import_site_url").val(), publish_now: $("#publish_imported_site_now").prop("checked") };
      //payload["url"] = $("import_site_url").val();
      //payload = [params];
      $.ajax({
        url: "site/import",
        type: "post",
        data: payload,
        success: function(data){
          importing_status_updater(data['site_id']);
          BucketBox.Sites.init();
          update_site_publication_stats();
        },
        error: function(data){
          $.smkAlert({text: "Your site can not be cloned.", type: "danger", time: 3});
        }
      });
    } else {
      // Warn that url is invalid.
    };
  });

  // Import a webpage into BucketBox.
  $(".import-page").click(function(){
    $("#import_page_modal").modal("hide");
    if ( $("#import_page_url").val() != null ) {
      // Validate that this is a URL.
      var payload = { page_url: $("#import_page_url").val() };
      //payload["url"] = $("import_site_url").val();
      //payload = [params];
      $.smkAlert({text: "Cloning your page. This may take several minutes.", type: "info", time: 3});
      $.ajax({
        url: "page/import",
        type: "post",
        data: payload,
        success: function(data){
          importing_status_updater(data['site_id']);
          BucketBox.Sites.init();
          update_site_publication_stats();
        },
        error: function(data){
          $.smkAlert({text: "Unable to clone your page.", type: "danger", time: 3});
        }
      });
    } else {
      // Warn that url is invalid.
      $.smkAlert({text: "Could not clone your page.", type: "danger", time: 3});
    };
  });

  // Submit event for the new site wizard.
  $("#save_template_btn").click(function(){
    // Let's get the values from the form.
    var new_site_vals = {};
    var new_site_pages = [];
    var new_site_data;
    new_site_vals["title"] = $("#website_name_txt").val();
    new_site_vals["description"] = $("#website_desc_txt").val();
    new_site_vals["template"] = $("#selected_template").val();

    $(".page-item").each(function(key, val){
      if ( $(val).val() != "" ) {
        var new_site_page = {};
        var link = $(".page-link").get(key);
        var curr_page = $(".external-link-chk").get(key);
        var is_checked = $( curr_page ).prop("checked");

        new_site_page["text"] = $(val).val();
        new_site_page["link"] = $(link).val();

        if (is_checked == true) {
          new_site_page["is_page"] = false;
        } else {
          new_site_page["is_page"] = true;
        };

        new_site_pages.push( new_site_page );
      }
    });

    new_site_vals["pages"] = new_site_pages;

    new_site_data = JSON.stringify(new_site_vals);

    $.smkAlert({text: "Working on your site...", type: "info", time: 3});

    // Let's submit the values from the form.
    $.ajax({
      url: "site/new",
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify(new_site_data),
      success: function(data){
        $.smkAlert({text: "Your new site has been created.", type: "success", time: 3});
        // Clear the values from the template site form.
        $("#website_name_txt").val("");
        $("#website_desc_txt").val("");
        $("#selected_template").val("");

        // Hide the site wizard modal.
        $("#site_wizard_modal").modal("hide");
        renderSites();

        //update_site_publication_stats();
      },
      error: function(data){
        $.smkAlert({text: "WARNING<p/>Unable to create your new site.", type: "danger", time: 3})

        // Hide the site wizard modal.
        $("#site_wizard_modal").modal("hide");
      }
    });
  });

  // Create the links for the nav.
  $(".page-item").blur(function(){
    var link = $(this).val();
    //var idx =
    link = encodeURIComponent(link);

  });

  // User tried to delete the current site. Prompt to make sure it's what they want.
  $('body').on('click', '.pnl-btn.delete-site', function(){
    $("#confirm_site_delete_modal").modal("show");
  });

  // User tried to duplicate the current site.
    $(document).on("click", "#duplicate_site_btn", function(){
        var payload = { site_id: BucketBox.Sites.current.id };
        var d = new Date();
        var mo;

        // Get the month.
        var months = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];

        mo = months[d.getMonth()];

        // Update the UI.
        var table_row = $("#sites_tbl_row_tmpl");
        $(".site-name", table_row).text( BucketBox.Sites.current.label );
        $(".site-domain", table_row).text( "" );
        $(".site-mode", table_row).text( BucketBox.Sites.current.mode );
        $(".site-created-at", table_row).text( mo + " " + d.getDate() + ", " + d.getFullYear() );
        $(".sites-list>table>tbody").append();

        $.ajax({
            url: "/site/duplicate",
            type: "POST",
            data: payload,
            async: true,
            success: function(data){
                // alert
                $.smkAlert({text: "Your site has been cloned.", type: "info", time: 3});
            },
            error: function(data){
                // alert and remove row.
                $.smkAlert({text: "Error! Cannot clone this site.", type: "danger", time: 3});

                $("tr.site-list-tr").last().remove();
            }
        });
    });

  // Delete the selected and confirmed site.
  $("#delete_site_btn").click(function(){
    $("#confirm_site_delete_modal").modal("hide");

    $.smkAlert({text: "BucketBox is removing your site from Amazon S3.", type: "info", time: 3});

    var payload = { site_id: BucketBox.Sites.current.id };

    $.ajax({
      url: "site/delete",
      type: "post",
      data: payload,
      success: function(data){
        var id = BucketBox.Sites.current.id;
        BucketBox.Sites.deleteCurrent();

        if ( BucketBox.Sites.collection.size < 0 ) {
          $(".sites-list").html( $("#no_sites_tmpl").html() );
        };

        // update_site_UI("remove", null);
        $.smkAlert({text: "The selected site has been successfully deleted.", type: "success", time: 3});
        renderSites();
      },
      error: function(data){
        $.smkAlert({text: "ERROR<p/>Unable to delete the selected site.", type: "danger", time: 3});
      },
      timeout: function(data){
        $.smkAlert({text: "Connection timed out.<p/>Unable to delete the selected site.", type: "danger", time: 3});
      }
    });
  });

  // User tried to delete the current image. Prompt to make sure it's what they want.

  $(".delete-file").click(function(){
    $("#delete_file_modal").modal("hide");
    $('.file-details-panel').empty()
    $.ajax({
      url: "document/delete/" + BucketBox.Files.current.id,
      type: "post",
      success: function(data){
        if (data["success"] !== true) {
          // Alert the user that there was an error.
          $.smkAlert({text: "BucketBox was unable to remove the file from your Amazon S3 account.<p/>Has it been removed elsewhere?", type: "danger", time: 3})
        };

        BucketBox.Files.deleteCurrent();
        BucketBox.Files.updateCollection();
      },
      error: function(data){
        $.smkAlert({text: "There was a problem deleted the selected file.", type: "danger", time: 3})
      }
    });
  });

  // Delete the selected and confirmed image.
  $(".delete-image").click(function(){
    $("#delete_image_modal").modal("hide");
    $('.file-details-panel').empty()
    $.ajax({
      url: "image/delete/" + BucketBox.Media.Images.current.id,
      type: "post",
      success: function(data){
        if (data["success"] !== true) {
          // Alert the user that there was an error.
          $.smkAlert({text: "BucketBox was unable to remove the image from your Amazon S3 account.<p/>Has it been removed elsewhere?", type: "danger", time: 3})
        };
        BucketBox.Media.Images.deleteCurrent();
        BucketBox.Files.updateCollection();
      },
      error: function(data){
        $.smkAlert({text: "There was a problem deleted the selected image.", type: "danger", time: 3})
      }
    });
  });

  // User tried to delete the current video. Prompt to make sure it's what they want.


  // Delete the selected and confirmed video.
  $(".delete-video").click(function(){
    $("#delete_video_modal").modal("hide");
    $('.file-details-panel').empty()
    $.ajax({
      url: "video/delete/" + BucketBox.Media.Videos.current.id,
      type: "post",
      success: function(data){
        if (data["success"] !== true) {
          // Alert the user that there was an error.
          $.smkAlert({text: "BucketBox was unable to remove the video from your Amazon S3 account.<p/>Has it been removed elsewhere?", type: "danger", time: 3})
        };

        // Remove the video from our collection.
        // BucketBox.Media.Videos.deleteCurrent();

        // Update the display div so the video is no longer included.
        update_video_UI();
      },
      error: function(data){
        $.smkAlert({text: "There was a problem deleted the selected video.", type: "danger", time: 3})
      }
    });
  });

  // User tried to delete the current audio. Prompt to make sure it's what they want.


  // Delete the selected and confirmed audio.
  $(".delete-audio").click(function(){
    $("#delete_audio_modal").modal("hide");
    $('.file-details-panel').empty()
    $.ajax({
      url: "audio/delete/" + BucketBox.Media.Audios.current.id,
      type: "post",
      success: function(data){
        if (data["success"] !== true) {
          $.smkAlert({text: "BucketBox was unable to remove the audio file from your Amazon S3 account.<p/>Has it been removed elsewhere?", type: "danger", time: 3})
        };

        // Remove the audio from our collection.
        BucketBox.Media.Audios.deleteCurrent();

        // Update the display div so the audio file is no longer included.
        update_audio_UI();
      },
      error: function(data){
        $.smkAlert({text: "There was a problem deleted the selected sound file.", type: "danger", time: 3})
      }
    });
  });

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // TODO: Refactor this into a single function.
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Rotate the video thumbnail image counter-clockwise.
  $("body").on("click", ".rotate-thumbnail-counter", function(){
    $("#video_details_tag").rotate({angle:-90});
    $(".rotate-thumbnail-counter").attr("disabled", "disabled");
    $(".rotate-thumbnail-clockwise").attr("disabled", "disabled");

    var payload = { video_id: BucketBox.Media.Videos.current.id, direction: "counter" };

    $.ajax({
      url: "video/thumbnail/rotate",
      type: "POST",
      data: payload,
      success: function(data){
        BucketBox.Media.Videos.updateCollection();

        BucketBox.Media.Videos.setDetails();

        update_video_UI();
      },
      error: function(data){
        $.smkAlert({text: "Unable to save thumbnail rotation changes.", type: "danger", time: 3});
      }
    });
  });

  // Rotate the video thumbnail image clockwise.
  $("body").on("click", ".rotate-thumbnail-clockwise", function(){
    $("#video_details_tag").rotate({angle:90});
    // $(".video-link[data-id='" + BucketBox.Media.Videos.current.id + "']").rotate({angle:90});

    $(".rotate-thumbnail-counter").attr("disabled", "disabled");
    $(".rotate-thumbnail-clockwise").attr("disabled", "disabled");

    var payload = { video_id: BucketBox.Media.Videos.current.id, direction: "clockwise" };

    $.ajax({
      url: "video/thumbnail/rotate",
      type: "POST",
      data: payload,
      success: function(data){
        BucketBox.Media.Videos.updateCollection();

        $(".rotate-thumbnail-counter").attr("disabled", false);
        $(".rotate-thumbnail-clockwise").attr("disabled", false);
      },
      error: function(data){
        $.smkAlert({text: "Unable to save thumbnail rotation changes.", type: "danger", time: 3});
      }
    });
  });
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  // Push the currently selected site to S3.
  $("body").on("click", "#publish_site_btn", function(){
    var payload = { site_id: BucketBox.Sites.current.id };
      $.smkAlert({text: "Your site is being published to Amazon S3. This can take several minutes.", type: "info", time: 5});

    $.ajax({
      url: "site/publish",
      type: "post",
      data: payload,
      success: function(data){
        $.smkAlert({text: "Your site is published to Amazon S3.", type: "success", time: 3});
        $('.pnl-btn.publish-site').attr("disabled", "disabled");
        $('.pnl-btn.publish-site').hide();
        $('.pnl-btn.unpublish-site').attr("disabled", false);
        $('.pnl-btn.unpublish-site').show();

        // update_site_publication_stats();
      },
      error: function(data){
        $('.pnl-btn.publish-site').attr("disabled", false);
        $('.pnl-btn.publish-site').show();
        $('.pnl-btn.unpublish-site').attr("disabled", "disabled");
        $('.pnl-btn.unpublish-site').hide();
      }
    });
  });

  // Remove the currently selected site to S3.
  $("body").on("click", "#unpublish_site_btn", function(){
    // Give an indication that we are doing something.
    $.smkAlert({text: "Your site is being removed from Amazon S3.", type: "info", time: 3});

      var payload = { site_id: BucketBox.Sites.current.id };

    $.ajax({
      url: "site/unpublish",
      type: "post",
      data: payload,
      success: function(data){
        $.smkAlert({text: "Successfully removed from Amazon S3.", type: "success", time: 3});

        // Toggle Pub/Unpub disabled and visibility.
        $('.pnl-btn.publish-site').attr("disabled", false);
        $('.pnl-btn.publish-site').show();
        $('.pnl-btn.unpublish-site').attr("disabled", "disabled");
        $('.pnl-btn.unpublish-site').hide();

          // Remove domain link.
          $('[data-id="' + BucketBox.Sites.current.id + '"]').find("td:nth-child(2)").text("");
          $('[data-id="' + BucketBox.Sites.current.id + '"]').find("td:nth-child(3)").text("edit");

        // update_site_publication_stats();
      },
      error: function(data){
        $.smkAlert({text: "Could not remove site from Amazon S3.", type: "warning", time: 3});

        // Toggle Pub/Unpub disabled and visibility.
        $('.pnl-btn.publish-site').attr("disabled", "disabled");
        $('.pnl-btn.publish-site').hide();
        $('.pnl-btn.unpublish-site').attr("disabled", false);
        $('.pnl-btn.unpublish-site').show();
      }
    });
  });

  // Manage a site's domain names.
    $("body").on("keyup", "#main_domain_text, #subdomain_name" , function(){
        var elem = $(this)
        var element_id = elem.attr('id')
        var button_id = '#save_domain_name'
        var parent_class = '.main-domain.edit'
        if (element_id == 'subdomain_name') {
            button_id = '#save_subdomain_name'
            parent_class = '.subdomain.edit'
        }
        var button = elem.closest(parent_class).find(button_id)
        if (elem.val() != null && elem.val() != "") {
            button.prop("disabled", false);
        } else {
            button.prop("disabled", "disabled");
        }
    });


    $("body").on("click", "#save_domain_name, #save_subdomain_name" , function(){
        var elem = $(this);
        var id = elem.attr('id');
        var is_main = BucketBox.Sites.domains.main;
        is_main = (is_main == null) || ($.isEmptyObject(is_main));
        var domain_id = BucketBox.Sites.domains.id;
        var parent = elem.closest('#main_domain_group');
        var domain = parent.find('#main_domain_text').val();
        if (id == 'save_subdomain_name') {
            parent = elem.closest('.subdomain.edit');
            var subdomain_field = parent.find('#subdomain_name');
            domain = subdomain_field.val();
            is_main = null;
            domain_id = subdomain_field.attr('data-id');
            if (typeof(domain_id) == 'undefined') {
                domain_id == null
            }
        }

        if (is_main) {
          BucketBox.Sites.domains.domain = domain;
        }
        BucketBox.Sites.domains.set_domains(BucketBox.Sites.current.id,
                                            is_main,
                                            domain_id,
                                            domain,
                                            parent.attr('id'));
        elem.attr('disabled', 'disabled');
    });

    $('body').on('click', '.close-domain-name, .toggle-subdomain-name', function(){
        var elem = $(this)
        if (elem.attr('class').indexOf('close-domain-name') + 1) {
            $('#main_domain_group').hide();
            $('p.main-domain.display').show();
        } else {
            var parent = elem.closest('.subdomain.edit');
            parent.find('.subdomain-editor').hide();
        }
    });

    // Toggle domain editing.
    $("body").on("click", ".remove-subdomain", function(){
        var form = $(this).closest('.subdomain.edit');
        var subdomain_id = form.find('#subdomain_name').attr('data-id');
        if (subdomain_id.length > 0) {
            BucketBox.Sites.domains.remove_domains(subdomain_id);
        }
        if ($('.subdomain.edit').length == 1) {
            BucketBox.Sites.domains.subdomain_form = form;
        }
        form.remove();
    });

    $("body").on("click", ".toggle-domain, .toggle-subdomain", function(){
        var elem = $(this)
        if (elem.attr('class').indexOf('toggle-domain') + 1) {
            $('#main_domain_group').show();
            $('p.main-domain.display').hide();
        } else {
            var parent = elem.closest('.subdomain.edit');
            parent.find('.subdomain-editor').show();
        }
    });

    // Toggle subdomain editing.
    $(".toggle-subdomain").click(function(){
        var parent = $(this).parent();
        $(".subdomain-editor", parent).show();
    });

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  //~~ Bucket functions.
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // "Add Bucket" button clicked.
    $(".add-bucket-btn").click(function(){
        $("#bucket_name_txt").val("");
        $("#create_bucket_modal").modal("show");
    });

    // Save a newly created bucket.
    $(".create-bucket").click(function(){
        BucketBox.Buckets.new( $("#bucket_name_txt").val() );
        $("#create_bucket_modal").modal("hide");

        update_buckets_UI();
    });

    $("body").on("click", ".bucket-link", function(){
        var bucket_id = $(this).attr("data-id");

        // Set the current bucket to the one just clicked.
        BucketBox.Buckets.setCurrent(bucket_id);

        // Calculate the number of each type of item.
        var audio_count = BucketBox.Buckets.current.audios.length;
        var image_count = BucketBox.Buckets.current.images.length;
        var video_count = BucketBox.Buckets.current.videos.length;
        // TODO: fix item_count so it accounts for documents, too.
        var item_count = parseInt(audio_count) + parseInt(image_count) + parseInt(video_count);

        // Make the bucket panel visible.
        $(".bucket-details-panel").html( $("#bucket_details").html() );

        // Set the values in the bucket panel.
        $("#bucket_name").text( BucketBox.Buckets.current.label );
        $("#bucket_item_count").text( item_count );
        $("#bucket_image_count").text( image_count );
        $("#bucket_audio_count").text( audio_count );
        $("#bucket_video_count").text( video_count );

        // Disable the open button if there is nothing in a bucket.
        if ( item_count < 1 ) {
            $(".open-bucket-btn").prop("disabled", "disabled");
        } else {
            $(".open-bucket-btn").prop("disabled", false);
        }
    });

    $("body").on("click", ".delete-bucket", function(){
        $("#delete_bucket_modal").modal("show");
    });

    // Delete a bucket.
    $("body").on("click", ".remove-bucket", function(){
        BucketBox.Buckets.deleteCurrent( BucketBox.Buckets.current.id );

        $("#delete_bucket_modal").modal("hide");

        update_buckets_UI();
    });

    // Open the selected bucket.
    $("body").on("click", ".open-bucket-btn", function(){
        var contents = '<div>';

        // Set the modal title.
        $("#openBucketModalLabel").text( BucketBox.Buckets.current.label );

        // Modal bucket contents.
        if ( BucketBox.Buckets.current.images.length > 0 ) {
            contents += "<h3>Images</h3>";
            //contents += '<div style="display: flex; align-content: center; flex-wrap: wrap;">';
            $.each(BucketBox.Buckets.current.images, function(key, val){
                contents += '<p class="bucket-image-item"><a href="javascript:void(0);" style="color: black;" data-id="' + val.id + '" data-media-type="image" class="bucket-item">' + val.file_file_name + '</a><a href="#" style="padding-left: 10px;" class="bucket-media-remove-link" data-id="' + val.id + '" data-media-type="image"><i class="fa fa-close"></i></a></p>';
            });
            //contents += '</div>';
        }
        if ( BucketBox.Buckets.current.videos.length > 0 ) {
            contents += "<h3>Videos</h3>";
            //contents += '<div style="display: flex; align-content: center; flex-wrap: wrap;">';
            $.each(BucketBox.Buckets.current.videos, function(key, val){
                contents += '<p class="bucket-video-item"><a href="#" style="color: black;" data-id="' + val.id + '" data-media-type="video" class="bucket-item">' + val.file_file_name + '</a><a href="#" style="padding-left: 10px;" class="bucket-media-remove-link" data-id="' + val.id + '" data-media-type="video"><i class="fa fa-close"></i></a></p>';
            });
            //contents += '</div>';
        }
        if ( BucketBox.Buckets.current.audios.length > 0 ) {
            contents += "<h3>Sound files</h3>";
            //contents += '<div style="display: flex; align-content: center; flex-wrap: wrap;">';
            $.each(BucketBox.Buckets.current.audios, function(key, val){
                contents += '<p class="bucket-audio-item"><a href="#" style="color: black;" data-id="' + val.id + '" data-media-type="audio" class="bucket-item">' + val.file_file_name + '</a><a href="#" style="padding-left: 10px;" class="bucket-media-remove-link" data-id="' + val.id + '" data-media-type="audio"><i class="fa fa-close"></i></a></p>';
            });
            //contents += '</div>';
        }
        contents += '</div>';
        $("#bucket_contents").html( contents );
    });

    // Show details when a bucket item is clicked.
    $("body").on("click", ".bucket-item", function(){
        // Get the currently selected media.
        var selected = bucket_media_preview($(this).attr("data-media-type"), $(this).attr("data-id"));

        var detail_template = $("#bucket_content_details_template").html();

        // Set the template and show the details section.
        $("#bucket_content_details").html( detail_template );
        $("#bucket_content_details").show();

        $("#bucket_content_name").text( selected.name );
        $("#bucket_content_details_image").html( '<img src="' + selected.thumbnail_url + '" style="max-width: 200px;" />' );
        $("#bucket_content_links_list").html( selected.links );
        $("#bucket_media_remove_btn").attr("data-id", selected.id);
        $("#bucket_media_remove_btn").attr("data-media_type", $(this).attr("data-media-type"));
        /*
        * bucket_name
        * bucket_item_count
        * bucket_image_count
        * bucket_audio_count
        * bucket_video_count
        * */
    });

    //$("body").on("click", ".bucket-media-copy-link", function(){
    //    bucket_media_copy_to_clipboard($(this).attr("data-media-type"), $(this).attr("data-id"));
    //});

    $("body").on("click", ".bucket-media-remove-link", function(){
        bucket_media_remove_link($(this).attr("data-media-type"), $(this).attr("data-id"));
    });

    $("body").on("click", ".toggle-buckets-btn", function(){
        var bucket_id;
        var media_type;
        var id;

        // Determine media type.
        switch( $(this).attr("data-media-type") ){
            case "image":
                bucket_id = BucketBox.Media.Images.current.bucket_id;
                media_type = "image";
                id = BucketBox.Media.Images.current.id;
                break;

            case "video":
                bucket_id = BucketBox.Media.Videos.current.bucket_id;
                media_type = "video";
                id = BucketBox.Media.Videos.current.id;
                break;

            case "audio":
                bucket_id = BucketBox.Media.Audios.current.bucket_id;
                media_type = "audio";
                id = BucketBox.Media.Audios.current.id;
                break;
        }

        if ( bucket_id == null ) {
            $("#add_to_bucket_modal").modal("show");
            $(".add-image-to-bucket-btn").attr("data-media-type", media_type);
        } else {
            bucket_media_remove_link(media_type, id);

            // Update the buckets interface.
            update_buckets_UI();

            // Update the collection.
            switch(media_type){
                case "image":
                    BucketBox.Media.Images.updateCollection();
                    break;

                case "video":
                    BucketBox.Media.Videos.updateCollection();
                    break;

                case "audio":
                    BucketBox.Media.Audios.updateCollection();
                    break;
            }
        }
    });

    $("body").on("click", ".add-image-to-bucket-btn", function(){
        if ($("#buckets_list option:selected").val() != null && $("#buckets_list option:selected").text() != "Select a bucket...") {
            // Get media type.
            var media_type = $(this).attr("data-media-type");
            var id = $(this).attr("data-id");

            // Determine id based on media type.
            // switch( media_type ){
            //     case "image":
            //         id = BucketBox.Media.Images.current.id;
            //         break;

            //     case "video":
            //         id = BucketBox.Media.Videos.current.id;
            //         break;

            //     case "audio":
            //         id = BucketBox.Media.Audios.current.id;
            //         break;
            // }

            var payload = {
                id: id,
                media_type: media_type,
                bucket_id: $("#buckets_list").val()
            };

            $.ajax({
                url: "buckets/add",
                type: "POST",
                data: payload,
                async: true,
                success: function (data) {
                    if (data.success) {
                      $.smkAlert({text: "Media has been added to this bucket.", type: "success", time: 3});
                    } else {
                      $.smkAlert({text: "Could not add media to this bucket.", type: "danger", time: 3});
                    }

                    // switch( media_type ){
                    //     case "image":
                    //         // Update the object.
                    //         BucketBox.Media.Images.current.bucket_id = $("#buckets_list").val();
                    //         BucketBox.Media.Images.current.bucket_name = $("#buckets_list option:selected").text();

                    //         // Update the Images collection.
                    //         BucketBox.Media.Images.updateCollection();

                    //         // Change the button.
                    //         $(".add-or-remove-image-from-bucket-btn").html('<button class="btn btn-info toggle-buckets-btn" style="width: 100%; margin-bottom: 10px;" data-media-type="image"><i class="fa fa-minus-square-o"></i>&nbsp;Remove From Bucket</button>');
                    //         $("#image_bucket_details").html("<ul><li>" + BucketBox.Media.Images.current.bucket_name + "</li></ul>");
                    //         break;

                    //     case "video":
                    //         // Update the object.
                    //         BucketBox.Media.Videos.current.bucket_id = $("#buckets_list").val();
                    //         BucketBox.Media.Videos.current.bucket_name = $("#buckets_list option:selected").text();

                    //         // Update the Videos collection.
                    //         BucketBox.Media.Videos.updateCollection();

                    //         // Change the button.
                    //         $(".add-or-remove-video-from-bucket-btn").html('<button class="btn btn-info toggle-buckets-btn" style="width: 100%; margin-bottom: 10px;" data-media-type="video"><i class="fa fa-minus-square-o"></i>&nbsp;Remove From Bucket</button>');
                    //         $("#video_bucket_details").html("<ul><li>" + BucketBox.Media.Videos.current.bucket_name + "</li></ul>");
                    //         break;

                    //     case "audio":
                    //         // Update the object.
                    //         BucketBox.Media.Audios.current.bucket_id = $("#buckets_list").val();
                    //         BucketBox.Media.Audios.current.bucket_name = $("#buckets_list option:selected").text();

                    //         // Update the Audios collection.
                    //         BucketBox.Media.Audios.updateCollection();

                    //         // Change the button.
                    //         $(".add-or-remove-audio-from-bucket-btn").html('<button class="btn btn-info toggle-buckets-btn" style="width: 100%; margin-bottom: 10px;" data-media-type="audio"><i class="fa fa-minus-square-o"></i>&nbsp;Remove From Bucket</button>');
                    //         $("#audio_bucket_details").html("<ul><li>" + BucketBox.Media.Audios.current.bucket_name + "</li></ul>");
                    //         break;
                    // }

                    // Update the buckets interface.
                    update_buckets_UI();

                    // Hide the "Add to bucket" modal.
                    $("#add_to_bucket_modal").modal("hide");
                },
                error: function (data) {
                    $.smkAlert({text: "Could not add media to this bucket.", type: "danger", time: 3});

                    update_buckets_UI();

                    $("#add_to_bucket_modal").modal("hide");
                }
            });
        } else {
            $.smkAlert({text: "Oops! You need to choose a bucket to add media to.", type: "danger", time: 3});
        }
    });

    $('#open_bucket_modal').on('hidden.bs.modal', function () {
        update_buckets_UI();
    })
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  //~~ End bucket functions.
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  //~~ Handle "Copy to clipboard" clips for images
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  $('a#img_original_cb').zclip({
  //$("body").on("zclip", "#img_original_cb", function(){
    path: "/assets/ZeroClipboard.swf",
    copy: BucketBox.Media.Images.current.original_size_url
  });

  /*$('a#img_medium_cb').zclip({
  //$("body").on("zclip", "#img_medium_cb", function(){
    path: "assets/ZeroClipboard.swf",
    copy: BucketBox.Media.current.medium_size_url
  });

  $('a#img_thumbnail_cb').zclip({
  //$("body").on("zclip", "#img_thumbnail_cb", function(){
    path: "assets/ZeroClipboard.swf",
    copy: BucketBox.Media.current.thumbnail_size_url
  });*/
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  //~~ End clipboard code.
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  // Launch the site upload wizard.
  $("#upload_site_btn").click(function(){
   $("#site_creation_method_modal").modal("hide");
   var wizard = $("#upload_site_modal").modal("show");
   wizard.show();
  });

  // Opt out of using a template on a new site.
  $("#new_site_no_template").click(function(){
    if ( $("#new_site_no_template").prop("checked") == true ) {
      $("#new_site_templates").slideUp("fast");
    } else {
      $("#new_site_templates").slideDown("fast");
    }
  });

  // Select a new site template.
  $(".template-desc-div").click(function(){
    $(".template-desc-div").removeClass("active");
    $(this).addClass("active");

    // Set a form value for this template.
    $("#selected_template").val( $(this).attr("data-id") );
  });

  // Add more page titles.
  $("#add_page_title_btn").click(function(){
    $("#pages_navigation_tbl").append( $("#new_site_pages").html() );
  });

  $(".page-item").blur(function(){
    var idx = $(".page-item").index( $(this) );
    var page_link = $(".page-link").get( idx );
    var link_chk = $(".external-link-chk").get(idx);

    if ( $(link_chk).prop("checked") ) {
      $(page_link).val("");
    } else {
      if ( $(this).val() != "" ) {
        var page = encodeURIComponent( $(this).val().replace(/ /g, "_").toLowerCase() );
        $(page_link).val(page + ".html");
      };
    };
  });

  // Change between link and pages.
  $(".external-link-chk").change(function(){
    var link_chk_idx = $(".external-link-chk").index( $(this) );
    var page_txt = $(".page-link").get( link_chk_idx );
    var link_txt = $(".page-item").get( link_chk_idx );

    if ( $(this).prop("checked") == true ) {
      $(page_txt).val("");
    } else {
      $(page_txt).val( encodeURIComponent( $(link_txt).val().replace(/ /g, "_").toLowerCase() ) + ".html" );
    };
  });

  // Choose the current site.
  $("body").on("click", ".site-list-tr", function(){
    var id = $(this).attr("data-id");

    $("#tab_links a[href='#sites']").tab("show");

    BucketBox.Sites.setCurrent(id, function(){ BucketBox.Sites.setDetails(id); BucketBox.Sites.setDomains(id)  });

    $(".site-list-tr").removeClass("active");
    $(this).addClass("active");

    // Build the "Edit" dropdown.
      var li_arr = [];
      $.each(BucketBox.Sites.current.pages, function(idx, pg){
          li_arr.push('<li><a target="_BLANK" href="/' + BucketBox.Sites.current.directory + '/' + pg.url + '">' + pg.label + '</a></li>');
      });

      $("ul#site_pages_dropdown").html(li_arr);
  });

  // Toggle site name editing.
  $("body").on("click", ".toggle-site-name", function(){
    $("#site_name_edit").toggle();
    $("#site_name_view").toggle();
  });

  // Toggle site Google Analytics editing.
  $("body").on("click", ".toggle-site-ganalytics", function(){
    $("#site_ganalytics_edit").toggle();
    $("#site_ganalytics_view").toggle();
  });

  $('body').on('click', '#add_subdomain', function() {
    var form = BucketBox.Sites.domains.subdomain_form.clone();
    // var form = BucketBox.Sites.domains.subdomain_form2.clone();

    if (form.length == 0) {
        form = BucketBox.Sites.domains.subdomain_form
    }
    form.attr('id', random_number)
    if (!form.hasClass('subdomain_without_id')) {
        form.addClass('subdomain_without_id')
    }
    form.find('#subdomain_name').val('www')
    form.find('#subdomain_name').attr('data-id', '')
    form.find('.main-domain').text('www.' + $('#main_domain_text').val())
    $('#subdomains_list').append(form);
    $('#subdomains_list').children().last().find('.subdomain-editor').show();
  });

  // Update the site name.
  $("body").on("click", "#save_site_name", function(){
    // Update our current model.
    BucketBox.Sites.current.label = $("#site_name").val();

    // Update the current details display.
    $(".pnl-site-name").text( BucketBox.Sites.current.label );

    var payload = {element: "label", new_val: BucketBox.Sites.current.label, site_id: BucketBox.Sites.current.id};
    var style = {
      style: $('button[id^="sites_style_"][disabled]').attr("data-style")
    }
    $.ajax({
      url: "/site/save/meta",
      type: "post",
      data: payload,
      success: function(data){
        if (data["success"] === true) {
          $.smkAlert({text: "Your site has been successfully updated.", type: "success", time: 3});
          reset_site_form(style)
        } else {
          $.smkAlert({text: "Unable to update your site. Please make sure you've entered your changes correctly.", type: "danger", time: 3});
        };

        $("#site_name_edit").toggle();
        $("#site_name_view").toggle();
      },
      error: function(data){
        $.smkAlert({text: "Unable to update your site. Is there a problem with your internet access?", type: "danger", time: 3});

        $("#site_name_edit").toggle();
        $("#site_name_view").toggle();
      }
    });
  });

  // Update the site Google Analytics.
  $("body").on("click", "#save_site_ganalytics", function(){
    // Update our current model.
    BucketBox.Sites.current.ga_code = $("#site_ganalytics").val();

    // Update the current details display.
    $(".pnl-site-ganalytics").text( BucketBox.Sites.current.ga_code );

    var payload = {element: "ga_code", new_val: BucketBox.Sites.current.ga_code, site_id: BucketBox.Sites.current.id};
    $.ajax({
      url: "/site/save/meta",
      type: "post",
      data: payload,
      success: function(data){
        if (data["success"] === true) {
          $.smkAlert({text: "Google Analytics has been successfully updated.", type: "success", time: 3});
        } else {
          $.smkAlert({text: "Unable to update your site. Please make sure you've entered your changes correctly.", type: "danger", time: 3});
        };

        // $("#site_ganalytics_edit").toggle();
        // $("#site_ganalytics_view").toggle();

        $("#analytics_modal").modal("hide");
      },
      error: function(data){
        $.smkAlert({text: "Unable to update your site. Is there a problem with your internet access?", type: "danger", time: 3});

          // $("#site_ganalytics_edit").toggle();
          // $("#site_ganalytics_view").toggle();

          $("#analytics_modal").modal("hide");
      }
    });
  });

    // Clear the gAnalytics box.
    $("#clear_site_ganalytics").click(function(){
        $("#site_ganalytics").val("");
    });

  // Update the site UI when the upload modal is closed.
  $("#upload_site_modal").on("hidden.bs.modal", function (e) {
    update_site_UI();
  });

  // Give the media tab focus.
  $(".media-tab-btn").click(function(){
    $("#tab_links a[href='#media']").tab("show");
  });

  // Add pictures.
  $("body").on("click", ".add-pic-btn", function(){
    $("#tab_links a[href='#media']").tab("show");
    $("#upload_pics_modal").modal("show");
  });

  // Update the UI when the 'Upload Pics' modal is closed.
  $('#upload_pics_modal').on('hidden.bs.modal', function (e) {
    update_image_UI();
  })

  $('#upload_file_modal').on('hidden.bs.modal', function (e) {
    update_file_UI();
  })
  $("body").on("click", ".toggle-image-name", function(){
    $("#image_name").val( BucketBox.Media.Images.current.file_name );
    $("#image_name_edit").toggle();
    $("#image_name_view").toggle();
  });

  $("body").on("click", "#save_image_name", function(){
    BucketBox.Media.Images.current.file_name = $("#image_name").val();
    $(".pnl-site-name").text( BucketBox.Media.Images.current.file_name );
    var payload = {element: "label", new_val: BucketBox.Media.Images.current.file_name, file_id: BucketBox.Media.Images.current.id};
    $.ajax({
      url: "/image/save/meta",
      type: "post",
      data: payload,
      success: function(data){
        if (data["success"] === true) {
          $.smkAlert({text: "Your image has been successfully updated.", type: "success", time: 3});
          BucketBox.Files.updateCollection();
        } else {
          $.smkAlert({text: "Unable to update your image. Please make sure you've entered your changes correctly.", type: "danger", time: 3});
        };

        $("#image_name_edit").toggle();
        $("#image_name_view").toggle();
      },
      error: function(data){
        $.smkAlert({text: "Unable to update your image. Is there a problem with your internet access?", type: "danger", time: 3});

        $("#image_name_edit").toggle();
        $("#image_name_view").toggle();
      }
    });
  });

  $("body").on("click", ".toggle-audio-name", function(){
    $("#audio_name").val( BucketBox.Media.Audios.current.file_name );
    $("#audio_name_edit").toggle();
    $("#audio_name_view").toggle();
  });

  $("body").on("click", "#save_audio_name", function(){
    BucketBox.Media.Audios.current.file_name = $("#audio_name").val();
    $(".pnl-site-name").text( BucketBox.Media.Audios.current.file_name );
    var payload = {element: "label", new_val: BucketBox.Media.Audios.current.file_name, file_id: BucketBox.Media.Audios.current.id};
    $.ajax({
      url: "/audio/save/meta",
      type: "post",
      data: payload,
      success: function(data){
        if (data["success"] === true) {
          $.smkAlert({text: "Your audio has been successfully updated.", type: "success", time: 3});
          BucketBox.Files.updateCollection();
        } else {
          $.smkAlert({text: "Unable to update your audio. Please make sure you've entered your changes correctly.", type: "danger", time: 3});
        };

        $("#audio_name_edit").toggle();
        $("#audio_name_view").toggle();
      },
      error: function(data){
        $.smkAlert({text: "Unable to update your video. Is there a problem with your internet access?", type: "danger", time: 3});

        $("#audio_name_edit").toggle();
        $("#audio_name_view").toggle();
      }
    });
  });

  $("body").on("click", ".toggle-video-name", function(){
    $("#video_name").val( BucketBox.Media.Videos.current.file_name );
    $("#video_name_edit").toggle();
    $("#video_name_view").toggle();
  });

  $("body").on("click", "#save_video_name", function(){
    BucketBox.Media.Videos.current.file_name = $("#video_name").val();
    $(".pnl-site-name").text( BucketBox.Media.Videos.current.file_name );
    var payload = {element: "label", new_val: BucketBox.Media.Videos.current.file_name, file_id: BucketBox.Media.Videos.current.id};
    $.ajax({
      url: "/video/save/meta",
      type: "post",
      data: payload,
      success: function(data){
        if (data["success"] === true) {
          $.smkAlert({text: "Your video has been successfully updated.", type: "success", time: 3});
          BucketBox.Files.updateCollection();
        } else {
          $.smkAlert({text: "Unable to update your video. Please make sure you've entered your changes correctly.", type: "danger", time: 3});
        };

        $("#video_name_edit").toggle();
        $("#video_name_view").toggle();
      },
      error: function(data){
        $.smkAlert({text: "Unable to update your video. Is there a problem with your internet access?", type: "danger", time: 3});

        $("#video_name_edit").toggle();
        $("#video_name_view").toggle();
      }
    });
  });

  $("body").on("click", ".toggle-file-name", function(){
    $("#file_name").val( BucketBox.Files.current.fileName );
    $("#file_name_edit").toggle();
    $("#file_name_view").toggle();
  });

  $("body").on("click", "#save_file_name", function(){
    BucketBox.Files.current.fileName = $("#file_name").val();
    $(".pnl-site-name").text( BucketBox.Files.current.fileName );
    var payload = {element: "label", new_val: BucketBox.Files.current.fileName, file_id: BucketBox.Files.current.id};
    $.ajax({
      url: "/document/save/meta",
      type: "post",
      data: payload,
      success: function(data){
        if (data["success"] === true) {
          $.smkAlert({text: "Your file has been successfully updated.", type: "success", time: 3});
          BucketBox.Files.updateCollection();
        } else {
          $.smkAlert({text: "Unable to update your file. Please make sure you've entered your changes correctly.", type: "danger", time: 3});
        };

        $("#file_name_edit").toggle();
        $("#file_name_view").toggle();
      },
      error: function(data){
        $.smkAlert({text: "Unable to update your file. Is there a problem with your internet access?", type: "danger", time: 3});

        $("#file_name_edit").toggle();
        $("#file_name_view").toggle();
      }
    });
  });

  var img_upload = $("#image_upload");

  img_upload.on("complete", function(file){
    update_image_UI();
  });

  // Update the file details panel when an icon is clicked.
  $("body").on("click", ".document-link", function(){
    var id = $(this).attr("data-id");

    $("#tab_links a[href='#file']").tab("show");
    imageUrl = $(this).find('img').attr('src');
    BucketBox.Files.setCurrent(id, function(){ BucketBox.Files.setDetails(imageUrl) });

      // BucketBox.Files.
  });

  // Update the image details panel when an image is clicked.
  $("body").on("click", ".image-link", function(){
    var id = $(this).attr("data-id");

    // Set focus to the 'media' tab.
    $("#tab_links a[href='#media']").tab("show");

    BucketBox.Media.Images.setCurrent(id, function(){ BucketBox.Media.Images.setDetails(id) });
  });

  // Update the video details panel when an image is clicked.
  $("body").on("click", ".video-link", function(){
    console.log("Clicked on video-link");
    var id = $(this).attr("data-id");

    // Set focus to the 'media' tab.
    $("#tab_links a[href='#media']").tab("show");

    BucketBox.Media.Videos.setCurrent(id, function(){ BucketBox.Media.Videos.setDetails(id) });
  });

  // Update the audio details panel when the file avatar is clicked.
  $("body").on("click", ".audio-link", function(){
    var id = $(this).attr("data-id");

    // Set focus to the 'media' tab.
    $("#tab_links a[href='#media']").tab("show");

    BucketBox.Media.Audios.setCurrent(id, function(){ BucketBox.Media.Audios.setDetails(id) });
  });

  // Add videos.
  $("body").on("click", ".add-vid-btn", function(){
    $("#tab_links a[href='#media']").tab("show");

    $("#upload_vids_modal").modal("show");
  });

  // Update the UI when the 'Upload Pics' modal is closed.
  $('#upload_vids_modal').on('hidden.bs.modal', function (e) {
    update_video_UI();
  })

  // Add audio.
  $("body").on("click", ".add-audio-btn", function(){
    $("#tab_links a[href='#media']").tab("show");

    $("#upload_audio_modal").modal("show");
  });

    // Add file.
    $("body").on("click", ".add-file-btn", function(){
        $("#tab_links a[href='#files']").tab("show");
        $("#upload_file_modal").modal("show");
    });

  $("#load_editable_html").click(function(){
    alert("load");
    // var test = $.load("site/html");
    // $("#edit").load("site/html");
    // console.log(test);
  });

  // Update the UI when the 'Upload Audio' modal is closed.
  $("#upload_audio_modal").on('hidden.bs.modal', function (e) {
    update_audio_UI();
  });

  // Open XML file for data processing.
  $("#wordpress_export_xml").change(function(){
    import_xml();
  });

  // Credentials prompt modal - button click.
  $("#credentials_prompt_btn").click(function(){
    $("#aws_credentials_modal").modal("hide");
  });

  // Open modal when user clicks the clone page icon.
  $("body").on("click", ".clone-page", function(){
    $("#confirm_page_clone_modal").modal("show");

    $("#page_clone_label").val( $(this).attr("data-label") );
    $("#clone_page_btn").attr("data-page-id", $(this).attr("data-page-id"));
  });

  // Clone page when user has confirmed via modal.
  $("body").on("click", "#clone_page_btn", function(){
    var payload = { page_id: $("#clone_page_btn").attr("data-page-id"), new_title: $("#page_clone_label").val() };
    $("#confirm_page_clone_modal").modal("hide");

    $.ajax({
      url: "/page/clone",
      type: "post",
      data: payload,
      success: function(data){
        if (data["success"] === true) {
          $.smkAlert({text: "Page has been cloned.", type: "success", time: 3});

          // Update the "Pages" details section.
          var page_links = "<li><a href='javascript:void(0);' title='Duplicate This Page' data-page-id='" + data["page_id"] + "' data-label='" + data["label"] + "' class='clone-page'><span class='fa fa-files-o'></span></a>&nbsp;&nbsp;<a href='/" + BucketBox.Sites.current.directory + "/" + data["url"] + "' target='_BLANK' title='" + data["label"] + "'>" + data["label"] + "</a>&nbsp;&nbsp;<a href='javascript:void(0);' title='Delete This Page' data-page-id='" +
            data["page_id"] + "' class='delete-page'><span class='fa fa-times'></span></a></li>";

          $(".pnl-site-pages").append( page_links );
        } else {
          $.smkAlert({text: "Unable to clone this page. Either the page ID or new page title is missing.", type: "danger", time: 3});
        };
      },
      error: function(data){
        $.smkAlert({text: "Unable to clone this page. Please try again.", type: "danger", time: 3});
      }
    });
  });

  // Open modal when user clicks the delete page icon.
  $("body").on("click", ".delete-page", function(){
    $("#confirm_page_delete_modal").modal("show");

    $("#delete_page_btn").attr("data-page-id", $(this).attr("data-page-id"));
  });

  // Delete page when user has confirmed via modal.
  $("body").on("click", "#delete_page_btn", function(){
    var that = this;
    var page_id = $(this).attr("data-page-id");

    $("#confirm_page_delete_modal").modal("hide");

    $.ajax({
      url: "/page/delete",
      type: "post",
      data: {page_id: $(that).attr("data-page-id")},
      success: function(data){
        if ( data["success"] === true ) {
          $.smkAlert({text: "Page has been removed.", type: "success", time: 3});

          // Update the "Pages" details section.
          $('a[data-page-id="' + page_id + '"][class="delete-page"]').parent().remove();
        } else {
          $.smkAlert({text: "Unable to remove this page. Page ID is missing or not found in the database.", type: "danger", time: 3});
        };
      },
      error: function(data){
        $.smkAlert({text: "Unable to remove this page. Please try again.", type: "danger", time: 3});
      }
    });
  });

  $('body').on('click', 'a[role="tab"]', function(){
    if ($(this).parent().hasClass('disabled')) {
      return false;
    }
  })

  $('body').on('click', '#create_report_bucket', function(){
    $.ajax({
      url: "/user/set_report_bucket",
      type: "POST",
      success: function(data){
        $('a[href="#step_2"]').parent().removeClass('disabled');
        $('a[href="#step_3"]').parent().removeClass('disabled');
        $('a[href="#step_1"]').parent().addClass('disabled');
        $('a[href="#step_2"]').trigger('click');
        $('#report_bucket_name').text(data.name)
      },
      error: function(data){
        $.smkAlert({text: data.message, type: "danger", time: 3});
      }
    });
  });

  $('body').on('click', '#test_report_setup', function(){
    $.ajax({
      url: "/user/test_report_setup",
      type: "GET",
      success: function(data){
        $.smkAlert({text: 'Test was succesful', type: "success", time: 3});
      },
      error: function(data){
        $.smkAlert({text: data.message, type: "danger", time: 3});
      }
    });
  });

  $('body').on('click', '#cloud_getting_report', function(){
    $("#create_billing_report_modal").modal('show');
  })

  $('body').on('click', '#test_connection', function(){
    $("#test_connection_modal").modal('show');
  })

  $('body').on('click', 'button[id^="files_style_"]', function(){
    $(this).attr('disabled', 'disabled');
    $(this).siblings().removeAttr('disabled');
    $('#search_files').trigger('submit');
  })

  $("body").on("click", "a.choosing-file", function(){
    var id = $(this).attr("data-id");
    $("#tab_links a[href='#file']").tab("show");
    imageUrl = $(this).find('img').attr('src');
    var mediaType = $(this).attr("data-class");
    switch(mediaType) {
      case 'Audio':
        BucketBox.Media.Audios.setCurrent(id, function(){ BucketBox.Media.Audios.setDetails(id) })
        break
      case 'Image':
        BucketBox.Media.Images.setCurrent(id, function(){ BucketBox.Media.Images.setDetails(id) })
        break
      case 'Video':
        BucketBox.Media.Videos.setCurrent(id, function(){ BucketBox.Media.Videos.setDetails(id) })
        break
      case 'Document':
        BucketBox.Files.setCurrent(id, function(){ BucketBox.Files.setDetails(imageUrl) })
        break
    }
    var addBucketButton = $('#add_to_bucket_modal .add-image-to-bucket-btn');
    addBucketButton.attr('data-media-type', mediaType);
    addBucketButton.attr('data-id', id);

  });

  $('body').on('change', '#bucket_filter', function(){
    $('#remove_bucket_filter').remove();
    $('#collection_filters').append('<a href="#" id="remove_bucket_filter" data-bucket-id="' + $(this).val() + '"><div class="panel-elements search_btn"><p class="pull-right">X</p><p>&nbsp;' + $('#bucket_filter option:selected').text() + '</p></div></a>')
    $(this).val('');
    $('#search_files').trigger('submit');
  })

  $('body').on('change', '#media_filter', function(){
    $('#remove_media_filter').remove();
    $('#collection_filters').append('<a href="#" id="remove_media_filter" data-media-type="' + $(this).val() + '"><div class="panel-elements search_btn"><p class="pull-right">X</p><p>&nbsp;' + $('#media_filter option:selected').text() + '</p></div></a>')
    $(this).val('');
    $('#search_files').trigger('submit');
  })

  $('body').on('click', '#remove_bucket_filter, #remove_media_filter', function(){
    $(this).remove();
    $('#search_files').trigger('submit');
  })

  $('body').on('submit', '#search_files', function(e){
    e.preventDefault();
    BucketBox.Files.applySearch();
  });

  $('body').on('click', 'button[id^="sites_style_"]', function(){
    $(this).attr('disabled', 'disabled');
    $(this).siblings().removeAttr('disabled');
  })

  $("body").on("click", 'button[id^="sites_style_"]', function() {
    renderSites();
  });

  function renderSites() {
    var data = {
      style: $('button[id^="sites_style_"][disabled]').attr("data-style")
    }
    $.ajax({
      url: "/sites/list",
      type: "get",
      data: data,
      success: function(data){
        $('.site_render_collection').html(data);
      },
      error: function(data){
        $.smkAlert({text: "Sites wasn't loaded", type: "danger", time: 3});
      }
    })
  }


  tute_videos = $('.tute_videos');
  [].forEach.call(tute_videos, function(item){
    var video = videojs(item.id, { "techOrder": ["vimeo"], "src": item.src, "inactivityTimeout": 0 });
    $("body").on("click", ".close_vid", function(){
      video.pause();
    });
  });

  new Clipboard('a[id*="_copy_size_url_to_clipboard"]');

  $('body').on('click', '#test_connection', function(){
    $.ajax({
      url: 'aws/connect_to_s3',
      type: 'get',
      success: function(data){
        if (data.success) {
          $('#test_credentials_state_1 .second-step').html('<p>Connected.</p><p> Testing Credentials...</p>');
          $.ajax({
            url: 'aws/test_credentials',
            data: {access_key: $('#s3_access_key').val(), secret_key: $('#secret_access_key').val()},
            type: 'post',
            success: function(data){
              if (data.success) {
                $('a[href="#test_credentials_state_2"').tab('show');
                $('#test_credentials_state_2 .third-step').html('<p>Connection is successful.</p>');
                $('#close_test_connection_modal').show();
              } else {
                $('a[href="#test_credentials_state_2"').tab('show');
                $('#test_credentials_state_2 .third-step').html('<p>Connection failed.</p>');
                $('#configure_aws_automatically').show();
              }
            }
          })
        } else {
          $('#test_credentials_state_1 .second-step').html('<p>Disconnected.</p>');
        }
      }
    })
  })

  $('#test_connection_modal').on('hidden.bs.modal', function () {
    $('a[href="#test_credentials_state_1"').tab('show');
    $('#test_credentials_state_1 .second-step').html('');
    $('#test_credentials_state_2 .third-step').html('');
    $('#status_credentials').html('');
    $('#close_test_connection_modal').hide();
    $('#configure_aws_automatically').hide();
  })

  $('body').on('click', '#configure_aws_automatically', function(){
    $.ajax({
      url: 'aws/configure_aws_automatically',
      type: "get",
      success: function(data){
        if (data.success == true) {
          showTestCredentialsState3('<span>Your AWS Account has been successfully updated</span>');
        } else {
          showTestCredentialsState3('<span>We could not verify or update your AWS credentials.</span><span>Please visit your AWS console and create a new API key</span>');
        }

      }
    })
  })

  function showTestCredentialsState3(status){
    $('a[href="#test_credentials_state_3"]').tab('show');
    $('#status_credentials').html(status);
    $('#configure_aws_automatically').hide();
    $('#close_test_connection_modal').show();
  }

  /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
  //setInterval(function(){
  //  var site_id = $(".site-list-tr.active").attr("data-id");
  //
  //  BucketBox.Sites.init();
  //  update_site_publication_stats();
  //  // update_image_UI();
  //  // update_video_UI();
  //}, 2000);
});

  function selectTute(){
    var category_id = $('#category').val()
    $.ajax({
      url: "/",
      type: "get",
      dataType: "script",
      data: {
        category_id: category_id
      }
    });

  }

$(document).on('click', '.go-to-setup-reports-btn', function(){
  $("#create_billing_report_modal").modal('show');
});

function reset_site_form(data){
  $.ajax({
    url: "/sites/list",
    type: "get",
    data: data,
    success: function(data){
      $('.site_render_collection').html(data);
    },
    error: function(data){
      $.smkAlert({text: "Sites wasn't loaded", type: "danger", time: 3});
    }
  })
}

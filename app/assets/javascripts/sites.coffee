$ ->
  $('body').on 'click', '.download-site', ->
    downloadIcon = $(@).find('.fa-download')
    processingIcon = $(@).find('.site-processing')
    toggle = (val)=>
      downloadIcon.toggle(!val)
      processingIcon.toggle(val)
      $(@).prop('disabled', val)
    toggle true
    $.fileDownload("/sites/download?dir=#{BucketBox.Sites.current.directory}")
    .done -> toggle(false)

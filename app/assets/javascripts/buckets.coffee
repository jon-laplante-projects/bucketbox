# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on("keyup change","#bucket_name_txt", ( ->
  txt = $.trim($("input#bucket_name_txt").val())
  if txt != "" && txt != null
    $("button.create-bucket").attr("disabled", false)
    return false
  else
    $("button.create-bucket").attr("disabled", "disabled")
    return false
));

$('#way_of_pay').ready ->
  checkWay =->
    way = $("#way_of_pay option:selected").val()
    if way == '1' then $('.pay-data').show() else $('.pay-data').hide()

  checkWay()
  $('#way_of_pay').on 'change', ->
    checkWay()

$('#new_user').ready ->
  handler = StripeCheckout.configure
    key: if stripePublicKey? then stripePublicKey else null
    token: (token, args) ->
      $("#stripeToken").val(token.id)
      $("#stripeEmail").val(token.email)
      $("#stripeAmount").val(payAmount)
      $("#new_user").submit()

  $('#subscribe_bttn').on 'click', (e) ->
    if $("#way_of_pay option:selected").val() == '3'
      handler.open
        name: $("#user_first_name").val() + $("#user_last_name").val(),
        description: 'pay for using our site',
        amount: payAmount * 100
      e.preventDefault()

  $(window).on 'popstate', ->
    handler.close()






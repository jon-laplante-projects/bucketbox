class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  
  # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # ~~ Disabling CSRF to make APIs work.
  # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  #protect_from_forgery with: :exception

  	def aws_conn(bucket_name, action)
		# Check if the bucketbox-image bucket exists and if not, create it.
		amazon_key = AmazonKey.find_by(user_id: current_user.id)

		Aws.config.update({
			  region: 'us-east-1',
			  credentials: Aws::Credentials.new(amazon_key.access_key, amazon_key.secret_access_key)
			})

		s3 = Aws::S3::Resource.new

		if action === "init"
			init_buckets(s3)
		else
			# Check for the buckets flags and if they aren't there, make the buckets.
			unless current_user.has_images_bucket? && current_user.has_videos_bucket? && current_user.has_audio_bucket? && current_user.has_documents_bucket?
				init_buckets(s3)
			end

			begin
				bucket = s3.create_bucket(bucket: "#{bucket_name}-#{current_user.token}", acl: 'public-read')
			rescue # Will need to send a message back to the client

			end
		end

		# # Make an array of asset bucket names, and check if bucket_name is in it.
		# bucket_arr = ["bucketbox-images", "bucketbox-videos", "bucketbox-audio"]

		# if bucket_arr.include?("#{bucket_name}")
		# 	# ??? name = "#{bucket_name}-#{current_user.token}"

		# 	# Set the record type.
		# 	case "#{bucket_name}"
		# 		when "bucketbox-images"
		# 			rec_type = "image"
		# 		when "bucketbox-videos"
		# 			rec_type = "video"
		# 		when "bucketbox-audio"
		# 			rec_type = "audio"
		# 	end

		# 	# Check if bucket name is available, and if not, find one that is.
		# 	token = unique_bucket("#{bucket_name}", "#{current_user.token}", rec_type, s3)
		# 	if "#{token}" != "#{current_user.token}"
		# 		current_user.token = token
		# 		current_user.save
		# 	end

		# 	# Make the bucket on s3 with our unique name.
		# 	bucket = s3.buckets.create("#{bucket_name}-#{current_user.token}", :acl => :public-read)
		# else
		# 	# We're creating a website bucket, so handle naming uniqueness, etc.
		# 	name = "#{bucket_name}"

		# 	# Check if bucket name is available, and if not, find one that is.
		# 	token = unique_bucket("#{bucket_name}", "#{bucket_name}", "website", s3)
		# 	if "#{token}" != "#{bucket_name}"
		# 		#########################################################
		# 		#########################################################
		# 		## Need to handle this!!!
		# 		#########################################################
		# 		#########################################################
		# 	end

		# 	# Make the bucket on s3 with our unique name.
		# 	bucket = s3.buckets.create("#{bucket_name}", :acl => :public-read)
		# end

		# Configure our paperclip attachment.
		# config.paperclip_defaults = {
		#   :storage => :s3,
		#   :s3_credentials => {
		#     :bucket => "#{name}",
		#     :access_key_id => "#{amazon_key.access_key}",
		#     :secret_access_key => "#{amazon_key.secret_access_key}"
		#   }
		# }
	end

	def unique_bucket(bucket_name, token, rec_type, s3_obj)
		# Create the bucket object.
		bucket_arr = ["bucketbox-images", "bucketbox-videos", "bucketbox-audio", "bucketbox-files"]
		
		if bucket_arr.include?("#{bucket_name}")
			# If this is an asset bucket, set the name prefix (i.e. bucketbox-images)
			name_prefix = "#{bucket_name}"
			bucket = s3_obj.create_bucket(bucket: "#{name_prefix}-#{token}")
		else
			name_prefix = ""
			bucket = s3_obj.create_bucket(bucket: "#{token}")
		end

		# Is this bucket already in the global namespace?
		unless bucket.exists?
			begin
				token = SecureRandom.urlsafe_base64 12
				token = token.gsub(/[^0-9a-z]/i, '').gsub(" ", "_").downcase

				bucket = s3_obj.create_bucket(bucket: "#{name_prefix}-#{token}")
			end while bucket.exists?
		end
		return token
	end

	def init_buckets(s3)
		valid_token = false

		# Make a hash of asset bucket names, and check if bucket_name is in it.
		buckets_hash = { has_images_bucket: false, has_videos_bucket: false, has_audio_bucket: false, has_documents_bucket: false }

		until valid_token === true
			buckets_hash.each do |bucket|
				# Transform the buckets_hash key into the bucket_name string.
				case "#{bucket[0]}"
					when "has_images_bucket"
						bucket_name = "bucketbox-images"
					when "has_videos_bucket"
						bucket_name = "bucketbox-videos"
					when "has_audio_bucket"
						bucket_name = "bucketbox-audio"
					when "has_document_bucket"
						bucket_name = "bucketbox-files"
				end

				# Check if bucket name is available, and if not, find one that is.
				token = unique_bucket("#{bucket_name}", "#{current_user.token}", nil, s3)
				if "#{token}" == "#{current_user.token}"
					buckets_hash[bucket[0]] = true
				end
			end

			# If all the buckets are available, set valid_token to true...
			if buckets_hash.all? == true
				valid_token = true
			
			# ...otherwise, change the current_user.token to the new one.
			else
				current_user.token = token
				current_user.save
			end
		end

		# Make the buckets on s3 with our unique name.
		buckets_hash.each do |b|
			# Record the success to the DB.
			case "#{b[0]}"
				when "has_images_bucket"
					bucket_name = "bucketbox-images-#{current_user.token}"
				when "has_videos_bucket"
					bucket_name = "bucketbox-videos-#{current_user.token}"
				when "has_audio_bucket"
					bucket_name = "bucketbox-audio-#{current_user.token}"
				when "has_documents_bucket"
					bucket_name = "bucketbox-files-#{current_user.token}"
			end

			bucket = s3.create_bucket(bucket: bucket_name, acl: 'public-read')
			# Record the success to the DB.
			case "#{b[0]}"
				when "has_images_bucket"
					current_user.has_images_bucket = true
				when "has_videos_bucket"
					current_user.has_videos_bucket = true
				when "has_audio_bucket"
					current_user.has_audio_bucket = true
				when "has_documents_bucket"
					current_user.has_documents_bucket = true
			end

			current_user.save
		end
	end
end
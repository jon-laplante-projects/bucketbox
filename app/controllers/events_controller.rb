class EventsController < ApplicationController
  after_filter :track_event

  def index
    @events = EventDecorator.decorate_collection Ahoy::Event.with_domains.by_time
  end

  protected

  def track_event
    ahoy.track "Processed #{controller_name}##{action_name}", request.params[:_json][0][:properties] if request.params[:_json].present?
  end

end

class BucketsController < ApplicationController
  def buckets_collection
    @buckets = current_user.buckets.all
  end

  def buckets_pagination
    buckets_count = Bucket.where(user_id: current_user.id).count

    render partial: "partials/buckets_pagination", locals: {buckets_count: buckets_count}
  end

  def buckets_page
    buckets = Bucket.where(user_id: current_user.id).paginate(page: params[:bucket_page], per_page: 6).order(id: :desc)

    render partial: "partials/buckets_page", locals: {buckets: buckets}
  end

  def bucket_contents
    #contents =
  end

  def create
    unless params[:label].blank?
      bucket = Bucket.new
      bucket.label = "#{params[:label]}"
      bucket.user_id = current_user.id
      bucket.save

      render json: { success: true, label: "#{bucket.label}", id: bucket.id, message: "Bucket successfully added." }, status: 200
    else
      render json: { success: false, message: "No label provided." }, status: 200
    end
  end

  # def update
  #
  # end

  def destroy
    unless params[:id].blank?
      # Remove the bucket_id from media.
      images = Image.where(bucket_id: params[:id])
      audios = Audio.where(bucket_id: params[:id])
      videos = Video.where(bucket_id: params[:id])
      docs = Document.where(bucket_id: params[:id])

      if images.count > 0
        images.update_all(bucket_id: nil)
        # images.save
      end

      if audios.count > 0
        audios.update_all(bucket_id: nil)
        # images.save
      end

      if videos.count > 0
        videos.update_all(bucket_id: nil)
        # videos.save
      end

      if docs.count > 0
        docs.update_all(bucket_id: nil)
      end

      bucket = Bucket.find(params[:id])

      bucket.destroy

      render json: { success: true }, status: 200
    else
      render json: { success: false }, status: 200
    end
  end

  def add_to_bucket
    unless params[:id].blank? || params[:media_type].blank? || params[:bucket_id].blank?
      case params[:media_type].downcase
        when "image"
          media = Image.find(params[:id])
        when "video"
          media = Video.find(params[:id])
        when "audio"
          media = Audio.find(params[:id])
        else
          media = Document.find(params[:id]) 
      end

      media.bucket_id = params[:bucket_id]
      media.save

      render json: { success: true }, status: 200
    else
      render json: { success: false }, status: 200
    end
  end

  def remove_from_bucket
    unless params[:id].blank? || params[:media_type].blank?
      case params[:media_type]
        when "image"
          media = Image.find(params[:id])
        when "video"
          media = Video.find(params[:id])
        when "audio"
          media = Audio.find(params[:id])
      end

      media.bucket_id = nil
      media.save

      render json: { success: true }, status: 200
    else
      render json: { success: false }, status: 200
    end
  end

  def buckets_select_list
    buckets = current_user.buckets.all

    render partial: "partials/buckets_select_list", locals: {buckets: buckets}
  end
end
class TrackingController < ApplicationController
  skip_before_action :verify_authenticity_token, only: [:tracking]

  def tracking
    respond_to do |format|
      format.js
    end
  end
end

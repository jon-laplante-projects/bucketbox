class RegistrationsController < Devise::RegistrationsController
  before_filter :ways_of_pay, only: [:new, :create]

private

  def after_sign_up_path_for(resource)
    way = params[:way_of_pay].to_i
    if way == 2
      paypal_process
    elsif way == 3
      stripe_process
    end
  end

  def paypal_process
    values = {
        cmd: '_xclick',
        charset: 'utf-8',
        business: App.paypal_merchant_email, #This is email of test merchant PayPal account
        item_name: App.paypal_payment_name, #name of product
        amount: App.pay_amount, #price of product
        return: root_url,
        cancel_return: new_user_registration_url
    }
    "#{App.paypal_url}cgi-bin/webscr?#{values.to_query}" #sandbox of paypal
  end

  def stripe_process
    customer = Stripe::Customer.create(
        email: params[:stripeEmail],
        card:  params[:stripeToken]
    )
    charge = Stripe::Charge.create(
        customer:    customer.id,
        amount:      App.pay_amount * 100,
        description: App.stripe_payment_description,
        currency:    'usd'
    )

    root_path
  rescue Stripe::CardError => e
    flash[:error] = e.message
    return new_user_registration_path
  end

  def ways_of_pay
    @ways ||= App.way_pay
  end

  def sign_up_params
    params.require(:user).permit(:first_name, :last_name, :email, :password, :password_confirmation)
  end

  def account_update_params
    params.require(:user).permit(:first_name, :last_name, :email, :password, :password_confirmation, :current_password)
  end
end

class DomainsController < ApplicationController
  before_action :find_domain, except: [:index, :new, :create]

  def index
    @domains = Domain.all
  end

  def new
    @domain = Domain.new
  end

  def create
    @domain = Domain.new domain_params
    if @domain.save
      redirect_to domains_path
    else
      render :new
    end
  end

  def show

  end

  def edit

  end

  def update
    if @domain.update domain_params
      redirect_to domains_path
    else
      render :edit
    end
  end

  def destroy
    @domain.destroy
    redirect_to domains_path
  end

  private

  def find_domain
    @domain = Domain.find params[:id]
  end

  def domain_params
    params.require(:domain).permit(:name, :description)
  end
end

class MediaController < ApplicationController
	def image_upload
		aws_conn("bucketbox-images", nil)

		@images = Image.new
		@images.user_id = current_user.id
		@images.name = params[:filename]
		@images.label = params[:filename]
		@images.file = params[:file]
	  	if @images.save
	  	  render json: { message: "success", link: @images.file.url(:original) }, :status => 200
	  	else
	  	  #  you need to send an error header, otherwise Dropzone
	          #  will not interpret the response as an error:
	  	  render json: { error: @images.errors.full_messages.join(',')}, :status => 400
	  	end  
	end

	def delete_image
		unless params[:image_id].blank?
			image = current_user.images.find(params[:image_id])

			image.destroy

			render json: { success: true }
		else
			render json: { success: false }
		end
	end

	def image_meta_update
		unless params[:new_val].blank? || params[:file_id].blank?
			Image.find(params[:file_id]).update_attributes(file_file_name: "#{params[:new_val]}")
			render json: { success: true }
		else
			render json: { success: false }
		end
	end

	def video_upload
		aws_conn("bucketbox-videos", nil)

		@videos = Video.new
		@videos.user_id = current_user.id
		@videos.name = params[:filename]
		@videos.label = params[:filename]
		@videos.file = params[:file]
	  	if @videos.save
	  	  render json: { message: "success" }, :status => 200
	  	else
	  	  #  you need to send an error header, otherwise Dropzone
	          #  will not interpret the response as an error:
	  	  render json: { error: @videos.errors.full_messages.join(',')}, :status => 400
	  	end  
	end

	def delete_video
		unless params[:video_id].blank?
			aws_conn("bucketbox-videos", nil)

			video = current_user.videos.find(params[:video_id])

			video.destroy

			render json: { success: true }
		else
			render json: { success: false }
		end
	end

	def video_meta_update
		unless params[:new_val].blank? || params[:file_id].blank?
			Video.find(params[:file_id]).update_attributes(file_file_name: "#{params[:new_val]}")
			render json: { success: true }
		else
			render json: { success: false }
		end
	end

	def rotate_video_thumbnail
		unless params[:direction].blank?
			video = Video.find(params[:video_id])

			img = MiniMagick::Image.new("#{video.file.url(:thumb)}")

			if params[:direction] === "clockwise"
				img.rotate "90"
			else
				img.rotate "-90"
			end

			aws_conn("bucketbox-videos", nil)

			s3 = AWS::S3.new

			##############################################
			## Set up the save to S3.
			##############################################
			# Get the object bucket and path.
			url_arr = video.file.url(:thumb).split("/")
			4.times { url_arr.shift }

			# Get rid of the querystring
			filename_with_param = url_arr.last
			url_filename = url_arr.last.split("?").first
			url_arr.pop
			url_arr.push(url_filename)

			# Set an AWS::S3 object.
			obj = s3.buckets["bucketbox-videos-#{current_user.token}"].objects["#{url_arr.join("/")}"]

			# Rename the file to get rid of the spoof protection querystring.
			File.rename("#{filename_with_param}", "#{url_arr.last}")

			# Get the local file created by mini_magick.
			file = File.open("#{url_arr.last}", 'rb')

			# Write the locally edited image back to S3.
			obj.write(file)

			# Set object access control to public readable.
			obj.acl = :public-read

			# Delete the local file object.
			file.close
			File.delete("#{url_arr.last}")

			##############################################
			## End save to S3.
			##############################################

			render json: { success: true }
		else
			render json: { success: false }
		end
	end

	def audio_upload
		aws_conn("bucketbox-audio", nil)

		@Audios = Audio.new
		@Audios.user_id = current_user.id
		@Audios.name = params[:filename]
		@Audios.label = params[:filename]
		@Audios.file = params[:file]
	  	if @Audios.save
	  	  render json: { message: "success" }, :status => 200
	  	else
	  	  #  you need to send an error header, otherwise Dropzone
	          #  will not interpret the response as an error:
	  	  render json: { error: @Audios.errors.full_messages.join(',')}, :status => 400
	  	end  
	end

	def delete_audio
		unless params[:audio_id].blank?
			audio = current_user.audios.find(params[:audio_id])

			audio.destroy

			render json: { success: true }
		else
			render json: { success: false }
		end
	end

	def audio_meta_update
		unless params[:new_val].blank? || params[:file_id].blank?
			Audio.find(params[:file_id]).update_attributes(file_file_name: "#{params[:new_val]}")
			render json: { success: true }
		else
			render json: { success: false }
		end
	end

	def image_url_array
		images = current_user.images.all

		img_arr = Array.new

		images.each do |img|
			img_arr << img.file.url(:medium)
		end

		render json: img_arr.to_json
	end

	def video_url_array
		@videos = current_user.videos.all

		# vid_arr = Array.new

		# videos.each do |vid|
		# 	vid_arr << vid.file.url(:thumb)
		# end

		# render json: vid_arr.to_json
	end

	def audio_url_array
		@audios = current_user.audios.all
	end

	def delete_page_image
		images = current_user.images.all

		img_arr = Array.new

		images.each do |img|
			img_hash = Hash.new

			img_hash["id"] = img.id
			img_hash["url"] = img.file.url(:medium)

			img_arr << img_hash
		end

		selected_img = img_arr.select {|img| img["url"] == "#{params[:src]}" }
		puts "ID: #{selected_img}"
		image = current_user.images.find(selected_img[0]["id"])

		image.destroy

		render json: { success: true }
	end

	def images_collection
		@images = current_user.images.all
	end

	def images_pagination
		site_image_count = Image.where(user_id: current_user.id).count

		render partial: "partials/media_image_pagination", locals: {site_image_count: site_image_count}
	end

	def images_page
		site_images = Image.where(user_id: current_user.id).paginate(page: params[:media_image_page], per_page: 8).order(id: :desc)

		render partial: "partials/media_image_page", locals: {site_images: site_images}
	end

	def videos_collection
		@videos = current_user.videos.all
	end

	def videos_pagination
		site_video_count = Video.where(user_id: current_user.id).count

		render partial: "partials/media_video_pagination", locals: {site_video_count: site_video_count}
	end

	def videos_page
		site_videos = Video.where(user_id: current_user.id).paginate(page: params[:media_video_page], per_page: 4).order(id: :desc)

		render partial: "partials/media_video_page", locals: {site_videos: site_videos}
	end

	def audios_collection
		@audios = current_user.audios.all
	end

	def audios_pagination
		site_audio_count = Audio.where(user_id: current_user.id).count

		render partial: "partials/media_audio_pagination", locals: {site_audio_count: site_audio_count}
	end

	def audios_page
		site_audios = Audio.where(user_id: current_user.id).paginate(page: params[:media_audio_page], per_page: 6).order(id: :desc)

		render partial: "partials/media_audio_page", locals: {site_audios: site_audios}
	end
end

class VisitsController < ApplicationController
  after_filter :track_visit

  def index
    @visits = VisitDecorator.decorate_collection Visit.with_domains.by_started_at
  end

  def track_visit
    ahoy.track_visit request.params
  end
end

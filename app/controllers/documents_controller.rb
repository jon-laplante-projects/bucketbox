class DocumentsController < ApplicationController
  MEDIA_TYPES = ['Audio', 'Video', 'Image', 'Document']
  MEDIA_MODELS = [Audio, Video, Image, Document]

  def file_upload
    aws_conn("bucketbox-files", nil)
    p params[:file]
    logger.debug params[:file].content_type
    logger.debug Audio::TYPES.include? params[:file].content_type
    logger.debug Video::TYPES.include? params[:file].content_type
    logger.debug Image::TYPES.include? params[:file].content_type
    logger.debug Document::TYPES.include? params[:file].content_type
    @files = MEDIA_MODELS.find { |model| model::TYPES.include? params[:file].content_type}.new

    if @files 
      @files.user_id = current_user.id
      @files.name = params[:filename]
      @files.label = params[:filename]
      @files.file = params[:file]
      if @files.save
        render json: { message: "success" }, :status => 200
      else
        #  you need to send an error header, otherwise Dropzone
        #  will not interpret the response as an error:
        render json: { error: @files.errors.full_messages.join(',')}, :status => 400
      end
    else
       render json: { error: 'undefined content type'}, :status => 400
    end  
  end

  def files_render
    if MEDIA_TYPES.include? params[:media_tag] 
      models = [eval(params[:media_tag])]
    else
      models = MEDIA_MODELS
    end
    @user_files = Document.none  
    models.each { |model| @user_files += model.ransack(params[:q].merge(user_id_eq: current_user.id)).result }
    render partial: "partials/files_#{params[:style]}"
  end  

  def files_collection
    @documents = current_user.documents.all
  end

  def file_url_array

  end

  def files_pagination
    files_count = Document.where(user_id: current_user.id).count

    render partial: "partials/files_pagination", locals: {files_count: files_count}
  end

  def files_page
    files = Document.where(user_id: current_user.id).paginate(page: params[:media_image_page], per_page: 8).order(id: :desc)

    render partial: "partials/files_page", locals: {files: files}
  end

  def delete_file
    file = current_user.documents.find(params[:document_id])
    if file
      file.destroy
      render json: { success: true }
    else
      render json: { error: true }
    end  
  end

  def document_meta_update
    unless params[:new_val].blank? || params[:file_id].blank?
      Document.find(params[:file_id]).update_attributes(file_file_name: "#{params[:new_val]}")
      render json: { success: true }
    else
      render json: { success: false }
    end
  end
end

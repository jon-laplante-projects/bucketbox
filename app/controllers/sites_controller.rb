class SitesController < ApplicationController
  before_action :authenticate_user!
  def index
    unless current_user.sites.find_by(directory: "#{params[:dir]}") == nil
      @directory = params[:dir]

      render "#{params[:dir]}/index", layout: false
    end
  end

  def download
    site = Site[params[:dir]]
    cookies[:fileDownload] = 'true'
    send_file site.zip, type: 'application/zip', disposition: 'attachment', filename: "#{site.name}_#{site.directory}.zip"
  end

  def processing_status
    site = Site.find(params[:id])
    render json: { processing_event: site.processing_event, processing_msg: site.processing_msg }, status: 200
  rescue ActiveRecord::RecordNotFound
    head :not_found
  end
end

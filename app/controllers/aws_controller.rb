class AwsController < ApplicationController
  require 'rss'

  def create_credentials
    json_response = {message: "One or both of the API keys is missing.", success: false}

    unless params[:access_key].blank? || params[:secret_key].blank?
      Aws.config.update({
        region: 'us-east-1',
        credentials: Aws::Credentials.new(params[:access_key], params[:secret_key]),
      })

      # Test to see if these credentials are in fact valid.
      begin
        s3 = Aws::S3::Client.new

        resp = s3.list_buckets.buckets.first


        # If this works, write the credentials to the database.
        # Make sure that this is the users only account.
        ##### If they already have an account in the database, require
        ##### verification for overwrite of credentials.

        # Do they have any credentials?
        existing_creds = AmazonKey.where(user_id: current_user.id).count

        if existing_creds > 0
          # Did the user pass the same credentials from an existing record (i.e. a duplicate)?
          unless AmazonKey.where(access_key: params[:access_key], secret_access_key: params[:secret_key]).count > 0
            credential = AmazonKey.select(:id).find_by(user_id: current_user.id)
            save_credentials(credential.id)
          end
        else
          save_credentials(nil)
        end

        # Run the buckets init regardless, as if the buckets exist the script should do nothing.
        unless current_user.has_images_bucket? && current_user.has_videos_bucket? && current_user.has_audio_bucket?
          aws_conn(nil, "init")
        end

        json_response = {message: "Success!", success: true}
      rescue Exception => err
        puts "Failure: #{err}"
        json_response = {message: "Failure. #{err}", success: false}
      end
    end

    render json: { result: json_response }, status: 200
  end

  def bucket_status
    buckets_hash = { has_images_bucket: current_user.has_images_bucket, has_videos_bucket: current_user.has_videos_bucket, has_audio_bucket: current_user.has_audio_bucket }

    # Convert nil values to false.
    buckets_hash.each do |bucket|
      if bucket[1].blank?
        buckets_hash[bucket[0]] = false       
      end
    end

    render json: buckets_hash
  end

  def connect_to_s3
    rss = RSS::Parser.parse('http://status.aws.amazon.com/rss/s3-us-standard.rss', false)

    if rss.items.first.title[0..28] === "Service is operating normally"
      render json: { success: true }
    else
      render json: { success: false }
    end
    # result = `ping -q -c 5 s3.amazonaws.com`
    # result = `ping -q -c 5 console.aws.amazon.com`

    # if $?.exitstatus == 0
    #   render json: { success: true }
    # else
    #   render json: { success: false }
    # end
  end  

  def test_credentials
    Aws.config.update({
        region: 'us-east-1',
        credentials: Aws::Credentials.new(params[:access_key], params[:secret_key])
      })
    iam = Aws::IAM::Client.new
    iam.list_users

    create_test_bucket

    render json: { success: true }
  rescue 
    render json: { success: false }
  end

  def configure_aws_automatically
    policy = Aws::IAM::Policy.new({
      version: "2012-10-17",
      statements: [
        {
          effect: "Allow",
          actions: "*",
          resources: "*"
        }      
      ]
    })
 
    policy_set = false  
    iam.users.each do |user| 
      if user.name == 'admin'
        user.policies['bucket_box_policy'] = policy
        policy_set = true
      end  
    end

    unless policy_set
      user = iam.users.create('bucket_box_user_#{current_user.token}')
      user.policies['bucket_box_policy'] = policy
    end  

    create_test_bucket

    render json: { success: true }
  rescue 
    render json: { success: false }
  end  

  private

  def create_test_bucket
    s3 = Aws::S3::Resource.new
    test_bucket_name = "test_credentials_bucket_#{current_user.token}"
    test_bucket = s3.create_bucket({ bucket: test_bucket_name })
    test_bucket.delete
  end
  # def spawn_buckets
  #   s3 = AWS::S3.new

  #   ["bucketbox-images", "bucketbox-videos", "bucketbox-audio", "bucketbox-sites"].each do |bucket_name|
  #     bucket = s3.buckets["#{bucket_name}"]

  #     unless bucket.exists?
  #       #s3 = AWS::S3.client
  #       bucket = s3.buckets.create("#{bucket_name}", :acl => :public-read)
  #     end
  #   end
  # end

  def save_credentials(cred_id)
    if cred_id.nil?
      creds = AmazonKey.new
    else
      creds = AmazonKey.find_by(user_id: current_user.id)
    end

    creds.user_id = current_user.id
    creds.access_key = params[:access_key]
    creds.secret_access_key = params[:secret_key]
    creds.save
  end

end
require 'zip'

class SpaController < ApplicationController
  #before_action :set_spa, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  include SpasHelper
  # GET /spas
  # GET /spas.json
  def index
    @template_designs = TemplateDesign.all
    @templates = Template.all
    @sites = current_user.sites.paginate(page: params[:site_page], per_page: 10)
    if AmazonKey.where(user_id: current_user.id).count > 0
      @amazon_key = AmazonKey.find_by(user_id: current_user.id)
    else
      @amazon_key = nil
    end

    @sites_count = current_user.sites.count
    @sites_deployed = current_user.sites.where(mode: "published").count

    # New objects created for file uploads.
    @images = Image.new
    @videos = Video.new
    @audios = Audio.new
    @files = Document.new
    @site_upload = Site.new
    #@imports = Import.new

    # Retrieve the images, videos, and sound files for the media tab and dashboard.
    @site_images = Image.where(user_id: current_user.id)
    @site_videos = Video.where(user_id: current_user.id)
    @site_audios = Audio.where(user_id: current_user.id)
    @site_documents = Document.where(user_id: current_user.id)
    @site_buckets = Bucket.where(user_id: current_user.id).paginate(page: params[:bucket_page], per_page: 6).order(id: :desc)
    @user_files =  @site_images + @site_videos + @site_audios + @site_documents

    # Get the user-created buckets.
    @buckets = current_user.buckets.all

    @tutes = params[:category_id] ? Tute.where(category_id: params[:category_id]) : Tute.all
    @categories = Category.all

    @media_types = [
      ['audio', 'Audio'],
      ['video', 'Video'],
      ['image', 'Image'],
      ['other', 'Document']
    ]
  end

  def publication_percentage_stats
    @sites_count = current_user.sites.count
    @sites_deployed = current_user.sites.where(mode: "published").count

    unpub = @site_count.to_i - @site_deployed.to_i

    @chart_data = {
        value: unpub,
        color: "#46BFBD",
        highlight: "#5AD3D1",
        label: "Unpublished"
    },
    {
        value: @site_deployed,
        color: "#FDB45C",
        highlight: "#FFC870",
        label: "Published"
    }

    @chart_options = { width: 225 }

    render layout: false
  end

  def site_from_template
    site_params = JSON.parse(params["_json"])

    site = Site.new

    site.user_id = current_user.id
    site.template_id = site_params["template"]
    site.name = site_params["title"].gsub(/[^0-9a-z]/i, '').gsub(" ", "_").downcase
    site.label = site_params["title"]
    site.mode = "edit"
    site.generate_token("directory", 14)
    # Persist state to DB to replace websockets since they can't be used in Nginx/Unicorn.
    site.processing_event = "record"
    site.processing_msg = "<span class='fa fa-database'></span>&nbsp;&nbsp;Site recorded in database."
    site.save

    tmpl = Template.find(site.template_id)

    # render json: { message: "Site recorded in database.", status: "record", site_id: "#{site.id}", template_id: "#{site.template_id}", site_name: "#{site.name}", site_label: "#{site.label}", template_label: "#{tmpl.label}", template_thumbnail: "#{tmpl.thumbnail}", directory: "#{site.directory}", guid: "#{params[:guid]}" }, :status => 200
    #WebsocketRails[:site_events].trigger 'new_event', { message: "Site recorded in database.", status: "record", site_id: "#{site.id}", template_id: "#{site.template_id}", site_name: "#{site.name}", site_label: "#{site.label}", template_label: "#{tmpl.label}", template_thumbnail: "#{tmpl.thumbnail}", directory: "#{site.directory}", guid: "#{params[:guid]}" }

    Thread.new do
      #system "mkdir #{Rails.root}/public/sites/#{site.directory}"
      system "cp -R #{Rails.root}/lib/templates/files/#{tmpl.directory}  #{site.local_path}"

      # Build a list of pages for this site.
      find_html_files(site.id, site.local_path)

      # Persist state to DB to replace websockets since they can't be used in Nginx/Unicorn.
      site.processing_event = "editor"
      site.processing_msg = "<span class='fa fa-pencil-square-o'></span>&nbsp;&nbsp;Template-based site processed. Adding editing capability."
      site.save
      #WebsocketRails[:site_events].trigger 'new_event', { message: "Template-based site processed. Adding editing capability.", status: "editor", site_id: "#{site.id}", guid: "#{params[:guid]}" }

      # Added editor data to every page.
      pages = site.pages

      pages.each do |page|
        # Inject the editor into pages.
        EditabilityInjector.inject_editability(site.local_path, page.url, site.local_path, site.id, page.id)
      end

      # Persist state to DB to replace websockets since they can't be used in Nginx/Unicorn.
      site.processing_event = "complete"
      site.processing_msg = nil
      site.save
      #WebsocketRails[:site_events].trigger 'new_event', { message: "Site is complete.", status: "complete", site_id: "#{site.id}", guid: "#{params[:guid]}" }

      # Ensure DB connections are closed, as threads open their own.
      ActiveRecord::Base.connection.close
    end

    # Build our response
    resp_hash = { success: true, id: site.id, template_id: tmpl.id, template_label: tmpl.label, template_thumbnail: tmpl.thumbnail, created_at: site.created_at, directory: site.directory }
    render json: resp_hash, :status => 200
  end

  def upload_site
    aws_conn("bucketbox-sites-#{current_user.token}", nil)

    site = Site.new

    site.user_id = current_user.id
    site.template_id = 6
    site.name = "untitled"
    site.label = "Untitled"
    site.mode = "published"
    site.generate_token("directory", 14)
    site.upload = params[:file]
    # Write to DB since websockets don't work in Unicorn/Nginx
    site.processing_event = "record"
    site.processing_msg = "<span class='fa fa-database'></span>&nbsp;&nbsp;Site recorded in database."

    tmpl = Template.find(site.template_id)

    if site.save
      #WebsocketRails[:site_events].trigger 'new_event', { message: "Site recorded in database.", status: "record", site_id: "#{site.id}", template_id: "#{site.template_id}", site_name: "#{site.name}", site_label: "#{site.label}", template_label: "#{tmpl.label}", template_thumbnail: "#{tmpl.thumbnail}", directory: "#{site.directory}", guid: "#{params[:guid]}" }

      # render json: { message: "success" }, :status => 200

      Thread.new do
        # If the path 'tmp/zips/unique_directory' doesn't exist, create it.
        dirname = File.dirname("tmp/zips/#{site.directory}/#{site.upload_file_name}")
        unless File.directory?(dirname)
          FileUtils.mkdir_p(dirname)
        end

        # Download the .zip file.
        File.open("tmp/zips/#{site.directory}/#{site.upload_file_name}", 'wb') do |file|
          file << open("#{site.upload.url}").read

          # Write to DB since websockets don't work in Unicorn/Nginx
          site.processing_event = "processing"
          site.processing_msg = "<span class='fa fa-archive'></span>&nbsp;&nbsp;Processing uploaded .zip file."
          site.save
          #WebsocketRails[:site_events].trigger 'new_event', { message: "Processing uploaded .zip file.", status: "processing", site_id: "#{site.id}", guid: "#{params[:guid]}" }
        end

        # Unzip the attached .zip file.
        Zip::File.open("tmp/zips/#{site.directory}/#{site.upload_file_name}") { |zip_file|
             zip_file.each { |f|
                 f_path=File.join(site.local_path, f.name)
                 #f_path=File.join("tmp/sites/#{site.directory}", f.name)
             FileUtils.mkdir_p(File.dirname(f_path))
             zip_file.extract(f, f_path) unless File.exist?(f_path)
            }
        }

        # Write to DB since websockets don't work in Unicorn/Nginx
        site.processing_event = "editor"
        site.processing_msg = "<span class='fa fa-pencil-square-o'></span>&nbsp;&nbsp;Uploaded site processed. Adding editing capability."
        site.save
        # WebsocketRails[:site_events].trigger 'new_event', { message: "Uploaded site processed. Adding editing capability.", status: "editor", site_id: "#{site.id}", guid: "#{params[:guid]}" }

        # Check if the unzipped directory contains index.html or index.htm in the root.
        upload_file_name = site.upload_file_name.split(".")[0]

        unless site_index(site)
        #unless File.exists?("tmp/sites/#{site.directory}/#{upload_file_name}/index.html") || File.exists?("tmp/sites/#{site.directory}/#{upload_file_name}/index.htm") || File.exists?("tmp/sites/#{site.directory}/index.html") || File.exists?("tmp/sites/#{site.directory}/index.htm")
          puts "Deleting site - no index file in zip..."
          puts "#{site.local_path}/#{upload_file_name}"
          # Delete the .zip file.
          Dir.chdir("#{Rails.root}/tmp/zips") do
            system "rm -r #{site.directory}"
          end

          # Delete the site directory
          system "rm -r #{site.local_path}"

          # Delete the database record.sit
          site.destroy

          render :nothing => true, :status => 200, :content_type => 'text/html'
          # render json: { status: "error", message: "Zip file does not contain index.html or index.htm in root directory." }, :status => 200
        else
          puts "Index file exists, continuing on..."
          # Get the index file name/extension.
          if File.exists?("#{site.local_path}/#{upload_file_name}/index.html")
            puts "First: #{upload_file_name}"
            index_dir = site.local_path
            index_name = "index.html"

            # Move from the URL sub-directory to the site root.
            move_to_root_dir(site.id, "#{upload_file_name}")

          elsif File.exists?("#{site.local_path}/#{upload_file_name}/index.htm")
            puts "Second"
            index_dir = site.local_path
            # elsif File.exists?("tmp/sites/#{site.directory}/#{upload_file_name}/index.htm")
            # index_dir = "tmp/sites/#{site.directory}"
            index_name = "index.htm"

            move_to_root_dir(site.id, upload_file_name)

          elsif File.exists?("#{site.local_path}/index.html")
            puts "Third"
            index_dir = site.local_path
            index_name = "index.html"

          elsif File.exists?("#{site.local_path}/index.htm")
            puts "Fourth"
            index_dir = site.local_path
            index_name = "index.htm"
          end

          # Publish to Amazon S3.
          SitePublisher.publish site

          # Get the title of the page from the HTML.
          file = File.open("#{index_dir}/#{index_name}")
          homepage = Nokogiri::HTML(file)

          # Update the database.
          file_name = index_name.split(".")

          if file_name[0] === "index" || file_name[0] === "default"
            site.label = homepage.css("title").text.to_s
            site.name = site.label.gsub(/[^0-9a-z]/i, '').gsub(" ", "_").downcase
          end

          # Build a list of pages for this site.
          find_html_files(site.id, site.local_path)

          # Added editor data to every page.
          pages = site.pages

          pages.each do |page|
            # Inject the editor into pages.
            EditabilityInjector.inject_editability("#{index_dir}", "#{index_name}","#{index_dir}",site.id, page.id)
          end

          Dir.chdir("#{Rails.root}/tmp/zips") do
            system "rm -r #{site.directory}"
          end

          # Clear the site processing_event.
          # Write to DB since websockets don't work in Unicorn/Nginx
          site.processing_event = "complete"
          site.processing_msg = nil
          site.save
          # WebsocketRails[:site_events].trigger 'new_event', { message: "Site is complete.", status: "complete", site_id: "#{site.id}", guid: "#{params[:guid]}" }

          # Ensure DB connections are closed, as threads open their own.
          ActiveRecord::Base.connection.close

          render :nothing => true, :status => 200, :content_type => 'text/html'
          return
          # render json: { message: "success" }, :status => 200
        end

        render :nothing => true, :status => 200, :content_type => 'text/html'
        return
        # render json: { message: "success" }, :status => 200
      end
    else
      #  Need to send an error header, otherwise Dropzone
      #  will not interpret the response as an error:
      render nothing: true, :status => 400, content_type: 'text/html'
      return
    end

    render :nothing => true, :status => 200, :content_type => 'text/html'
    return
  end

  def duplicate_site
    unless params[:site_id].blank?
      site = Site.find(params[:site_id])

      # Make a duplicate of the db rec.
      new_site = site.dup
      new_site.domain = nil
      new_site.generate_token("directory", 14)
      new_site.save

      # Make duplicates of each db page rec.
      site.pages.each do |page|
        new_page = page.dup
        new_page.site_id = new_site.id
        new_page.save

        # Make duplicates of each section in a page (db).
        page.sections.each do |sec|
          new_sec = sec.dup
          new_sec.page_id = new_page.id
          new_sec.save
        end
      end

      # Make a copy the files
      system "mkdir ./public/#{new_site.directory}"
      system "cp -R ./public/#{site.directory}/* ./public/#{new_site.directory}/"

      # Replace site directory references in the cloned pages with the new directory.
      new_site.pages.each do |page|
        file = File.read("#{Rails.root}/public/#{new_site.directory}/#{page.url}")
        updated_file = file.gsub("#{site.directory}", "#{new_site.directory}")

        File.open("#{Rails.root}/public/#{new_site.directory}/#{page.url}", "w") do |f|
          f.puts updated_file
        end
      end

      render json: { success: true, site: new_site }, status: 200
    else
      render json: { success: false }, status: 200
    end
  end

  def import_page
    if params[:page_url].present?
      site = SiteImporter.import(params[:page_url], current_user, true)
      render json: { success: true, site_id: site.id}, status: 200
    else
      puts "The params didn't get through."
    end
  end

  def import_site
    if params[:url].present?
      site = SiteImporter.import(params[:url], current_user, false, params[:publish_now] == "true")
      render json: { success: true, site_id: site.id}, status: 200
    else
      puts "The params didn't get through."
    end
  end

  def publish_site
    if params[:site_id].present?
      site = Site.find(params[:site_id])
      SitePublisher.publish site
      # Thread.new {  SitePublisher.publish site  }
      render json: { success: true }, :status => 200
    end
  end

  def unpublish_site
    unless params[:site_id].blank?
      # Delete the bucket from S3.
      delete_site_from_amazon(params[:site_id], "delete")

      # Delete the domain from the rec.
      site = Site.find(params[:site_id])

      site.domain = nil
      site.mode = "edit"
      site.save


      render json: { success: true }, :status => 200
    else
      render json: { success: false }, :status => 200
    end
  end

  def user_set_report_bucket
    report_bucket = current_user.report_bucket
    report_bucket = ReportBucket.create(name: "bucketbox-report-#{current_user.token}", user_id: current_user.id) unless report_bucket.present?
    if report_bucket.has_policy?
      current_user.update_attribute(:getting_reports, true)
      render json: { name: report_bucket.name }, status: 200
    else
      render json: { success: false, message: 'Internal server error' }, status: 500
    end
  end

  def user_test_report_setup
    if current_user.report_bucket.check_report_bucket()
      render json: { success: true }, status: 200
    else
      render json: { error: true }, status: 500
    end
  end

  def sites_collection
    SiteStatusChecker.check_sites_status
    @sites = current_user.sites.all
    render partial: "partials/site_#{params[:style]}" if params[:style].present?
  end

  def domains_collection
    unless params[:site_id].blank?
      site = Site.find(params[:site_id])

      if site.domains.count > 0
        @domains = site.domains.where(parent: true)
      else
        @domains = Site.where(id: params[:site_id])
      end
    end

  rescue
    return render json: { success: false, message: 'Internal server error' }, status: 500
  end

  def create_or_update_domain
    unless params[:domains].blank? || params[:site_id].blank?
      # Clear the saved domains.
      Domain.where(site_id: params[:site_id]).destroy_all

      # Inject the saved domains.
      for count in 0..params[:domains].count - 1
        domain = Domain.new
        domain.site_id = params[:site_id]
        domain.url = params[:domains]["#{count}"]["name"]
        domain.name = domain.url
        domain.save

        # Process the subdomains.
        if params[:domains]["#{count}"]["subdomains"]
          # Update the main domain to mark as a parent.
          domain.parent = true
          domain.save

          for inner_count in 0..params[:domains]["#{count}"]["subdomains"].count - 1
            sub = Domain.new
            sub.site_id = domain.site_id
            sub.url = params[:domains]["#{count}"]["subdomains"]["#{inner_count}"]["name"]
            sub.name = sub.url
            sub.parent_url_id = domain.id
          sub.save
          end
        end
      end

      render json: { success: true, message: "Site domains have been updated." }, :status => 200
    else
      render json: { success: false, message: "Site ID and domain name are required." }, :status => 200
    end
  rescue
    return render json: { success: false, message: 'Internal server error' }, status: 500

  #   unless params[:domain].blank? || params[:site_id].blank?
  #     if params[:domain_id].present?
  #       domain = Domain.find(params[:domain_id])
  #       domain.url = params[:domain]
  #       domain.save
  #     else
  #       domain = Domain.new
  #       domain.site_id = params[:site_id]
  #
  #       if params[:is_main].present?
  #         domain.parent = true
  #       else
  #         parent = Domain.find_by(site_id: params[:site_id], parent: true).try(:id)
  #         domain.parent_url_id = parent if parent.present?
  #       end
  #       domain.name = params[:domain]
  #       domain.url = params[:domain]
  #       domain.save!
  #     end
  #
  #     return render json: { success: true, domain_id: domain.id }, :status => 200
  #   end
  #
  #   render json: { success: false, message: "Site ID and domain name are required." }, :status => 200
  # rescue
  #   return render json: { success: false, message: 'Internal server error' }, status: 500
  end

  def delete_domain
    Domain.find(params[:id]).destroy!
    render json: { success: true }, status: 200
  rescue
    return render json: { success: false, message: 'Internal server error' }, status: 500
  end

  def delete_site
    unless params[:site_id].blank? || current_user.sites.find(params[:site_id]).nil?
      site = current_user.sites.find(params[:site_id])
      directory = site.directory

      # Delete the site from S3.
      delete_site_from_amazon(site.id, "delete")

      # Remove the editable folder and any temp folders.
      if Dir.exists?(site.local_path)
        system "rm -r #{site.local_path}"
      end

      if Dir.exists?("#{Rails.root}/tmp/sites/#{site.directory}/")
        system "rm -r #{Rails.root}/tmp/sites/#{site.directory}/"
      end

      if Dir.exists?("#{Rails.root}/tmp/zips/#{site.directory}/")
        system "rm -r #{Rails.root}/tmp/zips/#{site.directory}/"
      end

      # Destroy the database rec and Paperclip uploaded obj.
      site.destroy

      render json: { success: true }, :status => 200
    else
      render json: { success: false }, :status => 200
    end
  end

  def imported_data
    site = Site.find_by(directory: params[:directory])

    render xml: site.imports.first
  end

  def save_edits
    unless params[:page_id].blank?
      page = Page.find(params[:page_id])
      site = Site.find(page.site_id)
      file = File.open("#{site.local_path}/#{page.url}")

      html_page = Nokogiri::HTML(file)

      body = html_page.at_css(".fr_editor_body")

      body.inner_html = params[:edited_content]

      File.open("#{site.local_path}/#{page.url}", "w+") do |f|
        f.write html_page
      end

      render json: { success: true }, :status => 200
    else
      render json: { success: false }, :status => 200
    end
  end

  def site_pages
    site = Site.find(params[:site_id])

    pages = site.pages.where(page: true)

    render json: pages.to_json
  end

  # def page_editor
  #   #render "partials/page_editor", layout: false
  # end

  def page_html
    html = File.read("#{Rails.root}/#{site.directory}/index.html")

    puts "HTML: #{html}"

    render text: html
  end

  def delete_page
    unless params[:page_id].blank?
      page = Page.find(params[:page_id])

      # Get the page info and delete the file.
      system "rm #{page.site.local_path}/#{page.url}"

      page.destroy

      render json: { success: true }
    else
      render json: { success: false }
    end
  end

  def clone_page
    ###########################################################
    ###########################################################
    ## NEED TO INJECT GOOGLE ANALYTICS, IF PRESENT!!!!
    ###########################################################
    ###########################################################
    unless params[:page_id].blank? || params[:new_title].blank?
      puts "The title is: #{params[:new_title]}"
      new_name = params[:new_title]
      new_name = new_name.gsub(/[^0-9a-z]/i, "").gsub(" ", "_").downcase
      new_url = new_name.concat(".html")
      page = Page.find(params[:page_id])
      new_page = Page.new
      # Get the page info and duplicate the file.
      system "cp #{page.site.local_path}/#{page.url} #{page.site.local_path}/#{new_url}"

      # Open the newly created file.
      file = File.open("#{page.site.local_path}/#{new_url}")
      homepage = Nokogiri::HTML(file)

      # Select the page title and update it.
      page_title = homepage.at_css("title")
      page_title.content = "#{params[:new_title]}"

      # Write back to the new file with the new title.
      File.open("#{page.site.local_path}/#{new_url}", "w+") do |f|
        f.write homepage
      end

      new_page.site_id = page.site_id
      new_page.name = page.name
      new_page.label = params[:new_title]
      new_page.url = new_url
      new_page.page = page.page

      new_page.save

      unless new_page.site.ga_code.blank?
        analytics_handler("inject", new_page.id)
      end

      render json: { success: true, url: "#{new_url}", name: "#{new_name}", page_id: new_page.id, label: new_page.label }
    else
      render json: { success: false }
    end
  end

  # def open_pixel
  #   tracking_code = params[:tc]

  #   email = SentMail.find_by(tracking_code: tracking_code)
  #   unless email.blank?
  #     email.opened = Time.now
  #     email.save
  #   end

  #   #curr_dir = Dir.pwd
  #   image_url = "app/assets/images/open.png"

  #   respond_to do |format|
  #     format.png do
  #       File.open(image_url, 'rb') do |f|
  #         send_data f.read, :type => "image/png", :disposition => "inline"
  #       end
  #     end
  #   end
  # end

  def page_title_update
    unless params[:page_id].blank? || params[:new_title].blank?
      page = Page.find(params[:page_id])

      page.label = params[:new_title]
      page.save

      render json: { success: true }, status: 200
    else
      render json: { success: false }, status: 200
    end
  end

  def site_meta_update
    unless params[:element].blank? || params[:new_val].blank? || params[:site_id].blank?
      site = Site.find(params[:site_id])

      case "#{params[:element]}"
      when "label"
        site.label = "#{params[:new_val]}"

      when "ga_code"
        site.ga_code = "#{params[:new_val]}"
      end

      site.save

      render json: { success: true }
    else
      render json: { success: false }
    end
  end

  def analytics_handler(action, page_id)
    page = Page.find(page_id)

    case action
      when "inject"
        ga_code = <<-INSERT
          <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', "#{page.site.ga_code}", 'auto');
            ga('send', 'pageview');

          </script>
        INSERT

        puts "Google Analytics Code: #{ga_code}"

        # Make sure this is a legitimate page.
        unless page.blank?
          # Open the selected file.
          file = File.open("#{page.site.local_path}/#{page.url}")
          homepage = Nokogiri::HTML(file)

          # Select the page and inject GA.
          page_body = homepage.at_css("body")
          page_body << "#{ga_code}"

          # Write back to the file.
          File.open("#{page.site.local_path}/#{page.url}", "w+") do |f|
            f.write homepage
          end
        end
      when "scrub"
        unless page.blank?
          # Open the selected file.
          file = File.open("#{page.site.local_path}/#{page.url}").read
          file.gsub!(/(\(function\(i,s,o,g,r,a,m\){i\[\'GoogleAnalyticsObject)(.*?)(ga\('send', 'pageview'\);)/m, "")

          # Write back to the file.
          File.open("#{page.site.local_path}/#{page.url}", "w+") do |f|
            f.write file
          end
        end
      end
  end

  def move_to_root_dir(site_id, url)
    site = Site.find(site_id)

    # Move the site out from directory/site.domain.name to simply directory.
    system "mv #{site.local_path}/#{url} #{Rails.root}/public"
    system "rm -R #{site.local_path}"
    puts "url.host: #{url}, and site.directory: #{site.directory}"
    system "mv #{Rails.root}/public/#{url} #{site.local_path}"
  end

  def delete_site_from_amazon(site_id, action)
    site_id = site_id || params[:site_id]
    unless site_id.blank? || current_user.sites.find(site_id).nil?
      site = current_user.sites.find(site_id)
      main_bucket = site.amazon_bucket_name
      buckets = site.domains.pluck(:url).map {|domain| "#{domain}.#{main_bucket}"}
      buckets << main_bucket
      #Thread.new do
        # Open a connection to the users AWS account and blast the bucket.
        amazon_key = AmazonKey.find_by(user_id: current_user.id)

        access_key = params[:access_key] || amazon_key.access_key
        secret_key = params[:secret_key] || amazon_key.secret_access_key

        Aws.config.update({
            region: 'us-east-1',
            credentials: Aws::Credentials.new(access_key, secret_key)
        })

        # s3 = Aws::S3.new
        # s3 = Aws::S3::Client.new

        buckets.each do |bucket_name|
          bucket = Aws::S3::Bucket.new({ name: bucket_name })
          # bucket = s3.buckets[bucket_name]

          if bucket.exists?
            # Delete the bucket and all it's contents.
            if action == "delete"
              begin
                bucket.delete!
              rescue
                # Seems there was no bucket to delete!
              end
            else
              bucket.clear!
            end
          end
        end
      #end
    end

    return
  end
end

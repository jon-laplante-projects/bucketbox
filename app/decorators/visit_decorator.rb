class VisitDecorator < BaseDecorator
  include DomainHelpers

  def started_at
    I18n.l object.started_at, format: :short
  end

  def screen_resolution
    "#{object.screen_width} x #{object.screen_height}"
  end

  def lat_lon
    "#{object.latitude}, #{object.longitude}"
  end
end

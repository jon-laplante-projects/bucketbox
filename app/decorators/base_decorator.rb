require 'delegate'

class BaseDecorator < SimpleDelegator

  attr_reader :object

  def initialize(base)
    super base
    @object = base
  end

  def self.decorate_collection(collection)
    collection.map do |obj|
      new obj
    end
  end

  def created_at
    I18n.l object.created_at, format: :short
  end

  def updated_at
    I18n.l object.updated_at, fortmat: :short
  end

  private

  def h
    ActionController::Base.helpers
  end

  alias_method :helpers, :h
end

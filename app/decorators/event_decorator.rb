class EventDecorator < BaseDecorator
  include DomainHelpers

  def time
    I18n.l object.time, format: :short
  end

  def page
    object.properties['page']
  end
end

module DomainHelpers
  def domain_name
    object.domain.present? ? object.domain.name : object.domain_id
  end

  def domain_link
    if object.domain.present?
      h.link_to domain_name, "http://#{domain_name}/", target: '_blank'
    else
      domain_name
    end
  end
end

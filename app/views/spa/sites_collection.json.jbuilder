json.sites @sites.each do |site|
	json.id site.id

	tmpl = site.template
	tmpl.thumbnail = image_path("templates/#{site.template.thumbnail}") unless site.template_id.blank?

	json.template tmpl

	json.label site.label#.truncate(50)
	json.ga_code site.ga_code
	json.mode site.mode
	json.directory site.directory
	json.created_at site.created_at.strftime("%B %e, %Y")
	json.processing_event site.processing_event
	json.processing_msg site.processing_msg

  if site.domains.count < 1
    d = "http://#{site.domain}" if site.domain? || nil
  else
    d = "http://#{site.domains.first}"
  end

	# d = site.domain
	# if !site.ready?
	#     d = nil
	# elsif !(site.domain.blank? || site.domain[0,4] == "http")
	# 	d = "http://#{site.domain}"
	# end

	json.domain d

	pages = site.pages.where(page: true)
	page_arr = Array.new;

	pages.each do |page|
		page_hash = Hash.new;

		page_hash["id"] = page.id
		page_hash["url"] = page.url
		page_hash["label"] = page.label

		page_arr << page_hash
	end

	json.pages page_arr
end

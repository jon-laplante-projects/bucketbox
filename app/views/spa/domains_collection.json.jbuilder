unless @domains.blank?
  d = @domains.columns.map {|c| c.name}

  if d.include? "site_id"
    # This site DOES have domains.
    json.domains @domains.each do |domain|
      json.id domain.id
      json.url domain.url
      json.parent domain.parent
      json.parent_url_id nil
      subdomains = Domain.where(parent_url_id: domain.id, parent: nil).order('id DESC')
      json.subdomains subdomains.each do |subdomain|
        json.id subdomain.id
        json.url subdomain.url
      end
    end
  else
    # This site doesn't have any custom domains.
    json.domains @domains.each do |domain|
      unless domain.domain.blank?
        json.id domain.id
        json.url domain.domain
        json.parent false
        json.parent_url_id nil
        json.subdomains []
      end
    end
  end
end
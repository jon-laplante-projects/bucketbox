json.videos @videos.each do |video|
	# width, height = image.dimensions
	
	json.id video.id
	json.file_name video.file_file_name
	json.label video.label
	json.tall video.tall

  json.bucket_id video.bucket_id

  if video.bucket_id?
    json.bucket_name Bucket.find(video.bucket_id).label
  else
    json.bucket_name nil
  end

	json.thumbnail_size_url video.file.url(:thumb)
	json.preview_size_url video.file.url(:preview)
	json.original_size_url video.file.url(:original)
	json.file_type video.file_content_type
	json.file_size "#{video.file_file_size/1024/1024} mb"
	json.created_at video.created_at.strftime("%B %e, %Y")
	#json.meta video.file_meta
	
	meta = eval(video.file_meta)
	json.aspect meta[:aspect].round(4)
	json.length meta[:length]
	json.duration meta[:duration]
	json.height meta[:height]
	json.width meta[:width]
	json.dimensions meta[:size]
end
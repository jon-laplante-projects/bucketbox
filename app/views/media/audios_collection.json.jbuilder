json.audios @audios.each do |audio|
	json.id audio.id
	json.file_name audio.file_file_name
	json.file_content_type audio.file_content_type
	json.file_size audio.file_file_size
	json.file_uploaded audio.file_updated_at
	json.download_url audio.file.url(:original)

  json.bucket_id audio.bucket_id

  if audio.bucket_id?
    json.bucket_name Bucket.find(audio.bucket_id).label
  else
    json.bucket_name nil
  end
	
	begin # This is not the best way to handle this, but doing for the sake of time.
		meta = eval(audio.metadata)
		
		json.title meta["title"]
		json.artist meta["artist"]
		json.album meta["album"]
		json.year meta["year"]
		json.comments meta["comments"]
		json.genre meta["genre"]
		json.genre_s meta["genre_s"]
	rescue
		json.title nil
		json.artist nil
		json.album nil
		json.year nil
		json.comments nil
		json.genre nil
		json.genre_s nil
	end
end
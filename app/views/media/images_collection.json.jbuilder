json.images @images.each do |image|
	width, height = image.dimensions
	
	json.id image.id
	json.name image.file_file_name
	json.dimensions "#{width}x#{height}"
	json.file_type image.file_content_type
	json.created_at image.created_at.strftime("%B %e, %Y")
	json.file_size image.file_file_size
  json.bucket_id image.bucket_id

  if image.bucket_id?
    json.bucket_name Bucket.find(image.bucket_id).label
  else
    json.bucket_name nil
  end

	json.original_url image.file.url(:original)
	json.preview_url image.file.url(:medium)
	json.thumbnail_url image.file.url(:thumb)
end
json.videos @videos.each do |video|
	json.id video.id
	json.file_name video.file_file_name
	json.file_type video.file_content_type
	json.file_size video.file_file_size
	json.tall video.tall
	json.created_at video.created_at
	json.preview_size_url video.file.url(:preview)
	json.thumbnail_size_url video.file.url(:thumb)
	json.original_size_url video.file.url(:original)
	json.label video.label

	meta = eval(video.file_meta)
	json.aspect meta[:aspect].round(4)
	json.length meta[:length]
	json.duration meta[:duration]
	json.height meta[:height]
	json.width meta[:width]
	json.dimensions meta[:size]
end
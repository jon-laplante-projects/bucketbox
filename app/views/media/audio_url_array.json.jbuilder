json.audios @audios.each do |audio|
	json.id audio.id
	json.file_name audio.file_file_name
	json.file_content_type audio.file_content_type
	json.file_size audio.file_file_size
	json.file_uploaded audio.file_updated_at
  json.image_placeholder ActionController::Base.helpers.image_tag("audio_placeholder.png", style: "max-height: 200px; max-width: 200px;")
	json.download_url audio.file.url(:original)
	
	begin # This is not the best way to handle this, but doing for the sake of time.
		meta = eval(audio.metadata)
		
		json.title meta["title"]
		json.artist meta["artist"]
		json.album meta["album"]
		json.year meta["year"]
		json.comments meta["comments"]
		json.genre meta["genre"]
		json.genre_s meta["genre_s"]
	rescue
		json.title nil
		json.artist nil
		json.album nil
		json.year nil
		json.comments nil
		json.genre nil
		json.genre_s nil
	end
end
json.buckets @buckets.each do |bucket|
  json.id bucket.id
  json.label bucket.label

  json.audios Audio.where(bucket_id: bucket.id)
  json.images Image.where(bucket_id: bucket.id).each do |img|
    json.thumbnail img.file.url(:thumb)
    json.id img.id
    json.user_id img.user_id
    json.name img.name
    json.label img.label
    json.file_file_name img.file_file_name
    json.file_content_type img.file_content_type
    json.file_file_size img.file_file_size
    json.file_updated_at img.file_updated_at
    json.created_at img.created_at
    json.updated_at img.updated_at
    json.dimensions img.dimensions
    json.bucket_id img.bucket_id
  end
  json.videos Video.where(bucket_id: bucket.id).each do |vid|
    json.thumbnail vid.file.url(:thumb)
    json.id vid.id
    json.user_id vid.user_id
    json.name vid.name
    json.label vid.label
    json.file_file_name vid.file_file_name
    json.file_content_type vid.file_content_type
    json.file_file_size vid.file_file_size
    json.file_updated_at vid.file_updated_at
    json.created_at vid.created_at
    json.updated_at vid.updated_at
    json.file_meta vid.file_meta
    json.tall vid.tall
    json.bucket_id vid.bucket_id
  end
  json.docs Document.where(bucket_id: bucket.id)
end
json.files @documents.each do |doc|
  json.id doc.id
  json.file_name doc.file_file_name
  json.bucket_id doc.bucket_id
  json.file_content_type doc.file_content_type
  json.file_size doc.file_file_size

  if doc.bucket_id != nil
    json.bucket_name Bucket.find(doc.bucket_id).label
  else
    json.bucket_name nil
  end

  json.file_uploaded doc.created_at
  json.download_url doc.file.url
  json.metadata doc.metadata
end
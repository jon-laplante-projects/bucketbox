class EditabilityInjector
  def self.inject_editability(file_location, page_url, index_dir, site_id, page_id)
    # Open the current site rec from the database.
    site = Site.find(site_id)

    # Get the title of the page from the HTML.
    file = File.open("#{file_location}/#{page_url}")
    homepage = Nokogiri::HTML(file)

    # Update the database.
    if site.label == "Untitled" || site.label.blank?
      site.label = homepage.css("title").text#.to_s.truncate(100)
      site.name = site.label.gsub(/[^0-9a-z]/i, '').gsub(" ", "_").downcase
      site.save
    end

    # Rewrite URLs in the index file.
    homepage.css("script").each do |s|
      uri = URI("#{s.attributes["src"].to_s}")
      # Check to see if rewrites have already happened.
      unless uri.to_s.include?("#{site.directory}")
        unless uri.to_s.include?("http") || uri.to_s[0,2] === "//" || uri.to_s[0,3] === "../" || uri.to_s.blank?
          path_arr = uri.to_s.split("/")
          path_arr.unshift("/#{site.directory}")
          unless s.attributes["src"].blank?
            s.attributes["src"].value = path_arr.join("/")
          end
        end
      end
    end

    # URLs for style links.
    homepage.css("link").each do |s|
      uri = URI("#{s.attributes["href"].to_s}")
      # Check to see if rewrites have already happened.
      unless uri.to_s.include?("#{site.directory}")
        unless uri.to_s.include?("http") || uri.to_s[0,2] === "//" || uri.to_s[0,3] === "../" || uri.to_s.blank?
          path_arr = uri.to_s.split("/")
          path_arr.unshift("/#{site.directory}")
          s.attributes["href"].value = path_arr.join("/")
        end
      end
    end

    # URLs for images.
    homepage.css("img").each do |i|
      begin
        uri = URI("#{i.attributes["src"].to_s}")
        # Check to see if rewrites have already happened.
        unless uri.to_s.include?("#{site.directory}")
          unless uri.to_s.include?("http")
            path_arr = uri.to_s.split("/")
            path_arr.unshift("/#{site.directory}")
            unless i.attributes["src"].blank? || i.attributes["src"].value.blank?
              i.attributes["src"].value = path_arr.join("/")
            end
          end
        end
      rescue
        i.attributes["src"].value = "/#{site.directory}/#{i.attributes["src"].to_s}"
      end
    end

    # CSS @imports.
    # @import .*\;
    homepage.css("link").each do |s|
      uri = URI("#{s.attributes["href"].to_s}")
      # Check to see if rewrites have already happened.
      unless uri.to_s.include?("#{site.directory}")
        unless uri.to_s.include?("http") || uri.to_s[0,2] === "//" || uri.to_s[0,3] === "../" || uri.to_s.blank?
          path_arr = uri.to_s.split("/")
          css_import_file = File.open("#{file_location}/#{page_url}")
          #css_import_file = File.open("#{Rails.root}/tmp/sites/#{path_arr.join("/")}")

          if css_import_file.include? "@import"
            css_import_file.gsub!(/@import .*\;/) do |import|
              unless import.include?("http") || import.include?("..")
                str = import.gsub("@imports", "")
                str = str.gsub!("\"", "")
                str = str.gsub!("\'", "")
                str_arr = str.split("/")
                str_arr.unshift("#{site.directory}")
                str = str_arr.join("/")
                import = "@import \"#{str}\";"
              end
            end
          end
          # CSS url();
          # ^url\(.*\)
          if css_import_file.include? "url("
            css_import_file.gsub!(/^url\(.*\);/) do |url|
              unless url.include?("http") || url.include?("..")
                str = url.gsub("url(", "")
                str = url.gsub(");", "")
                str = str.gsub!("\"", "")
                str = str.gsub!("\'", "")
                str_arr = str.split("/")
                str_arr.unshift("#{site.directory}")
                str = str_arr.join("/")
                url = "url(\"#{str}\");"
              end
            end
          end
        end
      end
    end

    # Inject the Froala assets.
    unless File.directory?("#{site.local_path}/froala")
      system "mkdir #{site.local_path}/froala/"
      system "cp #{Rails.root}/vendor/assets/javascripts/froala_editor.min.js /#{file_location}/froala/"
      system "cp #{Rails.root}/vendor/assets/javascripts/froala_editor_ie8.min.js /#{file_location}/froala/"
      system "cp -avR #{Rails.root}/vendor/assets/javascripts/plugins/ /#{file_location}/froala/"
      system "cp #{Rails.root}/vendor/assets/stylesheets/froala_editor.min.css /#{file_location}/froala/"
      system "cp #{Rails.root}/vendor/assets/stylesheets/froala_content.min.css /#{file_location}/froala/"
      system "cp #{Rails.root}/vendor/assets/stylesheets/froala_style.min.css /#{file_location}/froala/"
    end

    # Inject the editor markup
    inserted_code = <<-INSERT
<link class="editor_injection" rel="stylesheet" href="/#{site.directory}/froala/froala_editor.min.css" />
<link class="editor_injection" rel="stylesheet" href="/#{site.directory}/froala/froala_style.min.css" />
<link class="editor_injection" rel="stylesheet" href="/#{site.directory}/froala/froala_content.min.css" />
<link class="editor_injection" rel='stylesheet' href='http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css'>
<link href="http://vjs.zencdn.net/4.10/video-js.css" rel="stylesheet" />
<script src="http://vjs.zencdn.net/4.10/video.js"></script>
<script class="editor_injection" type='text/javascript' src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script class="editor_injection" type="text/javascript" src="/#{site.directory}/froala/froala_editor.min.js"></script>
<!--script class="editor_injection" type="text/javascript" src="/#{site.directory}/froala/froala_editor_ie8.min.js"></script-->
<script class="editor_injection" type="text/javascript" src="/#{site.directory}/froala/block_styles.min.js"></script>
<script class="editor_injection" type="text/javascript" src="/#{site.directory}/froala/colors.min.js"></script>
<script class="editor_injection" type="text/javascript" src="/#{site.directory}/froala/char_counter.min.js"></script>
<script class="editor_injection" type="text/javascript" src="/#{site.directory}/froala/file_upload.min.js"></script>
<script class="editor_injection" type="text/javascript" src="/#{site.directory}/froala/font_family.min.js"></script>
<script class="editor_injection" type="text/javascript" src="/#{site.directory}/froala/font_size.min.js"></script>
<script class="editor_injection" type="text/javascript" src="/#{site.directory}/froala/fullscreen.min.js"></script>
<script class="editor_injection" type="text/javascript" src="/#{site.directory}/froala/lists.min.js"></script>
<script class="editor_injection" type="text/javascript" src="/#{site.directory}/froala/inline_styles.min.js"></script>
<script class="editor_injection" type="text/javascript" src="/#{site.directory}/froala/media_manager.min.js"></script>
<script class="editor_injection" type="text/javascript" src="/#{site.directory}/froala/tables.min.js"></script>
<script class="editor_injection" type="text/javascript" src="/#{site.directory}/froala/urls.min.js"></script>
<script class="editor_injection" type="text/javascript" src="/#{site.directory}/froala/video.min.js"></script>
<script class="editor_injection" type="text/javascript">
var page_id = #{page_id};
var selection;
$(function() {

  // code for saving cursor position
  var saveSelection, restoreSelection;

  if (window.getSelection && document.createRange) {
      saveSelection = function(containerEl) {
          var range = window.getSelection().getRangeAt(0);
          var preSelectionRange = range.cloneRange();
          preSelectionRange.selectNodeContents(containerEl);
          preSelectionRange.setEnd(range.startContainer, range.startOffset);
          var start = preSelectionRange.toString().length;

          return {
              start: start,
              end: start + range.toString().length
          }
      };

      restoreSelection = function(containerEl, savedSel) {
          var charIndex = 0, range = document.createRange();
          range.setStart(containerEl, 0);
          range.collapse(true);
          var nodeStack = [containerEl], node, foundStart = false, stop = false;
          
          while (!stop && (node = nodeStack.pop())) {
              if (node.nodeType == 3) {
                  var nextCharIndex = charIndex + node.length;
                  if (!foundStart && savedSel.start >= charIndex && savedSel.start <= nextCharIndex) {
                      range.setStart(node, savedSel.start - charIndex);
                      foundStart = true;
                  }
                  if (foundStart && savedSel.end >= charIndex && savedSel.end <= nextCharIndex) {
                      range.setEnd(node, savedSel.end - charIndex);
                      stop = true;
                  }
                  charIndex = nextCharIndex;
              } else {
                  var i = node.childNodes.length;
                  while (i--) {
                      nodeStack.push(node.childNodes[i]);
                  }
              }
          }

          var sel = window.getSelection();
          sel.removeAllRanges();
          sel.addRange(range);
      }
  } else if (document.selection && document.body.createTextRange) {
      saveSelection = function(containerEl) {
          var selectedTextRange = document.selection.createRange();
          var preSelectionTextRange = document.body.createTextRange();
          preSelectionTextRange.moveToElementText(containerEl);
          preSelectionTextRange.setEndPoint("EndToStart", selectedTextRange);
          var start = preSelectionTextRange.text.length;

          return {
              start: start,
              end: start + selectedTextRange.text.length
          }
      };

      restoreSelection = function(containerEl, savedSel) {
          var textRange = document.body.createTextRange();
          textRange.moveToElementText(containerEl);
          textRange.collapse(true);
          textRange.moveEnd("character", savedSel.end);
          textRange.moveStart("character", savedSel.start);
          textRange.select();
      };
  }
  
  var savedSelection;

  function saveCursorPosition() {
    savedSelection = saveSelection( $('.froala-view')[0] );
  }

  function restoreCursorPosition() {
    if (savedSelection) {
      restoreSelection($('.froala-view')[0], savedSelection);
    }
  }   

  $(".fr_editor_body").editable({ 
    inlineMode: true,
    alwaysVisible: true,
    countCharacters: false,
    saveURL: "/site/edit",
    autosave: false,
    autosaveInterval: 2500,
    saveParam: "edited_content",
    saveParams: { page_id: page_id },
    key: "EfpelmA-63hdtA6f1jd==",
    fileUploadURL: "/upload/images",
    mediaManager: true, 
    imagesLoadURL: "/images/array.json", 
    imageDeleteURL: "/page/image/delete", 
    buttons: [
      's3Video',
      's3Audio',
      'insertSnippet',
      'insertForm',
      'editTitle',
      'bold',
      'italic',
      'underline',
      'strikeThrough',
      'subscript',
      'superscript',
      'fontFamily',
      'fontSize',
      'color',
      'formatBlock',
      'blockStyle',
      'inlineStyle',
      'align',
      'insertOrderedList',
      'insertUnorderedList',
      'outdent',
      'indent',
      'selectAll',
      'createLink',
      'insertImage',
      'insertVideo',
      'table',
      'undo',
      'redo',
      'html',
      'insertHorizontalRule',
      'uploadFile',
      'removeFormat',
      'fullscreen',
      'save'
    ],
    customButtons: { 
      s3Video: {
      title: 'Video from S3',
      icon: {
        type: 'font',
        value: 'fa fa-file-video-o'
      },
  callback: function () {
    $("body").prepend( $("#video_insertion").html() );

    $.getJSON("/videos/array.json", function(data){
      if (data != null) {

        var videos = [];
        $.each(data, function(key, val){
          videos.push('<div class="media_overlay s3-image-div" data-link="' + val[0].original + '"><a href="javascript:void(0);"><img src="' + val[0].thumbnail_size_url + '" alt="" style="max-height: 200px; max-width: 200px;"><div class="s3-video-desc">' + val[0].label + '</div></a></div>');
        });

        $("#s3_video_list").html( videos );

        // $(".fr_editor_body").editable("hide");
      };
    });
  },
  // refresh: function () {
  //   alert("Refresh button clicked.");
  // }
  },
  s3Audio: {
    title: 'Audio File from S3',
    icon: {
      type: 'font',
      value: 'fa fa-volume-up'
    },
    callback: function () {
      $("body").prepend( $("#audio_insertion").html() );

      $.getJSON("/audios/array.json", function(data){
        if (data !== null) {
          var audios = [];
          $.each(data["audios"], function(key, val){
            var audio_title;

            if (val.title !== null) {
              if (val.artist !== null) {
                audio_title = val.title + ", " + val.artist;
              } else {
                audio_title = val.title;
              };
            } else {
              audio_title = val.file_file_name;
            };

            audios.push('<div class="media_overlay s3-audio-div" data-link="' + val.download_url + '" data-content-type="' + val.file_content_type + '" data-title="' + audio_title + '"><a href="javascript:void(0);">' + val.image_placeholder + '<div class="s3-audio-desc">' + val.label + '</div></a><br/><small>' + audio_title + '</small></div>');
          });

          $("#s3_audio_list").html( audios );

          $(".fr_editor_body").editable("hide");
        };
      });
    }
  },
  insertSnippet: {
    title: 'Insert Code Snippet',
    icon: {
      type: 'font',
      value: 'fa fa-file-code-o'
    },
    callback: function() {
      saveCursorPosition();
      var _this = this;
      $('body').prepend( $("#snippet_insertion").html() );
      $('.froala-editor').hide();

      $('.snippet_insertion_modal button').mousedown(function(){
        var textToInsert = $(this).siblings('textarea').val()
        restoreCursorPosition()
        _this.insertHTML(textToInsert);
        $(".snippet_insertion_modal").remove();
      });
    }
  },
  insertForm: {
    title: 'Insert Form Script',
    icon: {
      type: 'font',
      value: 'fa fa-list-alt'
    },
    callback: function() {
      saveCursorPosition();
      var _this = this;
      $('body').prepend( $("#form_insertion").html() );
      $('.froala-editor').hide();

      $('.form_insertion_modal button').mousedown(function(){
        var textToInsert = $(this).siblings('textarea').val()
        restoreCursorPosition()
        _this.insertHTML(textToInsert);
        $(".form_insertion_modal").remove();
      });
    }
  },
  editTitle: {
    title: 'Edit Page Title',
    icon: {
      type: 'font',
      value: 'fa fa-file'
    },
    callback: function () {
      $("body").prepend( $("#page_title_update").html() );

      $("#page_title_txt").val( $("title").text() );
    }
  }
} });

  // Destroy the video dialog when closed.
  $("body").on("click", ".close-media-container", function(){
    $(".parent-media-container").hide();
    $("body").remove(".parent-media-container");
  });

  // Insert a video into the site.
  $("body").on("click", ".s3-image-div", function(){
    var link = $(this).attr("data-link");
    var thumbnail = $("img", this).attr("src");

 $(".fr_editor_body").editable("insertHTML", '<video id="example_video_1" class="video-js vjs-default-skin" controls preload="auto" width="640" height="480" poster="' + thumbnail + '" data-setup=\\'{"example_option":true}\\'><source src="' + link + '" type="video/mp4" /><p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p></video>', true);

    $(".parent-media-container").hide();
    $("body").remove(".parent-media-container");
  });

  // Insert audio into the site.
  $("body").on("click", ".s3-audio-div", function(){
    var link = $(this).attr("data-link");
    var content_type = $(this).attr("data-content-type");
    var title = $(this).attr("data-title");

 $(".fr_editor_body").editable("insertHTML", '<div><audio controls><source src="' + link + '" type="' + content_type + '">Your browser does not support the audio element.</audio><p><small>' + title + '</small></p></div>', true);

    $(".parent-media-container").hide();
    $("body").remove(".parent-media-container");
  });

  // If the title textbox is empty, the user can't save.
  $("body").on("keyup change", "#page_title_txt", function(){
    if ( $("#page_title_txt").val() !== "" ) {
      $("#title_save_btn").prop("disabled", false);
    } else {
      $("#title_save_btn").prop("disabled", true);
    };
  });

  // Write the new page title.
  $("body").on("click", "#title_save_btn", function(){
    $("title").text( $("#page_title_txt").val() );

    $(".parent-media-container").hide();

    // Update the page <title/>.
    $("body").remove(".parent-media-container");

    // Push the change to the DB.
    payload = { page_id: page_id, new_title: $("#page_title_txt").val() };
    $.ajax({
        url: "/page/title/update",
        type: "post",
        data: payload,
        success: function(data){
          if( data["success"] === false ){
            alert("Can't update title.");
          }
        },
        error: function(data){
          alert("Can't update title.");
        }
      });
  });
});
</script>
<style class="editor_injection">
  .parent-media-container { position: absolute; /* height: 100%; */ width: 100%; z-index: 99; }
  .media-container { border: 1px solid black; border-top: 5px solid black; max-width: 600px; background-color: white; margin: 0 auto; padding-bottom: 50px; }
  /*.media-container div { margin: 10px; }*/
  .media-container  h1, .media-container h2 { border-bottom: 1px solid #eeeeee; }
  .media-body { width: 600px; }
  .media-body input, textarea { width: 85%; }
  .close-media-container { float: right; cursor: pointer; }
  .embedded, .youtube { width: 95%; margin-top: 25px; }
  .youtube button, .youtube input { float: left; }
  .editor_injection > .media-head, .embedded, .youtube { margin: 10px; }

  .media-overlay-container { display: block; }
  .media_overlay a { color:white; }
  .media_overlay { display: block; padding:0; margin:0; }
  div.media_overlay { position:relative; float:left; display:inline; list-style-type:none; padding:0; margin:10px; margin-bottom:10px; }
  div.media_overlay .s3-video-desc { position:absolute; width:100%; bottom:0; left:0; background-color:rgba(0, 0, 0, 0.5); filter:alpha(opacity=50); }
  div.media_overlay .s3-video-desc div { padding:5px; text-align:center; }
  div.media_overlay img { vertical-align:top; border:1px solid gray; }

  .bbx-alert {
    padding: 15px;
    margin-bottom: 20px;
    border: 1px solid transparent;
    border-radius: 4px;
  }
  .bbx-alert h4 {
    margin-top: 0;
    color: inherit;
  }
  .bbx-alert .bbx-alert-link {
    font-weight: bold;
  }
  .bbx-alert > p,
  .bbx-alert > ul {
    margin-bottom: 0;
  }
  .bbx-alert > p + p {
    margin-top: 5px;
  }
  .bbx-alert-dismissable,
  .bbx-alert-dismissible {
    padding-right: 35px;
  }
  .bbx-alert-dismissable .close,
  .bbx-alert-dismissible .close {
    position: relative;
    top: -2px;
    right: -21px;
    color: inherit;
  }
  .bbx-alert-success {
    color: #3c763d;
    background-color: #dff0d8;
    border-color: #d6e9c6;
  }
  .bbx-alert-success hr {
    border-top-color: #c9e2b3;
  }
  .bbx-alert-success .bbx-alert-link {
    color: #2b542c;
  }
  .bbx-alert-info {
    color: #31708f;
    background-color: #d9edf7;
    border-color: #bce8f1;
  }
  .bbx-alert-info hr {
    border-top-color: #a6e1ec;
  }
  .bbx-alert-info .bbx-alert-link {
    color: #245269;
  }
  .bbx-alert-warning {
    color: #8a6d3b;
    background-color: #fcf8e3;
    border-color: #faebcc;
  }
  .bbx-alert-warning hr {
    border-top-color: #f7e1b5;
  }
  .bbx-alert-warning .bbx-alert-link {
    color: #66512c;
  }
  .bbx-alert-danger {
    color: #a94442;
    background-color: #f2dede;
    border-color: #ebccd1;
  }
  .bbx-alert-danger hr {
    border-top-color: #e4b9c0;
  }
  .bbx-alert-danger .bbx-alert-link {
    color: #843534;
  }
</style>
<script type="text/template" id="video_insertion" class="editor_injection">
  <div class="parent-media-container fr_editor_body">
    <div class="editor_injection media-container">
        <div class="media-head">
          <span class="fa fa-close close-media-container"></span>
          <h1>S3 Hosted Videos</h1>
        </div>
        <div class="media-body">
          <div class="s3-hosted">
            <div class="media_overlay_container" id="s3_video_list">
            <h3>You do not yet have any videos hosted on S3.</h3>
            </div>
          </div>
          <div style="clear: both;"></div>
          <div class="embedded">
            <h2>Embedded Video<br/><small>Vimeo, Wistia, etc.</small></h2>
            <textarea name="" id="" cols="30" rows="10"></textarea><br/>
            <button><span class="fa fa-code"></span>&nbsp;Save</button>
          </div>
          <div class="youtube">
            <h2>YouTube Link</h2>
            <input type="text" placeholder="http://youtube.com/example/video" />
            <button><span class="fa fa-youtube"></span>&nbsp;Save</button>
          </div>
        </div>
      </div>
    </div>
  </script>
  <script type="text/template" id="audio_insertion" class="editor_injection">
  <div class="parent-media-container fr_editor_body">
    <div class="editor_injection media-container">
        <div class="media-head">
          <span class="fa fa-close close-media-container"></span>
          <h1>S3 Hosted Audio Files</h1>
        </div>
        <div class="media-body">
          <div class="s3-hosted">
            <div class="media_overlay_container" id="s3_audio_list">
            <h3>You do not yet have any audio files hosted on S3.</h3>
            </div>
          </div>
          <div style="clear: both;"></div>
          <div class="embedded">
            <h2>Embedded Audio<br/><small>SoundCloud, etc.</small></h2>
            <textarea name="" id="" cols="30" rows="10"></textarea><br/>
            <button><span class="fa fa-code"></span>&nbsp;Save</button>
          </div>
        </div>
      </div>
    </div>
  </script>
  <script type="text/template" id="form_insertion" class="editor_injection">
    <div class="form_insertion_modal parent-media-container fr_editor_body">
      <div class="editor_injection media-container">
        <div class="media-head">
          <span class="fa fa-close close-media-container"></span>
          <h1>Insert Form Script</h1>
        </div>
        <div class="media-body">
          <div class="embedded">
            <small>Paste a form script to insert.</small></h2>
            <textarea name="" class="insert_textarea" cols="30" rows="10"></textarea><br/>
            <button class="insertHtml_btn"><span class="fa fa-code"></span>&nbsp;Insert</button>
          </div>
        </div>
      </div>
    </div>
  </script>
  <script type="text/template" id="snippet_insertion" class="editor_injection">
    <div class="snippet_insertion_modal parent-media-container fr_editor_body">
      <div class="editor_injection media-container">
        <div class="media-head">
          <span class="fa fa-close close-media-container"></span>
          <h1>Insert Code Snippet</h1>
        </div>
        <div class="media-body">
          <div class="embedded">
            <small>Enter a code snippet to insert.</small></h2>
            <textarea name="" class="insert_textarea" cols="30" rows="10"></textarea><br/>
            <button><span class="fa fa-code"></span>&nbsp;Insert</button>
          </div>
        </div>
      </div>
    </div>
  </script>
  <script type="text/template" id="page_title_update" class="editor_injection">
  <div class="parent-media-container fr_editor_body">
    <div class="editor_injection media-container">
        <div class="media-head">
          <span class="fa fa-close close-media-container"></span>
          <h1>Edit Page Title</h1>
        </div>
        <div class="media-body">
          <div class="s3-hosted" style="display: flex; justify-content: center; width: 100%;">
            <div id="update_title" style="width: 90%;">
            <input type="text" name="page_title_txt" id="page_title_txt" style="width: 100%; margin-bottom: 10px;" placeholder="" value=""/>
            <button id="title_save_btn"><span class="fa fa-code"></span>&nbsp;Save</button>
            </div>
          </div>
          <div style="clear: both;"></div>
        </div>
      </div>
    </div>
  </script>
                  INSERT

    # Inject the HTML with editor markup.
    page_body = homepage.at_css("body")

    page_body["class"] = "fr_editor_body #{page_body["class"]}"

    page_head = homepage.at_css("head")
    ###########################################################
    # TODO: Address this more fully later.
    ###########################################################
    unless page_head.blank?
      page_head << inserted_code
    end

    # Save newly marked up page.
    file = File.open("#{file_location}/#{page_url}", "w+") do |f|
      f.write homepage
    end
  end
end
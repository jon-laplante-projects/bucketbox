class SiteImporter
  include SpasHelper

  def initialize(url, user, one_page, publish_now)
    @url = url
    @user = user
    @one_page = one_page
    @publish_now = publish_now
  end

  def self.import(url, user, one_page=false, publish_now=false)
    SiteImporter.new(url, user, one_page, publish_now).import
  end

  def import
    create_site
    @uri = get_uri(@url)

    if @uri.blank?
      set_site_status :error
      return @site
    end

    Thread.new do
      set_site_status(:processing) unless @one_page
      
      @pages = load_pages
      move_pages
      if @pages.blank?
        set_site_status :error
        ActiveRecord::Base.connection.close
      
        break
      end

      if @publish_now
        site.mode = "published"
        site.save
        SitePublisher.publish site
      end

      add_editor_to_pages

      # Persist state to DB to replace websockets since they can't be used in Nginx/Unicorn.
      set_site_status :complete
      
      # because thread
      ActiveRecord::Base.connection.close
    end

    @site
  end

  private

  SITE_IMPORTING_STATUSES = {
    record: "<span class='fa fa-database'></span>&nbsp;&nbsp;Site recorded in database.",
    processing: "<span class='fa fa-archive'></span>&nbsp;&nbsp;Cloning website. This may take a long time.",
    editor: "<span class='fa fa-pencil-square-o'></span>&nbsp;&nbsp;Site successfully cloned. Adding editing capability.",
    error: "<span class='fa fa-pencil-square-o'></span>&nbsp;&nbsp;Unable to clone.",
    complete: nil
  }

  def get_uri(string)
    uri = URI.parse(string)
    %w( http https ).include?(uri.scheme) ? uri : nil
  rescue URI::BadURIError
    nil
  rescue URI::InvalidURIError
    nil
  end

  def set_site_status(status)
    @site.processing_event = status.to_s
    @site.processing_msg = SITE_IMPORTING_STATUSES[status]
    @site.save
  end

  def create_site
    @site = Site.new

    @site.user_id = @user.id
    @site.name = "untitled"
    @site.label = @url
    @site.template_id = 6
    @site.mode = "edit"
    @site.generate_token("directory", 14)

    set_site_status :record
  end

  def load_pages
    system "mkdir #{@site.local_path}"

    Dir.chdir(@site.local_path) { @one_page ? wget_one_page : wget_site }
    
    find_html_files(@site.id, "#{@site.local_path}/#{@uri.host}")
    @site.pages
  rescue
    nil
  end

  def move_pages
    # Move the site out from directory/site.domain.name to simply directory.
    system "mv #{@site.local_path}/#{@uri.host} #{Rails.root}/public"
    system "rm -R #{@site.local_path}"
    system "mv #{Rails.root}/public/#{@uri.host}/ #{@site.local_path}"
  end

  def add_editor_to_pages
    # Persist state to DB to replace websockets since they can't be used in Nginx/Unicorn.
    set_site_status :editor

    @pages.each do |page|
      EditabilityInjector.inject_editability(@site.local_path, "#{page.url}", "#{@site.directory}", @site.id, page.id)
    end
  end

  def wget_one_page
    system "wget", *('-E -H -k -K -p'.split << @uri.to_s)
  end

  def wget_site
    system "wget", *('--mirror -p --html-extension --convert-links -e robots=off -P .'.split << @uri.to_s)
  end

end
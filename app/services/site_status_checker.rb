require 'net/http'

class SiteStatusChecker

  def self.check_sites_status
    Site.where(mode: 'published').map do | site |
    #Site.where(s3_status: 'published').map do | site |
      next unless site.domain
      url = URI.parse("http://#{site.domain}")
      req = Net::HTTP.new(url.host, url.port)
      res = req.request_head(url)
      #site.ready! if res.code.to_i == 200

      if res.code.to_i == 200
        site.ready!
        site.save
      end
    end
  end

end

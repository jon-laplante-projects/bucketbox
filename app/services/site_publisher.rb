class SitePublisher
  include SpasHelper

  def self.publish(site)
    SitePublisher.new(site).publish
  end

  def initialize(site)
    @site = site
    @user = site.user
    @amazon_key = @user.amazon_key
    @pub_dir = "#{Rails.root}/tmp/sites"
    @s3 = @user.get_s3

    @source_dir = "#{Rails.root}/public/#{@site.directory}"
    @tmp_dir = "#{Rails.root}/tmp/sites/#{@site.directory}"
  end

  def publish
    publish_site
    # prepare
    # move_to_amazon

    # subdomains = @site.subdomains

    # subdomains.each { |subdomain| move_to_amazon "#{subdomain.url}.#{@site.amazon_bucket_name}" } if subdomains

    # clear_temp_directory
    update_site_state
  end

  #private

  def real_domain
    "#{@site.amazon_bucket_name}.s3-website-us-east-1.amazonaws.com"
  end

  def update_site_state
    @site.domain = real_domain
    @site.mode = 'published'
    @site.save
  end

  def move_to_amazon(bucket=nil)
    bucket ||= @site.amazon_bucket_name
    index_dir = "#{@pub_dir}/#{@site.directory}"

    # Get the domain names for this site.
    domains = domains_hashtable

    domains.each do |d|
      if d.parent === true
        File.open("#{index_dir}/s3_website.yml", 'wb') do |file|
          file << "s3_id: #{@amazon_key.access_key}\n"
          file << "s3_secret: #{@amazon_key.secret_access_key}\n"
          file << "s3_bucket: #{@site.directory}\n"
          # file << "s3_bucket: #{bucket}\n"
          file << "site: ../#{@site.directory}"
        end
      else
        File.open("#{index_dir}/s3_website.yml", 'wb') do |file|
          file << "s3_id: #{@amazon_key.access_key}\n"
          file << "s3_secret: #{@amazon_key.secret_access_key}\n"
          file << "s3_bucket: #{@site.directory}\n"
          # file << "s3_bucket: #{bucket}\n"
          file << "site: ../#{@site.directory}\n"
          file << "redirects:\n"
          file << "index.html: #{d.url}"
        end
      end

      Dir.chdir("#{index_dir}") do
        s3_apply_cfg
        s3_push
      end

      bucket = @s3.bucket("#{@site.amazon_bucket_name}")
      #bucket = @s3.buckets[@site.amazon_bucket_name]
      if bucket.exists?
        bucket.clear! if @site.domain.present?
        bucket.configure_website do |cfg|
          cfg.index_document_suffix = site_index(@site)
          cfg.error_document_key = 'error.html'
        end
      end
    end
  end

  def s3_apply_cfg
    system 's3_website cfg apply --headless'
  end

  def s3_push
    system 's3_website push'
  end

  def prepare
    pages = @site.pages.all
    # Code to copy site to tmp and strip editor.

    system "cp -R #{@source_dir}/ #{@tmp_dir}/"

    froala_path = "#{@tmp_dir}/froala"
    system "rm -R #{froala_path}/" if Dir.exists?(froala_path)

    pages.each do |page|
      page_path = "#{@source_dir}/#{page.url}"

      homepage = File.open(page_path) { |f| Nokogiri::HTML f }

      # Remove the "fr_editor_body" class from the <body/> tag.
      page_body = homepage.at_css('body')
      class_arr = page_body['class'].split(' ')

      # Search array and remove fr_editor_body
      class_arr = class_arr - ['fr_editor_body']
      page_body['class'] = "#{class_arr.join(' ')}"

      # Remove the directory references.
      page_html = homepage.css('.editor_injection')

      page_html.each(&:remove)

      # Remove directory refererences.
      homepage = Nokogiri::HTML( homepage.to_s.gsub("#{@site.directory}/", '') )

      # Inject the HTML with editor markup.
      page_head = homepage.at_css('body')
      page_head << tracking_code
      page_head << ga_code if @site.ga_code.present?

      # Write the cleansed file back to FS.
      clean_page_file_path = "#{@tmp_dir}/#{page.url}"
      File.open(clean_page_file_path, 'w+') do |f|
        f.write homepage
      end
    end
  end

  def tracking_code
    <<-TRACKING
            <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js" />
            <script>
                (function (window, document, scriptPath, newElement) {
                    window.domainId = '1';
                    window.trackEvents = true;

                    newElement = document.createElement('script');
                    newElement.async = 1;
                    newElement.src = scriptPath;
                    document.body.appendChild(newElement);
                })(window, document, 'http://bucketbox.net/tracking.js');
            </script>
    TRACKING
  end

  def ga_code
    <<-GA_CODE
              <script>
                (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

                ga('create', '#{@site.ga_code}', 'auto');
                ga('send', 'pageview');

              </script>
    GA_CODE
  end

  def clear_temp_directory
    # Remove the temp directory we created for cleansing out the editor.
    system "rm -R #{@tmp_dir}"
  end




  ##################################################
  ## Adding back older code for publishing site -
  ## I cannot get the current code from the team
  ## to work correctly.
  ##################################################
  def publish_site
    #byebug
    unless @site.blank?
      # site = Site.find(site_id)
      pages = @site.pages.all

      unless @site.domain.blank?
        delete_site_from_amazon(@site.id, "empty")
      end

      #Thread.new do
        # Code to copy site to tmp and strip editor.
        system "cp -R #{Rails.root}/public/#{@site.directory}/ #{Rails.root}/tmp/sites/#{@site.directory}/"
        system "rm -R #{Rails.root}/tmp/sites/#{@site.directory}/foala/"

        pages.each do |page|
          file = File.read("#{Rails.root}/tmp/sites/#{@site.directory}/#{page.url}")
          #file = File.open("#{Rails.root}/tmp/sites/#{@site.directory}/#{page.url}")
          homepage = Nokogiri::HTML(file)

          # Remove the "fr_editor_body" class from the <body/> tag.
          page_body = homepage.at_css("body")

          unless page_body["class"].blank?
            class_arr = page_body["class"].split(" ")
          end

          # Search array and remove fr_editor_body
          class_arr = class_arr - ["fr_editor_body"]
          page_body["class"] = "#{class_arr.join(" ")}"

          # Remove the directory references.
          page_html = homepage.css(".editor_injection")

          page_html.each do |node|
            node.remove
          end

          # Remove directory refererences.
          if homepage.to_s.include? "#{@site.directory}" # Test to see if there are references and if so, remove.
            homepage = Nokogiri::HTML( homepage.to_s.gsub!("#{@site.directory}/", "") )
          end

          # Write the Ahoy tracking code.
          tracking_code = <<-TRACKING
          <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js" />
          <script>
              (function (window, document, scriptPath, newElement) {
                  window.domainId = '1';
                  window.trackEvents = true;

                  newElement = document.createElement('script');
                  newElement.async = 1;
                  newElement.src = scriptPath;
                  document.body.appendChild(newElement);
              })(window, document, 'http://bucketbox.net/tracking.js');
          </script>
          TRACKING

          if @site.ga_code != nil
            ga_code = <<-GA_CODE
          <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', '#{@site.ga_code}', 'auto');
            ga('send', 'pageview');

          </script>
            GA_CODE
          end

          # Inject the HTML with editor markup.
          page_head = homepage.at_css("body")
          page_head << tracking_code
          if @site.ga_code != nil
            page_head << ga_code
          end

          # Write the cleansed file back to FS.
          File.open("#{Rails.root}/tmp/sites/#{@site.directory}/#{page.url}", "w+") do |f|
            f.write homepage
          end

          # Update the DB to reflect our new domain name on S3.
          @site.domain = "#{@site.directory}.s3-website-us-east-1.amazonaws.com"
          @site.mode = "published"
          @site.save
        end

        # Push site from tmp to Amazon.
        move_to_amazon("#{Rails.root}/tmp/sites")
      #end
      # Remove the temp directory we created for cleansing out the editor.
      # system "rm -R #{Rails.root}/tmp/sites/#{site.directory}/"

      #render json: { success: true }, :status => 200
    end
  end

  def unpublish_site
    unless params[:site_id].blank?
      # Delete the bucket from S3.
      delete_site_from_amazon(@site.id, "delete")

      # Delete the domain from the rec.
      # site = Site.find(params[:site_id])

      @site.domain = nil
      @site.mode = "edit"
      @site.save


      render json: { success: true }, :status => 200
    else
      render json: { success: false }, :status => 200
    end
  end

  def delete_site_from_amazon(site_id, action)
    unless @site.blank?
      # site = current_user.sites.find(params[:site_id])
      main_bucket = @site.amazon_bucket_name
      buckets = @site.domains.pluck(:url).map {|domain| "#{domain}.#{main_bucket}"}
      buckets << main_bucket
      Thread.new do
        # Open a connection to the users AWS account and blast the bucket.
        amazon_key = AmazonKey.find_by(user_id: current_user.id)

        AWS.config({
                       region: 'us-east-1',
                       credentials: AWS.config(access_key_id: "#{amazon_key.access_key}", secret_access_key: "#{amazon_key.secret_access_key}"),
                   })

        s3 = Aws::S3.new

        buckets.each do |bucket_name|
          bucket = s3.buckets[bucket_name]

          if bucket.exists?
            # Delete the bucket and all it's contents.
            if action == "delete"
              begin
                bucket.delete!
              rescue
                # Seems there was no bucket to delete!
              end
            else
              bucket.clear!
            end
          end
        end
      end
    end

    return
  end
  ##################################################
  ## End old code for publishing and unpublishing
  ## sites to S3.
  ##################################################

  def domains_hashtable
    domains = []

    if Domain.where(site_id: @site.id).count > 0
      doms = Domain.where(site_id: @site.id, parent: true)

      # Iterate the main domains
      doms.each do |d|
        domain = {}

        # Our top-level domain name.
        domain["url"] = d.url
        domain["parent"] = true

        # Subdomains
        if Domain.where(parent_url_id: d.id, site_id: @site.id).count > 0
          subdomains = []
          subs = Domain.where(parent_url_id: d.id, site_id: @site.id)
          subs.each do |s|
            subdomain = {}

            subdomain["url"] = "#{s.url}.#{d.url}"
            subdomain["parent"] = false

            subdomains << subdomain
          end

          domain["subdomains"] = subdomains
        else
          domain["subdomains"] = []
        end
        domains << domain
      end
    else
      domains = [{ url: "#{@site.directory}", parent: true, subdomains: [] }]
    end

    return domains
  end
end
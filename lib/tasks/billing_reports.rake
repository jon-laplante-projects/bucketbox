task :get_reports => :environment do
  User.where(getting_reports: true).find_each { |user| user.report_bucket.get_report }
end
require 'rails_helper'

RSpec.describe SpaController, type: :controller do
  def login_as(user)
    user ||= create :user
    @request.env["devise.mapping"] = Devise.mappings[:user]

    sign_in user
  end

  def create_domain
    site.domains.create parent: true, name: 'one.org', url: 'one.org'
  end

  describe 'POST #create_or_update_domain' do
    let(:user) { create :user }
    let(:site) { user.sites.first }

    before do
      login_as user
      user.sites.create(template_id: 1)
    end

    it "responds with error when post without parameters" do
      post :create_or_update_domain

      expect(response.body).to match /Site ID and domain name are required./im
    end

    it "create new parent domain if is_main = 1" do
      domain_name = 'example_domain.org'
      post :create_or_update_domain, site_id: site.id, domain: domain_name, is_main: 1

      domain = site.domains.first

      expect(domain).to be_present

      expect(domain.name).to eq domain_name
      expect(domain.url).to eq domain_name
    end

    it "create new subdomain" do
      parent_domain = create_domain

      domain_name = 'example_subdomain.org'
      post :create_or_update_domain, site_id: site.id, domain: domain_name

      domain = site.domains.last

      expect(domain).to be_present

      expect(domain.name).to eq domain_name
      expect(domain.url).to eq domain_name
      expect(domain.parent).to be_falsey
      expect(domain.parent_url_id).to eq(parent_domain.id)
    end

    it "don't save domain if parent domain doesn't exist" do
      domain_name = 'example_subdomain.org'
      post :create_or_update_domain, site_id: site.id, domain: domain_name

      expect(site.domains.count).to eq(0)
      expect(site)
    end

    it "update domain" do
      old_domain = create_domain

      domain_name = 'example_subdomain.org'
      post :create_or_update_domain, site_id: site.id, domain: domain_name, domain_id: old_domain.id

      domain = site.domains.last

      expect(domain).to be_present

      expect(domain.name).to eq old_domain.name
      expect(domain.url).to eq domain_name
    end
  end
end
Template.create!([
  {name: "no_template", label: "No template.", description: "Stub.", thumbnail: "no_template.jpg", directory: "stub", template_design_id: 1},
  {name: "apple_farm", label: "Apple Farms", description: "This is a placeholder.", thumbnail: "applefarm.jpg", directory: "applefarm", template_design_id: 1},
  {name: "businessworld", label: "Business World", description: "Placeholder.", thumbnail: "businessworldtemplate.jpg", directory: "businessworldtemplate", template_design_id: 1},
  {name: "ecoliving", label: "Ecoliving", description: "Placeholder.", thumbnail: "ecoliving.jpg", directory: "ecoliving", template_design_id: 1},
  {name: "generic_website", label: "Generic Website", description: "Placeholder.", thumbnail: "genericwebsitetemplate.jpg", directory: "genericwebsitetemplate", template_design_id: 1},
  {name: "lawfirm", label: "Lawfirm", description: "Placeholder.", thumbnail: "lawfirm.jpg", directory: "lawfirm", template_design_id: 1},
  {name: "adventure_website", label: "Adventure Website Template", description: "An adventure website template good for any outdoor activity company.​", thumbnail: "adventurewebsitetemplate.jpg", directory: "adventurewebsitetemplate", template_design_id: 6},
  {name: "astronomy", label: "Astronomy Website", description: "An astronomy website template good for astronauts, spaceship builders or any science like astrophysics.", thumbnail: "astronomywebsitetemplate.jpg", directory: "astronomywebsitetemplate", template_design_id: 7}
])
TemplateDesign.create!([
  {name: "blog", label: "Blog", desc: nil},
  {name: "business", label: "Business", desc: nil},
  {name: "ecomm", label: "eCommerce", desc: nil},
  {name: "landing", label: "Landing Pages", desc: nil},
  {name: "launch", label: "Launch Pages", desc: nil},
  {name: "misc", label: "Miscellaneous", desc: nil},
  {name: "personal", label: "Personal", desc: nil},
  {name: "portfolio", label: "Portfolio", desc: nil}
])

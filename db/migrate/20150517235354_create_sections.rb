class CreateSections < ActiveRecord::Migration
  def change
    create_table :sections do |t|
      t.integer :page_id
      t.string :section_name
      t.text :content

      t.timestamps null: false
    end
  end
end

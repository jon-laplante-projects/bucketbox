class CreateTemplates < ActiveRecord::Migration
  def change
    create_table :templates do |t|
      t.string :name
      t.string :label
      t.text :description
      t.string :thumbnail
      t.string :directory

      t.timestamps null: false
    end
  end
end

class AddDomainToVisitsAndEvents < ActiveRecord::Migration
  def change
    add_column :visits, :domain_id, :integer, default: nil
    add_column :ahoy_events, :domain_id, :integer, default: nil

    add_index :visits, [:domain_id]
    add_index :ahoy_events, [:domain_id]
  end
end

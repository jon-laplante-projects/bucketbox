class AddSiteTitleToSites < ActiveRecord::Migration
  def change
    add_column :sites, :site_title, :string
  end
end

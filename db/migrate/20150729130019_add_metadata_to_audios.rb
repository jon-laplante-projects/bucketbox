class AddMetadataToAudios < ActiveRecord::Migration
  def change
    add_column :audios, :metadata, :text
  end
end

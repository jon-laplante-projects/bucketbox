class AddMetadataToDocuments < ActiveRecord::Migration
  def change
    add_column :documents, :metadata, :text
  end
end

class ChangeDesignToDesignIdOnTemplates < ActiveRecord::Migration
  def change
  	rename_column :templates, :design, :template_design_id
  	change_column :templates, :template_design_id, :integer
  end
end

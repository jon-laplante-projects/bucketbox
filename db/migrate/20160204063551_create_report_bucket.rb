class CreateReportBucket < ActiveRecord::Migration
  def change
    create_table :report_buckets do |t|
      t.string :name
      t.boolean :has_policy
      
      t.timestamps
    end
    add_index :report_buckets, :name
    add_reference :report_buckets, :user, index: true, foreign_key: true
  end
end

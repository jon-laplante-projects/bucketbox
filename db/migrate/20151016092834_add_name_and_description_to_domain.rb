class AddNameAndDescriptionToDomain < ActiveRecord::Migration
  def change
    add_column :domains, :name, :string
    add_column :domains, :description, :string
  end
end

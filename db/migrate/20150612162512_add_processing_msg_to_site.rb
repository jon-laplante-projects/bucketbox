class AddProcessingMsgToSite < ActiveRecord::Migration
  def change
    add_column :sites, :processing_msg, :text
  end
end

class RemoveNameAndDescriptionFromDomains < ActiveRecord::Migration
  def change
    remove_column :domains, :name
    remove_column :domains, :description
  end
end

class CreateVideos < ActiveRecord::Migration
  def change
    create_table :videos do |t|
      t.integer :user_id
      t.string :name
      t.string :label
      t.attachment :file

      t.timestamps null: false
    end
  end
end

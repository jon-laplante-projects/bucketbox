class CreateTutes < ActiveRecord::Migration
  def change
    create_table :tutes do |t|
      t.string :name
      t.integer :category_id, index: true
      t.text :description
      t.string :url_high

      t.timestamps null: false
    end
  end
end

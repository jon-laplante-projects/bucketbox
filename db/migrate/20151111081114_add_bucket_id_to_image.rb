class AddBucketIdToImage < ActiveRecord::Migration
  def change
    add_column :images, :bucket_id, :integer
  end
end

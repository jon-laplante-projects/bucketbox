class AddBucketIdToAudio < ActiveRecord::Migration
  def change
    add_column :audios, :bucket_id, :integer
  end
end

class CreateSites < ActiveRecord::Migration
  def change
    create_table :sites do |t|
      t.integer :user_id
      t.integer :template_id
      t.string :name
      t.string :label
      t.string :mode
      t.string :directory

      t.timestamps null: false
    end
  end
end

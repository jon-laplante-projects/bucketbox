class AddSiteIdAndUrlAndParentAndParentUrlIdToDomain < ActiveRecord::Migration
  def change
    add_column :domains, :site_id, :integer
    add_column :domains, :url, :string
    add_column :domains, :parent_url_id, :integer
    add_column :domains, :parent, :boolean
  end
end

class AddHasDocumentsBucketToUser < ActiveRecord::Migration
  def change
    add_column :users, :has_documents_bucket, :boolean
  end
end

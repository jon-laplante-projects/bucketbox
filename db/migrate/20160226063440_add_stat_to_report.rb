class AddStatToReport < ActiveRecord::Migration
  def change
    add_column :reports, :current_month_spend, :float, default: 0
    add_column :reports, :total_hosted_data, :float, default: 0
    add_column :reports, :spend_by_service, :text
  end
end
class AddS3StatusIntoSite < ActiveRecord::Migration

  def change
    add_column :sites, :s3_status, :integer
  end

end

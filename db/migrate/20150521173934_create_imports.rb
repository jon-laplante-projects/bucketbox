class CreateImports < ActiveRecord::Migration
  def change
    create_table :imports do |t|
      t.integer :site_id
      t.string :type
      t.text :import_data

      t.timestamps null: false
    end
  end
end

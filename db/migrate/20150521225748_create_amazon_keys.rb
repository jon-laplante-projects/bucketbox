class CreateAmazonKeys < ActiveRecord::Migration
  def change
    create_table :amazon_keys do |t|
      t.integer :user_id
      t.text :access_key
      t.text :secret_access_key

      t.timestamps null: false
    end
  end
end

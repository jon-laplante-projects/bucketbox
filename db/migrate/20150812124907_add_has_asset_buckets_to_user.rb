class AddHasAssetBucketsToUser < ActiveRecord::Migration
  def change
    add_column :users, :has_images_bucket, :boolean
    add_column :users, :has_videos_bucket, :boolean
    add_column :users, :has_audio_bucket, :boolean
  end
end

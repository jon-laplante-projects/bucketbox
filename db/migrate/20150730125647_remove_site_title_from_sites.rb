class RemoveSiteTitleFromSites < ActiveRecord::Migration
  def change
  	remove_column :sites, :site_title
  end
end

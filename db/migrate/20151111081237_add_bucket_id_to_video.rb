class AddBucketIdToVideo < ActiveRecord::Migration
  def change
    add_column :videos, :bucket_id, :integer
  end
end

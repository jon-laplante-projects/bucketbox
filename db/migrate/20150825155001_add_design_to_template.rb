class AddDesignToTemplate < ActiveRecord::Migration
  def change
    add_column :templates, :design, :string
  end
end

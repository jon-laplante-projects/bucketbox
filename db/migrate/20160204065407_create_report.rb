class CreateReport < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.text :content
      t.string :name
      t.timestamps
    end
    add_reference :reports, :report_bucket, index: true, foreign_key: true
  end
end

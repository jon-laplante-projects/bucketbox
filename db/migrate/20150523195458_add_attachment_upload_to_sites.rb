class AddAttachmentUploadToSites < ActiveRecord::Migration
  def self.up
    change_table :sites do |t|
      t.attachment :upload
    end
  end

  def self.down
    remove_attachment :sites, :upload
  end
end

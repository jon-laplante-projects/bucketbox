class AddProcessingEventToSite < ActiveRecord::Migration
  def change
    add_column :sites, :processing_event, :string
  end
end

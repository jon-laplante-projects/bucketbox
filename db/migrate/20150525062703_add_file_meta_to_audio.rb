class AddFileMetaToAudio < ActiveRecord::Migration
  def change
    add_column :audios, :file_meta, :text
  end
end

class AddMetadataToImagesAndVideos < ActiveRecord::Migration
  def change
    add_column :images, :metadata, :text
    add_column :videos, :metadata, :text
  end
end

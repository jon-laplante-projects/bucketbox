class AddGettingReportsToUser < ActiveRecord::Migration
  def change
  	add_column :users, :getting_reports, :boolean
  	add_index :users, :getting_reports
  end
end

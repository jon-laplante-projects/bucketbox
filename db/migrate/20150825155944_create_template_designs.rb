class CreateTemplateDesigns < ActiveRecord::Migration
  def change
    create_table :template_designs do |t|
      t.string :name
      t.string :label
      t.text :desc

      t.timestamps null: false
    end
  end
end

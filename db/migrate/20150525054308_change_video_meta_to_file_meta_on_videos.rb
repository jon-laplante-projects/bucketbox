class ChangeVideoMetaToFileMetaOnVideos < ActiveRecord::Migration
  def change
  	rename_column :videos, :video_meta, :file_meta
  end
end

class AddTallToVideo < ActiveRecord::Migration
  def change
    add_column :videos, :tall, :boolean
  end
end

class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
      t.integer :site_id
      t.string :name
      t.string :label
      t.integer :nav_order

      t.timestamps null: false
    end
  end
end

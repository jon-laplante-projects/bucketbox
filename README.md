## Technologies used
* Ruby on Rails
* jQuery
* Bootstrap
* Knockout

## Description
**S3 Site and Asset Manager** is a software as a service (SaaS) application that allows users to easily managetheir AWS S3 assets, create, update, and destroy buckets, and host websites directly from S3.Features include built-in web editing capabilities, upload of a website from a desktop ornotebook computer, and cloning a web page or website, all from within the application. A team has previously worked on BucketBox, but there are a handful of tasks that need to beclosed prior to release. BucketBox was developed with Ruby On Rails, jQuery, and Knockout.js.

![S3 Site and Asset Manager](https://axyjgq.am.files.1drv.com/y4mX8VMAmVoqVVxHOvw_A2CcacUpMPNHrRn7YZux87IMIUAi9_UE2f32c4ZVvv5f2dRVRjZ3Jnjp2A64uwS6wI6BzNMJ8ZilPRrBElYcx9GSHALCt-7ZUT8CjOR_IP9tCvGIO4QrfiGkRgA58dL6pW9YYhyEdLzJpHmHuttAOT8A0jkl00dasF2oJSnhp9M12CUF0gdn4zTmpqg8rKZIVVuQw?width=1024&height=477&cropmode=none)
![S3 Site and Asset Manager](https://axyggq.am.files.1drv.com/y4mvDyUAtHjDaXRTYUT-7tpvYDkcY_QOK8yJ_ZCVH3KBXyu9OK7GQuiqMhTNCJ9wg8rLRpTsR6VuQ_oq_40CGKhcUWFOxf4M8DoueEsDedv9XO-NKCawcFFCE2LXHPutJJFmQh1wbVipJknFT-BeXhLwvzGofa6bPDZ09knWdFEodDH2wCb-jKiSr__sXMNco2GAlmf9HfnarwoFQOJFjeTGw?width=1024&height=569&cropmode=none)
![S3 Site and Asset Manager](https://bbyegq.am.files.1drv.com/y4maMFd3aMxfpkEtRbjDEvVtjENglcFvvLti5artVRV_z8ZQe8ZBiynn_Vp9Tnz-JagvPWuq2vMiLZ6rXo2IQ_AUbf8E0zVTfcpbF08dyCSVTkT75HjS3CEsyIItbr5Qo5fbQpfjmzkbYxzqRsV1KKD5L7M2FoZSnXaXCdv2r9StyLZ1fOSHP3HIWnxmAUYB6h2UgsQ8EdIekmhI8xtX8mLLQ?width=1024&height=562&cropmode=none)
![S3 Site and Asset Manager](https://axb9ga.am.files.1drv.com/y4mMYC4MgkloeWEFwkwdWiQJ0iamQyemuX34T3cOufQXrQzGJtryYaTDQ_oXauEq749olhy4aOahPw53s-MN5iAJQOvJUIjFyejtI9ZSyAGLM025-UDcF7jOzaCFCCqFyyOJQs8DZsKqB1enAA1t-W9t20n1dxXaRmCJDGTjIrClHF0LCmv-xQb3CA_bi2x4qSq_Ndq2HGfw3NoBZE9FOTLjg?width=1024&height=669&cropmode=none)
![S3 Site and Asset Manager](https://bbyigq.am.files.1drv.com/y4mL4U-HVClitYz627Gwbm-M91IR3iRlFygUcI1dg-_PugbBj0HuOFmDMVIwUJauoX6v30XZJPUPJEFWgXsrTnXFD67Uqh7dEB4wH5iGeXNRSgmGJKM_procTtyu6vjW2NWj9rpEBaGJVQ15Ydc2nWFd0jlQ3YaLNW5hqpaAmj1rW6XS-cSjN2XcL5juPVNktWRIsu0X06AJO-1Lig9uOX9uA?width=1024&height=570&cropmode=none)
source 'https://rubygems.org'


# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.0'
# Use MySQL as the database for Active Record
gem 'mysql2', '~> 0.3.15'#, '0.3.15'
#gem 'pg'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.1.0'
# See https://github.com/sstephenson/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
# gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc

gem "devise", '3.4.1'
gem "cancancan"
gem "font-awesome-rails"
gem "nokogiri" # XML parser for the rest of our XML needs.
gem "wysiwyg-rails" # Editor for changes to sites before publication.
gem "s3_website", '> 1' # Sync website to S3 instead of painful folder/file recursion and push.
#gem "paperclip", '>= 4.3.0' # File upload/attachement.
gem 'paperclip', :git=> 'https://github.com/thoughtbot/paperclip'#, :ref => '523bd46c768226893f23889079a7aa9c73b57d68'
gem 'aws-sdk', '~> 2' # AWS API wrapper.
gem 'paperclip-av-transcoder' # Generates the preview movie and thumbnail image for a video.
gem 'ruby-mp3info', :require => 'mp3info' # Extract metadata from mp3/audio files.
gem 'ahoy_matey' # Analytics tracking for traffic stats.
gem 'activeuuid', '>= 0.5.0' # Add binary(16) UUIDs to ActiveRecord.
gem 'rack-cors', require: 'rack/cors' # Gem to handle CORS cross-origin requests.
gem 'chartkick' # Data/statistical visualization (generates JS).
gem 'chartjs-ror' # Data/statistical visualization (generates JS).
gem 'rubyzip' # Unzip web archives.
gem 'will_paginate' # ActiveRecord query pagination.
gem 'will_paginate-bootstrap' # Integrating the Rubygem will_paginate with Twitter Bootstrap.
gem "mini_magick" # Image editing wrapper using ImageMagick.
gem 'sendgrid-ruby' # Connects to SendGrid's email API.
gem 'activeadmin', '~> 1.0.0.pre2'#, github: 'gregbell/active_admin'  # Administrative interface for application.
gem 'zeroclipboard-rails' # Zeroclipboard JS/Flash utility to copy to clipboard.
gem 'bootstrap-validator-rails' # Bootstrap styling and assets are somehow tied to this gem???
#gem 'formvalidation-rails' # Gem for http://formvalidation.io/, which I have a commercial license for.
gem 'formtastic', '~> 3.0'
gem 'formtastic-bootstrap'
gem "slim-rails"
gem 'stripe'
gem 'app'
gem 'sendgrid'
gem 'git-deploy' # Reduces steps to deployment considerably.
gem 'video_info'
gem 'videojs_rails'
gem 'magick-metadata'
gem 'streamio-ffmpeg'
gem 'handlebars_assets'
gem 'whenever', :require => false
gem 'ransack'
gem 'bootstrap-select-rails'
gem 'clipboard-rails'
gem 'smarter_csv'
gem 'screencap' # Generate a screen capture.
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# gem 'capistrano-rails', group: :development

source 'https://rails-assets.org' do
  gem 'rails-assets-jquery-file-download'
end

group :production do
	# gem 'mina'
	gem 'unicorn' # Use Unicorn as the app server
end

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'
  # gem 'pry-debugger'

  gem 'thin' # A possible improvement over the Webrick testing server.

  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'

  gem 'pry'
  gem 'guard'
  gem 'guard-shell'
  gem 'guard-livereload'
  gem 'capybara'
  gem 'rspec-rails'
  gem 'cucumber-rails', :require => false
  gem 'database_cleaner'
  gem 'factory_girl_rails'
  gem 'seed_dump' # generates seed data.
  gem 'quiet_assets'
  gem 'capistrano'#, '~> 3.4.0'
  gem 'capistrano-bundler'
  gem 'capistrano-rails'
  gem 'capistrano-rvm'
  gem 'capistrano-rails-console'
  gem 'capistrano-file-permissions'
  gem 'capistrano-ext'
end

group :test do
  gem 'poltergeist'
  gem 'phantomjs', :require => 'phantomjs/poltergeist'
  gem 'selenium-webdriver'
end

set :password, ask('Server password:', nil)
set :rails_env, 'production'
hosts = "root@104.236.120.136"

server '104.236.120.136',
  user: 'root',
  roles: %w{web app db},
  hosts: %w{host},
  ssh_options: {
    port: 22,
    forward_agent: true,
        auth_methods: %w(publickey, password)
  }
namespace :db do
  desc 'Dump db'
    task :dump do
      on roles (:db) do
        execute  "mysqldump -u $DB_USER -p$DB_PASS  $DB_NAME > /tmp/dump.mysql" do
          puts out
        end
      download! '/tmp/dump.mysql', '../'
      execute "bash -c 'rm -f /tmp/dump.mysql'"
    end
  end
end

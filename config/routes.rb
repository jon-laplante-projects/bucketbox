Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  devise_for :users, :controllers => { registrations: 'registrations' }
  # devise_for :users
  resources :spas

  root 'spa#index'
  #post "site/new" => 'spa#new_site'
  post "site/new" => 'spa#site_from_template'
  post "upload/site" => 'spa#upload_site'
  post "update/upload/site" => 'spa#update_uploaded_site'
  get "site/import" => 'spa#import_site'
  post "site/import" => 'spa#import_site'
  get "site/:dir" => 'sites#index'
  resources :sites, only: [] do
    get :download, on: :collection
    get :processing_status, on: :member
  end
  get "site/page/edit" => 'spa#page_editor'
  get "site/html" => 'spa#page_html'
  get "site/:site_id/pages" => 'spa#site_pages'
  post "site/pages" => 'spa#site_pages'
  post "sites/list" => 'spa#sites_collection'
  get "sites/list" => 'spa#sites_collection'
  post "site/delete" => 'spa#delete_site'
  post "site/publish" => 'spa#publish_site'
  post "site/unpublish" => 'spa#unpublish_site'
  post "site/save/meta" => 'spa#site_meta_update'
  get "site/pub/stats" => 'spa#publication_percentage_stats'
  post "site/duplicate", to: 'spa#duplicate_site'

  post "sites/:site_id/domains" => 'spa#domains_collection'
  get "sites/:site_id/domains" => 'spa#domains_collection'

  get "page/import" => 'spa#import_page'
  post "page/import" => 'spa#import_page'
  post "page/clone" => 'spa#clone_page'
  post "page/delete" => 'spa#delete_page'
  post "page/title/update" => 'spa#page_title_update'

  post "user/set_report_bucket" => "spa#user_set_report_bucket"
  get "user/test_report_setup" => "spa#user_test_report_setup"

  post "upload/images" => 'media#image_upload'
  get "images/list" => 'media#images_collection'
  post "images/list" => 'media#images_collection'
  get "images/array" => 'media#image_url_array'
  get "images/pagination" => 'media#images_pagination'
  get "images/page/:media_image_page" => 'media#images_page'
  post "image/delete/:image_id" => 'media#delete_image'
  post "page/image/delete" => 'media#delete_page_image'
  post "image/save/meta" => 'media#image_meta_update'

  post "upload/videos" => 'media#video_upload'
  get "videos/list" => 'media#videos_collection'
  post "videos/list" => 'media#videos_collection'
  get "videos/array" => 'media#video_url_array'
  get "videos/pagination" => 'media#videos_pagination'
  get "videos/page/:media_video_page" => 'media#videos_page'
  post "video/delete/:video_id" => 'media#delete_video'
  post "video/thumbnail/rotate" => 'media#rotate_video_thumbnail'
  post "video/save/meta" => 'media#video_meta_update'

  post "upload/audios" => 'media#audio_upload'
  get "audios/list" => 'media#audios_collection'
  post "audios/list" => 'media#audios_collection'
  get "audios/array" => 'media#audio_url_array'
  get "audios/pagination" => 'media#audios_pagination'
  get "audios/page/:media_audio_page" => 'media#audios_page'
  post "audio/delete/:audio_id" => 'media#delete_audio'
  post "audio/save/meta" => 'media#audio_meta_update'

  post "upload/files" => 'documents#file_upload'
  get "documents/files_render" => 'documents#files_render'
  get "documents/list" => 'documents#files_collection'
  post "documents/list" => 'documents#files_collection'
  get "documents/array" => 'documents#file_url_array'
  get "documents/pagination" => 'documents#files_pagination'
  get "documents/page/:media_document_page" => 'documents#files_page'
  post "document/delete/:document_id" => 'documents#delete_file'
  post "document/save/meta" => 'documents#document_meta_update'

  post "buckets/list", to: 'buckets#buckets_collection'
  get "buckets/list", to: 'buckets#buckets_collection'
  post "buckets/new", to: 'buckets#create'
  post "buckets/delete", to: 'buckets#destroy'
  get "buckets/pagination", to: 'buckets#buckets_pagination'
  get "buckets/page/:bucket_page", to: 'buckets#buckets_page'
  get "buckets/select/list", to: 'buckets#buckets_select_list'
  post "buckets/add", to: 'buckets#add_to_bucket'
  post "buckets/remove", to: 'buckets#remove_from_bucket'

  post "aws/new" => 'aws#create_credentials'
  get 'aws/connect_to_s3' => 'aws#connect_to_s3'
  post 'aws/test_credentials' => 'aws#test_credentials'
  get 'aws/configure_aws_automatically' => 'aws#configure_aws_automatically'
  get "bucket/status" => 'aws#bucket_status'

  get "site/:site_id/imported_data" => 'spa#imported_data'

  post "site/edit" => 'spa#save_edits'

  #post "/:site_token/visits" => 'analytics#visits'
  #post "/:site_token/events" => 'analytcs#events'

  # Analytics/tracking
  resources :domains
  match 'ahoy/visits', to: "visits#index", via: [:options]
  post 'ahoy/visits', to: "visits#index"
  resources :visits, only: [:index]
  match 'ahoy/events', to: "events#index", via: [:options]
  post 'ahoy/events', to: "events#index"
  resources :events, only: [:index]

  get '/tracking', to: 'tracking#tracking'
  post 'sites/create_or_update_domain' => 'spa#create_or_update_domain'
  get "sites/:id/delete_domain" => 'spa#delete_domain'
end

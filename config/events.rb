WebsocketRails::EventMap.describe do
  # You can use this file to map incoming events to controller actions.
  # One event can be mapped to any number of controller actions. The
  # actions will be executed in the order they were subscribed.
  #
  # Uncomment and edit the next line to handle the client connected event:
  #   subscribe :client_connected, :to => Controller, :with_method => :method_name
  #
  # Here is an example of mapping namespaced events:
  #   namespace :product do
  #     subscribe :new, :to => ProductController, :with_method => :new_product
  #   end
  # The above will handle an event triggered on the client like `product.new`.
  # subscribe :site_event, :to => 'event#site'
  # subscribe :media_event, :to => 'event#media'
  # Site Events
    # Create
      # Initiated
      # Upload/Download - timer? (name of file being downloaded/uploaded?)
      # Written to DB - Sites
      # Written to DB - Pages
      # Editor injection
      # Completed

    # Update
      # Initiated
      # Write to DB
      # Completed

    # Destroy
      # Initiated
      # Completed

    # Publication
      # Initiated
      # Timer - name of file being pushed?
      # Completed

  # Media Events
    # Create

    # Update

    # Destroy

end

# config valid only for Capistrano 3.1
lock '3.5.0'
#load File.expand_path('../recipes/my_new_thing.rb', __FILE__)
set :application, 'bucketbox'
set :repo_url, 'git@gitlab.com:jonlaplante/bucketbox.git'

set :hosts, 'hosts'

# Default branch is :master
#ask :branch, proc { `git rev-parse --abbrev-ref Master`.chomp }.call
#set :branch, :docker
#set :branch, fetch(:branch, "docker")
#set :env, fetch(:env, "production")
#set :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }
current_branch = `git branch`.match(/\* (\S+)\s/m)[1]
set :branch, ENV['branch'] || current_branch || "master"
# Default deploy_to directory is /var/www/my_app
set :deploy_to, '/webapps/bucketbox'

# Default value for :scm is :git
set :scm, :git
# Default value for :format is :pretty
# set :format, :pretty
set :rvm_type, :system
set :rvm_ruby_version, '2.1.3'
# Default value for :log_level is :debug
# set :log_level, :debug
set :bundle_flags, '--no-deployment --quiet' 

# Default value for :pty is false
# set :pty, true
# Default value for :linked_files is []
set :linked_files, %w{config/database.yml config/secrets.yml }

# Default value for linked_dirs is []
set :linked_dirs, %w{log tmp/pids tmp/cache tmp/sockets tmp/s3_downloads  tmp/sessions tmp/sites  tmp/sockets  tmp/zips}
set :file_permissions_paths, ["#{releases_path}"]
set :file_permissions_users, ["www-data"]

namespace :deploy do
  desc 'Restart application'
  task :restart do
    on roles (:all) do
    execute "cd #{release_path}; touch tmp/restart.txt"
    execute "service unicorn restart"
    execute "chown -R www-data:www-data #{releases_path}"
    end
  end
  task :setup_config do
    on roles (:all) do
    execute :mkdir, "-p #{shared_path}/config; true"
    upload! 'config/database.yml', '/tmp/database.yml'
    upload! 'config/secrets.yml','/tmp/secrets.yml'
    execute "mv /tmp/database.yml #{shared_path}/config/"
    execute "mv /tmp/secrets.yml #{shared_path}/config/"
    end
  end
end
after :deploy, 'deploy:restart'

Rails.configuration.stripe = {
    publishable_key: ENV['STRIPE_PUBLISHABLE_KEY'],      #'for test pk_test_ZSt0XMNE516A7DXQtiWO9e3d',
    secret_key: ENV['STRIPE_SECRET_KEY']                 #'for test sk_test_chsQtUX0afzLqLO2gnzvXoax'
}

Stripe.api_key = Rails.configuration.stripe[:secret_key]
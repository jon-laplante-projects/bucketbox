class Ahoy::Store < Ahoy::Stores::ActiveRecordStore
  def track_visit(options)
    Rails.logger.debug "Track Visit: Options = #{options}"
    super options do |visit|
      # visit.domain_id = "test.domain.jon"
      visit.domain_id = request.params[:domain_id]
    end
  rescue ActiveRecord::RecordInvalid
    # Domain does not exist, do nothing
  end

  def track_event(name, properties, options)
    Rails.logger.debug "Track Event: Name = #{name}"
    Rails.logger.debug "Track Event: Properties = #{properties}"

    super name, properties, options do |event|
      #event.domain_id = "test.domain.jon"
      event.domain_id = request.params[:_json][0][:domain_id]
    end
  rescue ActiveRecord::RecordInvalid
    # Domain does not exist, do nothing
  end
end

require 'paperclip/av/transcoder'

Paperclip.configure do |c|
  c.register_processor :transcoder, Paperclip::Transcoder
end
Paperclip.options[:content_type_mappings] = {
  wma: "audio/x-ms-wma"
}

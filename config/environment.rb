# Load the Rails application.
require File.expand_path('../application', __FILE__)
Bundler.require(*Rails.groups)
# Initialize the Rails application.
Rails.application.initialize!

ActionMailer::Base.smtp_settings = {
    :address => "smtp.sendgrid.net",
    :port => 587,
    :domain => "sendgrid.net",
    :authentication => :plain,
    :user_name => ENV['SENDGRID_USER_NAME'],
    :password => ENV['SENDGRID_PASSWORD']
}

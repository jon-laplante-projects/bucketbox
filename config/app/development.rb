App.configure do
  # Settings specified here will take precedence over those in config/app.rb

  # email of merchat paypal account
  config.paypal_merchant_email = 'bbbox-facilitator@mail.com'
  # name of payment for paypal
  config.paypal_payment_name = 'payment for activation'
  # url of paypal
  config.paypal_url = 'https://www.sandbox.paypal.com/'
  # description of stripe payment
  config.stripe_payment_description = 'payment for using site'

  # config.key = "value"
end

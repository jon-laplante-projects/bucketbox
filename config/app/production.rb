App.configure do
  # Settings specified here will take precedence over those in config/app.rb
  # email of merchat paypal account
  # config.paypal_merchant_email =
  # name of payment for paypal
  # config.paypal_payment_name =
  # url of paypal
  config.paypal_url = 'https://www.paypal.com/'
  # description of stripe payment
  # config.stripe_payment_description =

  # config.key = "value"
end

class App < Configurable # :nodoc:
  # Settings in config/app/* take precedence over those specified here.
  # pay for using site
  config.pay_amount = 275
  # ways of pay
  config.way_pay = [['credit card', 1], ['PayPal', 2], ['Stripe', 3 ]]

  # config.key = "value"
end
